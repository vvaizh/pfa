﻿define([
	  'forms/base/fastened/fastening/fc_abstract'
	, 'forms/base/fastened/fastening/h_fastening_clip'
]
, function (fc_abstract, h_fastening_clip)
{
	var fc_typeahead = fc_abstract();

	fc_typeahead.match = function (dom_item, tag_name, fc_type)
	{
		return 'INPUT' == tag_name && 'typeahead' == fc_type;
	}

	fc_typeahead.render = function (options, model, field_selector, adom_item, fc_data)
	{
		var dom_item = $(adom_item);
		dom_item.typeahead.apply(dom_item, options);
		return fc_data;
	}

	fc_typeahead.load_from_model = function (model, field_selector, dom_item, fc_data)
	{
		var value = h_fastening_clip.get_model_field_value(model, field_selector);
		$(dom_item).typeahead('val', value);
		return fc_data;
	}

	fc_typeahead.save_to_model = function (model, field_selector, dom_item)
	{
		var value = $(dom_item).filter('.tt-input').val();
		return h_fastening_clip.set_model_field_value(model, field_selector, value);
	}

	fc_typeahead.destroy = function (adom_item, fc_data)
	{
		var dom_item = $(adom_item);
		dom_item.typeahead("destroy");
	}

	fc_typeahead.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.typeahead_attrs = function ()
		{
			return this.fastening_attrs(null, 'typeahead');
		}
	}

	return fc_typeahead;
});