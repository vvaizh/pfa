﻿define([
	  'forms/base/fastened/fastening/field/input/fc_input_text'
	, 'forms/base/h_times'
	, 'forms/base/codec/codec.copy'
]
, function (fc_input_text, h_times, codec_copy)
{
	var fc_month_field = codec_copy().Copy(fc_input_text);

	fc_month_field.match = function (dom_item, tag_name, fc_type)
	{
		return 'INPUT' == tag_name && 'month' == fc_type;
	}

	fc_month_field.render = function (options, model, model_selector, adom_item, fc_data)
	{
		var dom_item = $(adom_item);
		dom_item.datepicker(h_times.MonthPickerOptions);
		return fc_data;
	}

	fc_month_field.destroy = function (adom_item, fc_data)
	{
		var dom_item = $(adom_item);
		dom_item.datepicker("destroy");
	}

	fc_month_field.add_template_argument_methods = function (_template_argument)
	{
		_template_argument.month_attrs = function ()
		{
			if (_template_argument.options && _template_argument.options.readonly)
				return '';
			return this.fastening_attrs(null, 'month');
		}

		_template_argument.date = function (model_selector)
		{
			return this.txt(model_selector, 'month');
		}
	}

	return fc_month_field;
});