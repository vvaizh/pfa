start_store_lines_as typeahead_fields_1

  js wbt.SetModelFieldValue("first.a", "Alas");
  js wbt.CheckModelFieldValue("first.a", "Alaska");

stop_store_lines

start_store_lines_as typeahead_fields_2

  js wbt.SetModelFieldValue("first.a", "Pen");
  js wbt.CheckModelFieldValue("first.a", "Pennsylvania");

stop_store_lines

start_store_lines_as typeahead_fields_3

  js wbt.SetModelFieldValue("first.a", "Delaw");
  js wbt.CheckModelFieldValue("first.a", "Delaware");

stop_store_lines

