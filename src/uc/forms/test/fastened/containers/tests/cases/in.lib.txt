start_store_lines_as containers_fields_1

  je wbt.Model_selector_prefix= 'div.self ';
  play_stored_lines simple_fields_1
  je wbt.Model_selector_prefix= 'div.boss ';
  play_stored_lines simple_fields_2
  je wbt.Model_selector_prefix= '';

stop_store_lines

start_store_lines_as containers_fields_2

  je wbt.Model_selector_prefix= 'div.self ';
  play_stored_lines simple_fields_2
  je wbt.Model_selector_prefix= 'div.boss ';
  play_stored_lines simple_fields_3
  je wbt.Model_selector_prefix= '';

stop_store_lines
