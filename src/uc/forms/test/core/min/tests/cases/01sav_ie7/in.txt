include ..\..\..\..\..\..\wbt.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
shot_check_png ..\..\shots\00new_ie7.png
wait_click_full_text "Сохранить отредактированную модель"
shot_check_png ..\..\shots\00new_invalid_ie7.png
wait_click_full_text "OK"
type_id test-test-form-edit 12
shot_check_png ..\..\shots\01sav_ie7.png
wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\01new.result.txt
wait_click_full_text "Редактировать модель в элементе управления"
# shot_check_png ..\..\shots\01sav_ie7.png
exit