﻿define
(
	[
		'txt!forms/test/collection/simple/tests/contents/test3.json.txt'
		, 'forms/base/ajax/ajax-jqGrid'
		, 'forms/base/codec/codec.copy'
	]
	,function (content_ini_txt, ajax_jqGrid, codec_copy)
	{
		var transport = ajax_jqGrid();

		var portion = JSON.parse(content_ini_txt);
		for (var i = 0; i < 10; i++)
		{
			for (var j = 0; j < portion.length; j++)
			{
				var row = codec_copy().Copy(portion[j]);
				row.id = transport.rows_all.length;
				transport.rows_all.push(row);
			}
		}

		transport.options.FilterGetField= function(row,name)
		{
			switch (name)
			{
				case 'алгебра': return row.Оценки.алгебра;
				case 'пение': return row.Оценки.пение;
				default: return row[name];
			}
		}

		transport.options.url_prefix = 'test/grid/rows';

		return transport.prepare_try_to_prepare_send_abort();
	}
);