define(function ()
{

	return {

		controller: {
		  "collection":          { path: 'forms/test/collection/simple/c_test_collection' }
		, "subset":              { path: 'forms/test/collection/subset/c_subset' }
		, "editable_collection": { path: 'forms/test/collection/editable/c_collection_editable' }
		, "grid":                { path: 'forms/test/collection/grid/local/c_test_grid' }
		, "grid_ajax":           { path: 'forms/test/collection/grid/ajax/c_test_grid_ajax' }
		, "collection_inline":   { path: 'forms/test/collection/inline/c_collection_inline' }
		, "simplest_collection": { path: 'forms/test/collection/simplest/c_test_collection0' }
		, "collection_sections": { path: 'forms/test/collection/sections/c_collection_sections' }
		}

		, content: {
		  "collection-0": { path: 'txt!forms/test/collection/simple/tests/contents/test0.json.txt' }
		, "collection-1": { path: 'txt!forms/test/collection/simple/tests/contents/test1.json.txt' }
		, "collection-2": { path: 'txt!forms/test/collection/simple/tests/contents/test2.json.txt' }
		, "collection-3": { path: 'txt!forms/test/collection/simple/tests/contents/test3.json.txt' }
		, 'simplest-collection-3': { path: 'txt!forms/test/collection/simplest/tests/contents/test3.json.txt' }
		}

	};

});