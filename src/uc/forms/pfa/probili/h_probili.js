﻿define(['tpl!forms/pfa/probili/v_probili_company.html']
, function (v_probili_company)
{
	var helper = {};

	// взято из ПАУ
	// магия от неизвестного мне Волшебника
	helper.getNormalCaseOfCity = function (cityName)
	{
		var city = "";
		if (cityName != undefined && cityName.length > 0)
		{
			city = cityName.toLowerCase();
			var cityWordArray = city.split(/[-\s]/);
			$.each(cityWordArray, function (i, cityWord)
			{
				var tempWord = cityWord.charAt(0).toUpperCase() + cityWord.slice(1);
				city = city.replace(cityWord, tempWord);
			});
		}
		return city;
	}

	helper.probiliBloodhound = function (url)
	{
		var bh = new Bloodhound
			({
				datumTokenizer: Bloodhound.tokenizers.obj.whitespace("stitle"),
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				remote: {
					url: url,
					wildcard: "%QUERY",
					rateLimitWait : 600,
					filter: function (responce)
					{
						return $.map(responce.data.items, function (с)
						{
							
							return {
								title: с.title,
								stitle: с.stitle || с.title,
								fio: с.fio,
								inn: с.inn,
								ogrn: с.ogrn,
								city: helper.getNormalCaseOfCity(с.city),
								addressf: с.addressf,
								address: с.address,
								region: с.region,
								bik: с.bank_bik,
								korrschet: с.bank_korrschet,
								phone: с.phone,
							}
						});
					}
				}
			});
		bh.initialize();
		return bh;
	}

	helper.typeahead_field_spec_options_company = function (url)
	{
		return [
			{
				hint: true,
				highlight: true,
				minLength: 1
			},
			{
				name: 'probili'
				, displayKey: "stitle"
				, source: helper.probiliBloodhound(url).ttAdapter()
				, templates: { suggestion: v_probili_company }
			}
		];
	}

	return helper;
});