define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/abonent/debtors/e_pfa_debtors.html'
	, 'forms/base/h_msgbox'
],
function (c_fastened, tpl, h_msgbox)
{
	return function(options_arg)
	{
		var controller = c_fastened(tpl);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderGrid();
		}

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'pfa.back');
		controller.base_grid_url = controller.base_url + '?action=debtor.jqgrid';

		controller.PrepareUrl = function ()
		{
			var url = this.base_grid_url;
			if (this.model)
				url += '&id_Abonent=' + this.model.id_Abonent;
			return url;
		}

		controller.colModel =
		[
			  { name: 'id_Debtor', hidden: true }
			, { label: 'Фамилия', name: 'lastName', width: 100 }
			, { label: 'Имя', name: 'firstName', width: 100 }
			, { label: 'Отчество', name: 'middleName', width: 100 }
		];

		controller.RenderGrid = function ()
		{
			var sel = this.fastening.selector;
			var self = this;

			var url = self.PrepareUrl();
			var grid = $(sel + ' table.grid');
			grid.jqGrid
			({
				datatype: 'json'
				, url: url
				, colModel: self.colModel
				, gridview: true
				, loadtext: 'Загрузка списка анкет...'
				, recordtext: 'Показаны анкеты: {1} из {2}'
				, emptyText: 'Нет анкет для отображения'
				, pgtext : "Страница {0} из {1}"
				, rownumbers: false
				, rowNum: 15
				// , rowList: [5, 10, 15]
				, pager: 'cpw-ama-pfa-debtors-pager'
				, viewrecords: true
				, autowidth: true
				, height: 'auto'
				, multiselect: false
				, multiboxonly: true
				, shrinkToFit: true
				, ignoreCase: true
				, shrinkToFit: true
				, searchOnEnter: true
				, onSelectRow: function () { self.OnSelectRow(); }
				, ondblClickRow: function () { self.OnEdit(); }
				, loadError: function (jqXHR, textStatus, errorThrown)
				{
					h_msgbox.ShowAjaxError("Загрузка списка анкет", url, jqXHR.responseText, textStatus, errorThrown)
				}
			});
			grid.jqGrid('filterToolbar', { stringResult: true, searchOnEnter: false });
		}

		controller.OnSelectRow = function ()
		{
			var sel = this.fastening.selector;
			var grid = $(sel + ' table.grid');
			var selrow = grid.jqGrid('getGridParam', 'selrow');
			var rowdata = grid.jqGrid('getRowData', selrow);

			if (this.OnShowDebtor)
				this.OnShowDebtor(rowdata.id_Debtor);
		}

		return controller;
	}
});
