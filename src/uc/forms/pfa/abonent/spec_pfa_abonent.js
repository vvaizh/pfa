define([
	  'forms/base/h_spec'
]
, function (h_spec)
{
	var items_spec = {

		controller:
		{
			"debtors":
			{
				path: 'forms/pfa/abonent/debtors/c_pfa_debtors'
				, title: 'Анкета абонента'
			}
			,"abonent-login":
			{
				path: 'forms/pfa/abonent/login/c_pfa_abonent_login'
				, title: 'Вход в админку абонента (список анкет)'
			}
			,"abonent-main":
			{
				path: 'forms/pfa/abonent/main/c_pfa_abonent_main'
				, title: 'Админка абонента (список анкет) - главная форма'
			}
		}

		, content:
		{
			"abonent-logged-1":
			{
				path: 'txt!forms/pfa/abonent/main/tests/contents/logged-abonent1.json.txt'
				, title: 'Авторизованный абонент 1'
			}
		}
	};
	return h_spec.combine(items_spec);
});