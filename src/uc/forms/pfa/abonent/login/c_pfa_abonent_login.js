define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/abonent/login/e_pfa_abonent_login.html'
	, 'forms/base/h_msgbox'
	, 'forms/pfa/abonent/main/c_pfa_abonent_main'
],
function (c_fastened, tpl, h_msgbox, c_pfa_abonent_main)
{
	return function(options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'pfa.back');
		controller.base_login_url = controller.base_url + '?action=abonent.login';

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			if (!this.model || null == this.model)
				this.model = { id_Abonent: 1 };
			base_Render.call(this, sel);

			this.form_selector = sel + ' > div.cpw-ama-pfa-abonent-login > div.login-form';

			var self= this;
			$(this.form_selector + ' button.login').click(function () { self.OnLogin(); });
		}

		controller.OnLogin = function ()
		{
			var sel = this.fastening.selector;
			var login = $(this.form_selector + ' input.login').val();
			var password = $(this.form_selector + ' input.password').val();

			var self= this;
			var url= this.base_login_url;
			var v_ajax = h_msgbox.ShowAjaxRequest("Отправка запроса на доступ на сервер", url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: {login:login,password:password, id_Abonent:self.model.id_Abonent}
				, success: function (responce, textStatus)
				{
					if (null == responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else
					{
						self.OnLoged(responce);
					}
				}
			});
		}

		controller.OnLoged = function (logged_data)
		{
			var sel = this.fastening.selector;

			this.main_controller = c_pfa_abonent_main({ base_url: this.base_url });
			this.main_controller.SetFormContent(logged_data);

			$(this.form_selector).hide();

			var logged_selector= sel + ' > div.cpw-ama-pfa-abonent-login > div.logger-part';
			$(logged_selector).show();

			this.main_controller.Edit(logged_selector);
		}

		return controller;
	}
});
