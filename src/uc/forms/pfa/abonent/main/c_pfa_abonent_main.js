define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/abonent/main/e_pfa_abonent_main.html'
	, 'forms/pfa/abonent/debtors/c_pfa_debtors'
	, 'forms/base/h_msgbox'
	, 'forms/pfa/debtor/main/c_pfa_main'
],
function (c_fastened, tpl, c_pfa_debtors, h_msgbox, c_pfa_main)
{
	return function(options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'pfa.back');

		var options =
		{
			field_spec:
			{
				Анкеты: { controller: function () { return c_pfa_debtors({base_url:base_url}); } }
			}
		};

		var controller = c_fastened(tpl, options);

		controller.base_url = base_url;
		controller.base_read_url = controller.base_url + '?action=debtor.crud&cmd=get';

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self= this;
			var Анкеты = this.fastening.get_fc_controller('Анкеты')
			Анкеты.OnShowDebtor = function (id_Debtor) { self.OnShowDebtor(id_Debtor); }

			$(sel + ' > div.cpw-ama-pfa-abonent-main > div.header span.title').click(function () { self.OnBackToAdmin(); });
		}

		controller.OnShowDebtor = function (id_Debtor)
		{
			var self= this;
			var url= this.base_read_url + '&id=' + id_Debtor;
			var v_ajax = h_msgbox.ShowAjaxRequest("Запрос анкеты на сервере", url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'GET'
				, cache: false
				, success: function (responce, textStatus)
				{
					if (null == responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else
					{
						self.ShowDebtor(id_Debtor,responce);
					}
				}
			});
		}

		controller.ShowDebtor = function (id_Debtor,debtor)
		{
			var sel = this.fastening.selector;
			$(sel + ' > div.cpw-ama-pfa-abonent-main > div#cpw-pfa-abonent-main-tabs').hide();

			var debtor_view_selector= sel + ' > div.cpw-ama-pfa-abonent-main > div.debtor-view';
			$(debtor_view_selector).show();

			this.debtor_controller = c_pfa_main({ base_url: base_url, readonly: true });
			this.debtor_controller.SetFormContent(debtor);
			this.debtor_controller.Edit(debtor_view_selector);
		}

		controller.OnBackToAdmin = function ()
		{
			if (this.debtor_controller)
			{
				var sel = this.fastening.selector;

				var debtor_view_selector = sel + ' > div.cpw-ama-pfa-abonent-main > div.debtor-view';

				this.debtor_controller.Destroy();
				delete this.debtor_controller;
				$(debtor_view_selector).html('');
				$(debtor_view_selector).hide();

				$(sel + ' > div.cpw-ama-pfa-abonent-main > div#cpw-pfa-abonent-main-tabs').show();
			}
		}

		return controller;
	}
});
