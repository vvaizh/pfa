
/* ВНИМАНИЕ! */
/* данный файл генерируется автоматически */
/* НЕ редактируйте этот файл вручную для избежания конфликтов! */

/* ATTENTION! */
/* This file is generated automatically */
/* Do not edit this file manually to prevent conflicts! */

define
(
	[
		  'forms/base/collector'
		, 'txt!forms/pfa/brief/steps/step5_person/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/pfa/brief/steps/master/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/pfa/brief/steps/master/tests/contents/example1.json.txt'
		, 'txt!forms/pfa/brief/steps/master/tests/contents/example2.json.txt'
		, 'txt!forms/pfa/brief/steps/master/tests/contents/description-test.json.txt'
		, 'txt!forms/pfa/brief/items/dependent/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/pfa/brief/items/spouse/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/pfa/brief/items/alimony/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/pfa/brief/items/income/tests/contents/01sav-salary.json.etalon.txt'
		, 'txt!forms/pfa/brief/items/income/tests/contents/02edt-contract.json.etalon.txt'
		, 'txt!forms/pfa/brief/items/income/tests/contents/03edt-alimony.json.etalon.txt'
		, 'txt!forms/pfa/brief/items/income/tests/contents/04edt-pension.json.etalon.txt'
		, 'txt!forms/pfa/brief/items/income/tests/contents/05edt-allowance.json.etalon.txt'
		, 'txt!forms/pfa/brief/items/income/tests/contents/06edt-ip.json.etalon.txt'
		, 'txt!forms/pfa/brief/items/debt/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/pfa/brief/items/property/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/pfa/brief/items/expense/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/pfa/brief/items/pledge/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/pfa/brief/items/coowner/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/pfa/calc/tests/credit_payments/credit1.calc.json.etalon.txt'
		, 'txt!forms/pfa/brief/collections/debts/tests/contents/01sav.json.etalon.txt'
		, 'txt!forms/pfa/debtor/graph/tests/contents/example1.json.txt'
		, 'txt!forms/pfa/debtor/main/tests/contents/logged-debtor1.json.txt'
		, 'txt!forms/pfa/debtor/main/tests/contents/logged-debtor2.json.txt'
		, 'txt!forms/pfa/debtor/resolution/tests/contents/ok.json.txt'
		, 'txt!forms/pfa/debtor/resolution/tests/contents/volatile.json.txt'
		, 'txt!forms/pfa/debtor/resolution/tests/contents/critical.json.txt'
		, 'txt!forms/pfa/abonent/main/tests/contents/logged-abonent1.json.txt'
		, 'txt!forms/pfa/root/tests/contents/example1.json.txt'
	],
	function (collect)
	{
		return collect([
		  'step5_person-01sav'
		, 'master-01sav'
		, 'master-example1'
		, 'master-02sav'
		, 'master-03sav'
		, 'dependent-01sav'
		, 'spouse-01sav'
		, 'alimony-01sav'
		, 'income-salary'
		, 'income-contract'
		, 'income-alimony'
		, 'income-pension'
		, 'income-allowance'
		, 'income-ip'
		, 'debt-credit-1'
		, 'property-01sav'
		, 'expense-01sav'
		, 'pledge-01sav-no'
		, 'coowner-01sav'
		, 'payments-credit1'
		, 'debts-01sav'
		, 'graph-example-1'
		, 'debtor-logged-1'
		, 'debtor-logged-2'
		, 'resolution-example-ok'
		, 'resolution-example-volatile'
		, 'resolution-example-critical'
		, 'abonent-logged-1'
		, 'root-example1'
		], Array.prototype.slice.call(arguments,1));
	}
);