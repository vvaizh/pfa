define(
	function () {
		var CODE_EMPTY = 0;
		var CODE_ERROR = 1;
		
		var email_re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		var phone_re = /^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/;
		
		return {
			regexValidation: function (value, regexp) {
				if (value && value != null && value.length != 0) {
					if(regexp.test(String(value))){
						return true;
					}
					else {
						return CODE_ERROR;
					}
				}
				else {
					return CODE_EMPTY;
				}
			},
			
			debtMonthValidation: function (value){
				currentDate = new Date;
				currentMonth = new Date(currentDate.getFullYear(), currentDate.getMonth(), 1, 0, 0, 0);
				MinAllowedMonth = new Date(currentMonth.setMonth(currentDate.getMonth()+1));
				date_pcs =  value.split(".");
				valueDate = new Date(date_pcs[1], date_pcs[0]-1, 1, 0, 0, 0);
				if (value && value != null && value.length != 0) {
					if(valueDate >= MinAllowedMonth){
						return true;
					}
					else {
						return CODE_ERROR;
					}
				}
				else {
					return CODE_EMPTY;
				}
			},
						
			setErrorMessage: function (inputSelector, msgSelector, errMsg){
				document.getElementById(msgSelector).innerHTML = errMsg;
				$(inputSelector).parent()[0].classList.add('hasErrors');
			},
			
			unsetErrorMessage: function (inputSelector, msgSelector){
				document.getElementById(msgSelector).innerHTML = "";
				$(inputSelector).parent()[0].classList.remove('hasErrors');	
			},
			
			requiredValidation: function (inputSelector, msgSelector, onlyResult) {
				var value = $(inputSelector).val();
				if (value != null && value.length != 0) {
					if(!onlyResult){
					document.getElementById(msgSelector).innerHTML = "";
						$(inputSelector).parent()[0].classList.remove('hasErrors');
					}
					return true;
				} else {
					if(!onlyResult){
					document.getElementById(msgSelector).innerHTML = "Это поле обязательно для заполнения";
						$(inputSelector).parent()[0].classList.add('hasErrors');
					}
					return false;
				} 
			},
			
			phoneValidation: function (inputSelector, msgSelector) {
				var value = $(inputSelector).val();
				result = this.regexValidation(value, phone_re);
				if (result === true){
					this.unsetErrorMessage(inputSelector, msgSelector);
				}
				else if (result === CODE_EMPTY){
					this.setErrorMessage(inputSelector, msgSelector, "Это поле обязательно для заполнения");
				}
				else if (result === CODE_ERROR){
					this.setErrorMessage(inputSelector, msgSelector, "Введён неверный номер телефона");
				}
			},
			
			phoneValidationResult: function (inputSelector) {
				var value = $(inputSelector).val();
				result = this.regexValidation(value, phone_re);
				return result === true;
			},
			
			emailValidation: function (inputSelector, msgSelector) {
				var value = $(inputSelector).val();
				result = this.regexValidation(value, email_re);
				if (result === true){
					this.unsetErrorMessage(inputSelector, msgSelector);
				}
				else if (result === CODE_EMPTY){
					this.setErrorMessage(inputSelector, msgSelector, "Это поле обязательно для заполнения");
				}
				else if (result === CODE_ERROR){
					this.setErrorMessage(inputSelector, msgSelector, "Введён неверный email");
				}
			},
			
			emailValidationResult: function (inputSelector) {
				var value = $(inputSelector).val();
				result = this.regexValidation(value, email_re);
				return result === true;
			},
			
			monthValidation: function (inputSelector, msgSelector) {
				var value = $(inputSelector).val();
				result = this.debtMonthValidation(value);
				if (result === true){
					this.unsetErrorMessage(inputSelector, msgSelector);
				}
				else if (result === CODE_EMPTY){
					this.setErrorMessage(inputSelector, msgSelector, "Это поле обязательно для заполнения");
				}
				else if (result === CODE_ERROR){
					this.setErrorMessage(inputSelector, msgSelector, "Введите правильный месяц (мин. 1 мес. от сегодн. даты)");
				}
				return true;
			},
			
			monthValidationResult: function (inputSelector) {
				var value = $(inputSelector).val();
				result = this.debtMonthValidation(value);
				return result === true;
			},
                        
                        checkedValidation: function (inputSelector) {
                            var value = $(inputSelector).attr('checked');
                            if (!(value === 'checked')){
                                $(inputSelector).siblings('label').addClass('pfa-styled error-checkbox-flash');
                            }
                            else {
                                $(inputSelector).siblings('label').removeClass('pfa-styled error-checkbox-flash')
                                return true;
                            };
                        },
                        
                        checkedValidationResult: function (inputSelector) {
                            var value = $(inputSelector).attr('checked');
                            return value === 'checked';
                        }
		};
	}
);
