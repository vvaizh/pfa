﻿define(
	function () {
		Inputmask.extendAliases({
			'currency': {
				'alias': 'numeric',
				'groupSeparator': ' ',
				'digits': 0,
				'digitsOptional': false,
				'suffix': ' ₽',
				'rightAlign': false,
				'showMaskOnHover': false,
				'removeMaskOnSubmit': true,
				'autoUnmask': true,
				'jitMasking': true
			},
			'datetime': {
				'inputFormat': 'dd.mm.yyyy',
				'outputFormat': 'dd.mm.yyyy',
				'showMaskOnHover': false,
				'placeholder': 'дд.мм.гггг',
				'showMaskOnFocus': true,
				'clearMaskOnLostFocus': false
			},
			'payments': {
				'alias': 'numeric',
				'groupSeparator': ' ',
				'digits': 2,
				'digitsOptional': false,
				'suffix': ' ₽/мес',
				'rightAlign': false,
				'showMaskOnHover': false,
				'removeMaskOnSubmit': true,
				'autoUnmask': true,
				'radixPoint': ','
			},
			'percentage': {
				'showMaskOnHover': false,
				'digits': 2,
				'digitsOptional': false,
				'rightAlign': false,
				'radixPoint': ',',
				'removeMaskOnSubmit': true,
				'autoUnmask': true
			},
			'emails': {
				'alias': 'email',
				'showMaskOnHover':false
			},
			'phone': {
				'alias': 'phoneru',
				'showMaskOnHover':false,
			}
		});


		return {
			render_input: function (sel)
			{
				$(sel + ' input[data-inputmask]').inputmask();
			}
		};
});
