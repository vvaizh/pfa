﻿define(function ()
{
	return function(options)
	{
		var helper = {};

		helper.options = options;

		if (options && !options.field_name)
			options.field_name = 'Тип';

		if (options && !options.attribute_name)
			options.attribute_name = 'data-variant';

		helper.variants= options.variants;

		helper.switch_attrs = function (tpl_args)
		{
			var model = tpl_args.value();
			var field = null;
			if (!model || !model[this.options.field_name])
			{
				field = this.variants[0].field_name;
			}
			else
			{
				field = model[this.options.field_name];
				for (var i = 0; i < options.variants.length; i++)
				{
					var variant= options.variants[i];
					if (field == variant.value)
					{
						field= variant.field_name;
						break;
					}
				}
			}
			return ' ' + this.options.attribute_name + '="' + field + '"';
		}

		helper.css = function (sel,visible_display)
		{
			var res = '';
			var field_names = {};
			for (var i= 0; i < this.variants.length; i++)
			{
				var v = this.variants[i].field_name;
				if (!field_names[v])
				{
					field_names[v]= v;
					res+= sel + ' [model-selector="' + v + '"]{display:none;}\r\n';
				}
			}
			field_names = {};
			for (var i= 0; i < this.variants.length; i++)
			{
				var v = this.variants[i].field_name;
				if (!field_names[v])
				{
					field_names[v] = v;
					res += sel + '[' + this.options.attribute_name + '="' + v + '"] [model-selector="' + v + '"]{display:' + visible_display + ';}\r\n';
				}
			}
			this.root_selector= sel;
			return res;
		}

		helper.Fasten = function (controller)
		{
			var options = this.options;
			var sel = controller.fastening.selector;
			var select_dom_item = $(sel + ' [model-selector=' + options.field_name + ']');

			options.controller= controller;

			var self = this;
			select_dom_item.change(function (e)
			{
				console.log('select_dom_item.change');
				var select_dom_item = $(e.target);
				var selected_value = select_dom_item.val();
				var root_dom_item = $(options.controller.fastening.selector + ' ' + self.root_selector);
				var selected_field_name= null;
				for (var i = 0; i < options.variants.length; i++)
				{
					var variant= options.variants[i];
					if (selected_value == variant.value)
					{
						selected_field_name= variant.field_name;
						break;
					}
				}
				root_dom_item.attr(options.attribute_name,selected_field_name);
			});

			var base_GetFormContent= controller.GetFormContent;
			controller.GetFormContent = function ()
			{
				var result = base_GetFormContent.call(this);

				var field_value = result[options.field_name];
				var field_name= null;
				for (var i = 0; i < options.variants.length; i++)
				{
					var variant= options.variants[i];
					if (field_value == variant.value)
					{
						field_name= variant.field_name;
						break;
					}
				}
				for (var i = 0; i < helper.variants.length; i++)
				{
					var option_field_name= helper.variants[i].field_name;
					if (result[option_field_name] && option_field_name != field_name)
						delete result[option_field_name];
				}

				return result;
			}
		}

		return helper;
	}
});
