define(function ()
{
	return function (variants)
	{
		var helper = {};

		helper.variants = variants;
		for (var i = 0; i < variants.length; i++)
		{
			var variant= variants[i];
			if ('string' == typeof variant)
			{
				variant= variants[i] = { text: variant, value: variant.replace(/ /g, '_') };
			}
			else 
			{
				if (!variant.value)
					variant.value = variant.text.replace(/ /g, '_');
			}
			if (!variant.field_name)
				variant.field_name= variant.value;
		}

		helper.txt_for_value = function (value)
		{
			for (var i = 0; i < helper.variants.length; i++)
			{
				var variant = helper.variants[i];
				if (value == variant.value)
					return variant.text;
			}
			return null;
		}

		helper.field_name_for_value = function (value)
		{
			for (var i = 0; i < helper.variants.length; i++)
			{
				var variant = helper.variants[i];
				if (value == variant.value)
					return variant.field_name;
			}
			return null;
		}

		return helper;
	}
});
