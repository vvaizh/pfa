﻿define(function ()
{
	var helper = {};

	helper.RenderSelects = function (sel)
	{
		var self= this;
		$(sel + ' select[fc-by="cpw"]').each(function () { self.RenderSelect($(this)); })
	}

	helper.DestroySelects = function (sel)
	{
		var self= this;
		$(sel + ' select[fc-by="cpw"]').each(function () { self.DestroySelect($(this)); })
	}

	helper.DestroySelect = function (select_item)
	{
		var field_value_item = select_item.parent();
		field_value_item.find('div.selected-value').remove();
		field_value_item.find('select.popup').remove();
	}

	helper.PrepareSelectHtml = function (select_item)
	{
		var width = select_item.width();
		var height = select_item.height();
		var selected_text= select_item.find('option:selected').text();
			
		var html_to_append = '<div class="selected-value" style="width:' + width + 'px;height:' + height + 'px">' + selected_text + '</div>';

		html_to_append += '<select style="display:none;width:' + width + 'px" size="10" class="popup">';
		select_item.find('option').each(function ()
		{
			html_to_append += '<option value="';
			html_to_append += $(this).attr('value');
			html_to_append += '">';
			html_to_append += $(this).text();
			html_to_append += '</option>';
		})

		html_to_append += '</select>';

		return html_to_append;
	}

	helper.RenderSelect = function (select_item)
	{
		return;
		var field_value_item = select_item.parent();

		var html_to_append = this.PrepareSelectHtml(select_item);
		select_item.hide();
		field_value_item.append(html_to_append);

		var selected_value_item= field_value_item.find('div.selected-value');

		select_item.change(function ()
		{
			var selected_text = select_item.find('option:selected').text();
			selected_value_item.text(selected_text);
		});

		console.log(select_item);
		var shown = false;

		var popup_on_click= function (e)
		{
			console.log('popup_on_click {');
			var target= $(e.target);
			if ('SELECT'!=target.prop("tagName"))
			{
				console.log('target[0]:{');
				console.log(target[0]);
				console.log('target[0]:{');
				field_value_item.off('click');
				console.log('click ');
				var popup = field_value_item.find('select.popup');
				if (!shown)
				{
					popup.show();
					click_binded = true;
					console.log('bind select');
					shown = true;
					var popup_on_select = function (echange)
					{
						console.log('popup_on_select {');
						console.log(echange);
						echange.preventDefault();
						var selected_option= popup.find('option:selected');
						var selected_val = selected_option.attr('value');
						var selected_text= selected_option.text();
						console.log(selected_val);
						selected_value_item.text(selected_text);
						select_item.val(selected_val);
						console.log('popup.hide()');
						popup.hide();
						shown = false;
						selected_value_item.on('click', popup_on_click);
						console.log('popup_on_select }');
					};
					popup.on('change', popup_on_select);
				}
			}
			console.log('popup_on_click }');
		};

		selected_value_item.on('click',popup_on_click);
	}

	return helper;
});
