define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/brief/collections/dependents/e_dependents.html'
	, 'forms/pfa/brief/items/dependent/h_dependents'
	, 'forms/base/h_number_format'
],
function (c_fastened, tpl, h_dependents, h_number_format)
{
	return function(options_arg)
	{
		var tpl_options = {
			readonly: options_arg && options_arg.readonly
			, h_number_format: h_number_format
		};
		
		var controller = c_fastened(tpl, tpl_options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			h_dependents(this).Fasten();
			this.fastening.on_after_render();
		}

		return controller;
	}
});
