define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/brief/collections/alimonies/e_alimonies.html'
	, 'forms/pfa/brief/items/alimony/h_alimonies'
	, 'forms/base/h_number_format'
],
function (c_fastened, tpl, h_alimonies, h_number_format)
{
	return function(options_arg)
	{
		var tpl_options = {
			readonly: options_arg && options_arg.readonly
			, h_number_format: h_number_format
		};
		
		var controller = c_fastened(tpl, tpl_options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			h_alimonies(this).Fasten();
			this.fastening.on_after_render();
		}

		return controller;
	}
});
