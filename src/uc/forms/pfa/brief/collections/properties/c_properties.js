define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/brief/collections/properties/e_properties.html'
	, 'forms/pfa/brief/items/property/h_properties'
	, 'forms/pfa/brief/items/property/h_property_type'
	, 'forms/base/h_number_format'
],
function (c_fastened, tpl, h_properties, h_property_type, h_number_format)
{
	return function(options_arg)
	{
		var денежная_сумма_текстом = function (d)
		{
			var res= '';
			if (d && null != d)
			{
				if ('string' != typeof d)
				{
					res= h_number_format(d, 0, '&nbsp;,', '&nbsp;');
				}
				else
				{
					var num= parseFloat(d);
					res= isNaN(num) ? d : h_number_format(num, 0, '&nbsp;,', '&nbsp;');
				}
			}
			return res;
		}
		
		
		
		var добавить_значение_к_сумме = function (val, sumName, total)
		{
			if ('string' == typeof val)
				val = parseInt(val);
			total[sumName] += val;
			return total;
		}
		
		var tpl_options = {
			h_property_type: h_property_type
			, readonly: options_arg && options_arg.readonly
			, h_number_format: h_number_format
		};

		var controller = c_fastened(tpl, tpl_options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			
			var self = this;
			$(sel).on('model_change', function () { self.OnModelChange(); });
			
			h_properties(this).Fasten(sel);
			this.fastening.on_after_render();
			this.UpdateTotal();
		}
		
		controller.UpdateTotal = function ()
		{
			var fastening= this.fastening;
			var sel= fastening.selector;
			var fc_dom_item= $(sel);
			var имущества = fastening.get_fc_model_value(fc_dom_item);

			var total = { Сумма_стоимостей: 0, Сумма_расходов: 0 };
			if (имущества && null != имущества && имущества.length)
			{
				
				for (var i = 0; i < имущества.length; i++)
				{
					var имущество = имущества[i];
					if (имущество.Стоимость)
					{
						total = добавить_значение_к_сумме(имущество.Стоимость, 'Сумма_стоимостей', total);
						
					}
					if (('Недвижимость' in имущество)&&(имущество.Недвижимость.Расходы))
					{
						total = добавить_значение_к_сумме(имущество.Недвижимость.Расходы, 'Сумма_расходов', total);
					}
					if (('Транспорт' in имущество)&&(имущество.Транспорт.Расходы))
					{
						total = добавить_значение_к_сумме(имущество.Транспорт.Расходы, 'Сумма_расходов', total);
					}
					if (('Ценности' in имущество)&&(имущество.Ценности.Расходы))
					{
						total = добавить_значение_к_сумме(имущество.Ценности.Расходы, 'Сумма_расходов', total);
					}
				}
			}
			$(sel + ' div.total span.sum').html(денежная_сумма_текстом(total.Сумма_стоимостей));
			$(sel + ' div.total span.payments').html(денежная_сумма_текстом(total.Сумма_расходов) + ' ₽/мес');
		}

		controller.OnModelChange = function ()
		{
			this.UpdateTotal();
		}

		return controller;
	}
});
