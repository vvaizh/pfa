define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/brief/collections/income/e_income.html'
	, 'forms/pfa/brief/items/income/h_incomes'
	, 'forms/pfa/brief/items/income/h_income_type'
	, 'forms/base/h_number_format'
],
function (c_fastened, tpl, h_incomes, h_income_type, h_number_format)
{
	return function(options_arg)
	{
		var денежная_сумма_текстом = function (d)
		{
			var res= '';
			if (d && null != d)
			{
				if ('string' != typeof d)
				{
					res= h_number_format(d, 0, '&nbsp;,', '&nbsp;');
				}
				else
				{
					var num= parseFloat(d);
					res= isNaN(num) ? d : h_number_format(num, 0, '&nbsp;,', '&nbsp;');
				}
			}
			return res;
		}
		
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'pfa.back');

		var tpl_options = {
			h_income_type: h_income_type
			, readonly: options_arg && options_arg.readonly
			, h_number_format: h_number_format
			, денежная_сумма_текстом: денежная_сумма_текстом
		};

		var controller = c_fastened(tpl, tpl_options );

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			
			var self = this;
			$(sel).on('model_change', function () { self.OnModelChange(); });
			
			h_incomes(this, { base_url: base_url, readonly: options_arg && options_arg.readonly }).Fasten();
			this.fastening.on_after_render();
			this.UpdateTotal();
		}
		
		controller.UpdateTotal = function ()
		{
			var fastening= this.fastening;
			var sel= fastening.selector;
			var fc_dom_item= $(sel);
			var доходы = fastening.get_fc_model_value(fc_dom_item);

			var total = { Среднемесячный_размер: 0 };
			if (доходы && null != доходы && доходы.length)
			{
				
				for (var i = 0; i < доходы.length; i++)
				{
					var доход = доходы[i];
					if (доход.Среднемесячный_размер)
					{
						var Среднемесячный_размер = доход.Среднемесячный_размер;
						if ('string' == typeof Среднемесячный_размер)
							Среднемесячный_размер = parseInt(Среднемесячный_размер);
						total.Среднемесячный_размер += Среднемесячный_размер;
					}
				}
			}
			$(sel + ' div.total span.sum').html(денежная_сумма_текстом(total.Среднемесячный_размер) + ' ₽/мес');
		}

		controller.OnModelChange = function ()
		{
			this.UpdateTotal();
		}

		return controller;
	}
});
