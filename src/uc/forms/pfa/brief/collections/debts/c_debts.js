define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/brief/collections/debts/e_debts.html'
	, 'forms/pfa/brief/items/debt/h_debts'
	, 'forms/pfa/calc/x_debt_payments'
	, 'forms/base/ql'
	, 'forms/base/h_number_format'
	, 'forms/base/log'
],
function (c_fastened, tpl, h_debts, x_debt_payments, ql, h_number_format, GetLogger)
{
	var log = GetLogger('c_debts');
	return function(options_arg)
	{
		var денежная_сумма_текстом = function (d)
		{
			var res= '';
			if (d && null != d)
			{
				if ('string' != typeof d)
				{
					res= h_number_format(d, 0, '&nbsp;,', '&nbsp;');
				}
				else
				{
					var num= parseFloat(d);
					res= isNaN(num) ? d : h_number_format(num, 0, '&nbsp;,', '&nbsp;');
				}
			}
			else {
					res = 0;
			}
			return res;
		}

		var среднемесячный_платёж_по_задолженности = function (debt)
		{
			if (!debt || null === debt){
				return 0;
			}
			try
			{
				var avg = parseFloat(0);
				if (debt.Долг_с_кредитором && debt.Долг_с_кредитором.Пени){
					avg += parseFloat(debt.Долг_с_кредитором.Пени);
				}
				else if (debt.Кредит && debt.Кредит.Ежемесячный_платёж){
					avg += parseFloat(debt.Кредит.Ежемесячный_платёж);
				}
				else if (debt.Долг && debt.Долг.Пени){
					avg += parseFloat(debt.Долг.Пени);
				}
				else if (debt.Долг_с_получателем && debt.Долг_с_получателем.Пени){ 
					avg += parseFloat(debt.Долг_с_получателем.Пени);
				}
				
				if (isNaN(avg)){
						return parseFloat(0);
				}
				return avg;
			}
			catch (e)
			{
				console.log(e);
				log.Error(e.description);
				log.Error(JSON.stringify(debt,null,'\t'));
				return 'ошибка расчёта';
			}
		}
                
                var описание = function (debt){
                    if (!debt || null === debt){
                            return "-";
                    }
                    switch (debt.Тип){
                        case 'перед_банком':
                            return debt.Кредит.Кредитор.Наименование.trim().length ? debt.Кредит.Кредитор.Наименование.trim() : 'Кредит перед банком';
                            
                        case 'по_налогам':
                            return debt.Долг.Описание.trim().length ? debt.Долг.Описание.trim() : 'Долг по налогам';
                            
                        case 'по_социальному_страхованию':
                            return debt.Долг.Описание.trim().length ? debt.Долг.Описание.trim() : 'Долг по социальному страхованию';
                            
                        case 'по_административным_штрафам':
                            return debt.Долг.Описание.trim().length ? debt.Долг.Описание.trim() : 'Долг по административным штрафам';
                            
                        case 'по_уголовным_штрафам':
                            return debt.Долг.Описание.trim().length ? debt.Долг.Описание.trim() : 'Долг по уголовным штрафам';
                            
                        case 'по_алиментам':
                            var creds = debt.Долг_с_получателем.Получатель;
                            if (creds.Фамилия.trim().length || creds.Имя.trim().length || creds.Отчество.trim().length){
                                return [creds.Фамилия, creds.Имя, creds.Отчество].join(' ').trim();
                            }
                            else {
                                return 'Долг по алиментам';
                            }
                            
                        case 'по_ЖКХ':
                            return debt.Долг_с_кредитором.Кредитор.Наименование.trim().length ? debt.Долг_с_кредитором.Кредитор.Наименование.trim() : 'Долг по ЖКХ';
                            
                        case 'перед_микрофинансовой_организацией':
                            return debt.Кредит.Кредитор.Наименование.trim().length ? debt.Кредит.Кредитор.Наименование.trim() : 'Кредит перед микрофинансовой организацией';
                            
                        case 'перед_коллекторским_агенством':
                            return debt.Долг_с_кредитором.Кредитор.Наименование.trim().length ? debt.Долг_с_кредитором.Кредитор.Наименование.trim() : 'Долг перед коллекторским агенством';
                            
                        case 'перед_другим_физическим_лицом':
                            var creds = debt.Долг_с_получателем.Получатель;
                            if (creds.Фамилия.trim().length || creds.Имя.trim().length || creds.Отчество.trim().length){
                                return [creds.Фамилия, creds.Имя, creds.Отчество].join(' ').trim();
                            }
                            else {
                                return 'Долг перед другим физическим лицом';
                            }
                        
                        case 'перед_организацией':
                             return debt.Долг_с_кредитором.Кредитор.Наименование.trim().length ? debt.Долг_с_кредитором.Кредитор.Наименование.trim() : 'Долг перед организацией';
                        
                        default:
                            return "-";
                        
                    }
                }

		var options = {
			readonly: options_arg && options_arg.readonly
			, среднемесячный_платёж_по_задолженности: среднемесячный_платёж_по_задолженности
			, денежная_сумма_текстом: денежная_сумма_текстом
                        , описание: описание
		};
		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel).on('model_change', function () { self.OnModelChange(); });

			h_debts(this).Fasten();
			this.fastening.on_after_render();
			this.UpdateTotal();
		}

		controller.UpdateTotal = function ()
		{
			var fastening= this.fastening;
			var sel= fastening.selector;
			var fc_dom_item= $(sel);
			var долги = fastening.get_fc_model_value(fc_dom_item);

			var total = { Сумма_задолженности: 0, Сумма_платежей: 0 };
			if (долги && null != долги && долги.length)
			{				
				for (var i = 0; i < долги.length; i++)
				{
					var долг = долги[i];
					if (!долг.Сумма_задолженности){
						долг.Сумма_задолженности = 0;
					}
					var Сумма_задолженности = долг.Сумма_задолженности;
					if ('string' == typeof Сумма_задолженности)
						Сумма_задолженности = parseInt(Сумма_задолженности);
					total.Сумма_задолженности += Сумма_задолженности;					
					if (!isNaN(среднемесячный_платёж_по_задолженности(долг))){
						total.Сумма_платежей += parseInt(среднемесячный_платёж_по_задолженности(долг));
					}
					
				}
			}
			$(sel + ' div.total span.sum').html(денежная_сумма_текстом(total.Сумма_задолженности));
			$(sel + ' div.total span.payments').html(денежная_сумма_текстом(total.Сумма_платежей)+' ₽/мес');
		}

		controller.validate = function () {
			return 'Вызывается валидация из c_debts.js';
		}

		controller.OnModelChange = function ()
		{
			this.UpdateTotal();
		}

		return controller;
	}
});
