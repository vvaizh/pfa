define(function ()
{
	var pfa_brief_collections_spec= {

		controller:
		{
			"coowners":
			{
				path: 'forms/pfa/brief/collections/coowners/c_coowners'
				, title: 'Информация о совладельцах (иных собственниках)'
			}
			,"debts":
			{
				path: 'forms/pfa/brief/collections/debts/c_debts'
				, title: 'Перечень задолженностей (долгов)'
			}
			,"incomes":
			{
				path: 'forms/pfa/brief/collections/income/c_income'
				, title: 'Перечень доходов'
			}
			,"properties":
			{
				path: 'forms/pfa/brief/collections/properties/c_properties'
				, title: 'Перечень имущества'
			}
			,"expenses":
			{
				path: 'forms/pfa/brief/collections/expenses/c_expenses'
				, title: 'Перечень расходов'
			}
			,"dependents":
			{
				path: 'forms/pfa/brief/collections/dependents/c_dependents'
				, title: 'Перечень иждивенцев'
			}
			,"alimonies":
			{
				path: 'forms/pfa/brief/collections/alimonies/c_alimonies'
				, title: 'Перечень выплачиваемых алиментов'
			}
			,"spouses":
			{
				path: 'forms/pfa/brief/collections/spouses/c_spouses'
				, title: 'Список супруг (супругов)'
			}
		}

		, content: {
			"debts-01sav":
			{
				path: 'txt!forms/pfa/brief/collections/debts/tests/contents/01sav.json.etalon.txt'
				, title: 'Тестовые данные о долгах'
			}
		}
	};
	return pfa_brief_collections_spec;
});