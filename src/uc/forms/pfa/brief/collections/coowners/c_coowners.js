define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/brief/collections/coowners/e_coowners.html'
	, 'forms/pfa/brief/items/coowner/h_coowners'
],
function (c_fastened, tpl, h_coowners)
{
	return function(options_arg)
	{
		var controller = c_fastened(tpl, { readonly: options_arg && options_arg.readonly });

		controller.size = { width: 1000, height: 500 };

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			h_coowners(this).Fasten();
			this.fastening.on_after_render();
		}

		return controller;
	}
});
