define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/brief/collections/spouses/e_spouses.html'
	, 'forms/pfa/brief/items/spouse/h_spouses'
	, 'forms/base/h_number_format'
],
function (c_fastened, tpl, h_spouses, h_number_format)
{
	return function(options_arg)
	{
		var tpl_options = {
			readonly: options_arg && options_arg.readonly
			, h_number_format: h_number_format
		};
		
		var controller = c_fastened(tpl, tpl_options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			h_spouses(this).Fasten();
			this.fastening.on_after_render();
		}

		return controller;
	}
});
