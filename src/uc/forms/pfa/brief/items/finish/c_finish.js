define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/brief/items/finish/v_finish.html'
	, 'forms/base/h_number_format'
],
function (c_fastened, tpl)
{
	return function()
	{
		var controller = c_fastened(tpl);
		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			var self= this;
			$(sel).parent().addClass('pfa-styled finish-pop-up-window');
			
			$(sel).parent().find('.ui-dialog-buttonset').addClass('finish-pop-up-button-holder');
			$(sel).parent().find('.ui-dialog-buttonset').find('button').removeClass('ui-button');
			$(sel).parent().find('.ui-dialog-buttonset').find('button').addClass('btn btn-forward');
			$(sel).parent().find('.ui-dialog-buttonset').find('button').css('transition', 'none');
		}	
		return controller;
	}
});
