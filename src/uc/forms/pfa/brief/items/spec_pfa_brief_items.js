define(function ()
{
	var pfa_brief_items_spec= {

		controller:
		{
			"dependent":
			{
				path: 'forms/pfa/brief/items/dependent/c_dependent'
				, title: 'Информация об иждивенце'
			}
			,"spouse":
			{
				path: 'forms/pfa/brief/items/spouse/c_spouse'
				, title: 'Информация о супруге'
			}
			,"alimony":
			{
				path: 'forms/pfa/brief/items/alimony/c_alimony'
				, title: 'Информация о расходах на алименты'
			}
			,"income":
			{
				path: 'forms/pfa/brief/items/income/c_income'
				, title: 'Информация о доходе'
			}
			,"debt":
			{
				path: 'forms/pfa/brief/items/debt/c_debt'
				, title: 'Информация о задолженности'
			}
			,"property":
			{
				path: 'forms/pfa/brief/items/property/c_property'
				, title: 'Информация об имуществе'
			}
			,"expense":
			{
				path: 'forms/pfa/brief/items/expense/c_expense'
				, title: 'Информация о расходе'
			}
			,"pledge":
			{
				path: 'forms/pfa/brief/items/pledge/c_pledge'
				, title: 'Информация о залогодержателе'
			}
			,"coowner":
			{
				path: 'forms/pfa/brief/items/coowner/c_coowner'
				, title: 'Информация о совладельце (ином собственнике)'
			}
			,"payments":
			{
				path: 'forms/pfa/brief/items/payments/c_payments'
				, title: 'График платежей по кредиту'
			}
		}

		, content: {
			"dependent-01sav":
			{
				path: 'txt!forms/pfa/brief/items/dependent/tests/contents/01sav.json.etalon.txt'
				, title: 'Информация об иждивенце v1'
			}
			,"spouse-01sav":
			{
				path: 'txt!forms/pfa/brief/items/spouse/tests/contents/01sav.json.etalon.txt'
				, title: 'Информация о супруге v1'
			}
			,"alimony-01sav":
			{
				path: 'txt!forms/pfa/brief/items/alimony/tests/contents/01sav.json.etalon.txt'
				, title: 'Информация о расходах на алименты v1'
			}
			,"income-salary":
			{
				path: 'txt!forms/pfa/brief/items/income/tests/contents/01sav-salary.json.etalon.txt'
				, title: 'Информация о доходе - заработной плате v1'
			}
			,"income-contract":
			{
				path: 'txt!forms/pfa/brief/items/income/tests/contents/02edt-contract.json.etalon.txt'
				, title: 'Информация о доходе - подработке по гражданскому договору v1'
			}
			,"income-alimony":
			{
				path: 'txt!forms/pfa/brief/items/income/tests/contents/03edt-alimony.json.etalon.txt'
				, title: 'Информация о доходе - получаемых алиментах v1'
			}
			,"income-pension":
			{
				path: 'txt!forms/pfa/brief/items/income/tests/contents/04edt-pension.json.etalon.txt'
				, title: 'Информация о доходе - пенсии v1'
			}
			,"income-allowance":
			{
				path: 'txt!forms/pfa/brief/items/income/tests/contents/05edt-allowance.json.etalon.txt'
				, title: 'Информация о доходе - пособиях v1'
			}
			,"income-ip":
			{
				path: 'txt!forms/pfa/brief/items/income/tests/contents/06edt-ip.json.etalon.txt'
				, title: 'Информация о доходе - от деятельности ИП v1'
			}
			,"debt-credit-1":
			{
				path: 'txt!forms/pfa/brief/items/debt/tests/contents/01sav.json.etalon.txt'
				, title: 'Информация о задолженности по кредиту v1'
			}
			,"property-01sav":
			{
				path: 'txt!forms/pfa/brief/items/property/tests/contents/01sav.json.etalon.txt'
				, title: 'Информация об имуществе v1'
			}
			,"expense-01sav":
			{
				path: 'txt!forms/pfa/brief/items/expense/tests/contents/01sav.json.etalon.txt'
				, title: 'Информация о расходе v1'
			}
			,"pledge-01sav-no":
			{
				path: 'txt!forms/pfa/brief/items/pledge/tests/contents/01sav.json.etalon.txt'
				, title: 'Информация об отсутствии залога на имущество'
			}
			,"coowner-01sav":
			{
				path: 'txt!forms/pfa/brief/items/coowner/tests/contents/01sav.json.etalon.txt'
				, title: 'Информация о совладельце v1'
			}
			,"payments-credit1":
			{
				path: 'txt!forms/pfa/calc/tests/credit_payments/credit1.calc.json.etalon.txt'
				, title: 'График платежей по кредиту v1'
			}
		}

	};
	return pfa_brief_items_spec;
});