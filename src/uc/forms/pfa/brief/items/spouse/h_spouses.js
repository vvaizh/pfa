define([
	  'forms/base/fastened/fastening/array/h_array'
	, 'forms/pfa/brief/items/spouse/c_spouse'
],
function (h_array, c_spouse)
{
	return function(controller,selector)
	{
		var settings = {
			controller: controller
			, item_controller: function () { return c_spouse({readonly: (controller.options && controller.options.readonly)}); }
			, О_ком_о_чём: 'о супруге'
			, id_div_prefix: 'pfa-spouse'
			, short_item_description_html: function (item) { return item.Фамилия + ' ' + item.Имя + ' ' + item.Отчество; }
                        , skip_delete_msg: true
		};
		return h_array(settings,selector);
	}
});
