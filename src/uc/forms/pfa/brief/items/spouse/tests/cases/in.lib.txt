
start_store_lines_as pfa_spouse_fields_1

  js wbt.SetModelFieldValue("Расходы_в_месяц", "30000");

  js wbt.SetModelFieldValue("Фамилия", "Зёленкина");
  js wbt.SetModelFieldValue("Имя", "Валентина");
  js wbt.SetModelFieldValue("Отчество", "Денисовна");

  js wbt.SetModelFieldValue("ИНН", "796723099221");
  js wbt.SetModelFieldValue("СНИЛС", "36252510960");

stop_store_lines

start_store_lines_as pfa_spouse_fields_2

  js wbt.SetModelFieldValue("Расходы_в_месяц", "500");

  js wbt.SetModelFieldValue("Фамилия", "Оленин");
  js wbt.SetModelFieldValue("Имя", "Пётр");
  js wbt.SetModelFieldValue("Отчество", "Александрович");

  js wbt.SetModelFieldValue("ИНН", "039741188553");
  js wbt.SetModelFieldValue("СНИЛС", "56948672168");

stop_store_lines

