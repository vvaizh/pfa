define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/brief/items/coowner/e_coowner.html'
],
function (c_fastened, tpl)
{
	return function(options_arg)
	{
		var controller = c_fastened(tpl, { readonly: options_arg && options_arg.readonly });

		controller.size = {width:1000,height:500};

		return controller;
	}
});
