define([
	  'forms/base/fastened/fastening/array/h_array'
	, 'forms/pfa/brief/items/coowner/c_coowner'
],
function (h_array, c_coowner)
{
	return function(controller,selector)
	{
		var settings = {
			controller: controller
			, item_controller: function () { return c_coowner({readonly: (controller.options && controller.options.readonly)}); }
			, id_div_prefix: 'pfa-coowner'
			, О_ком_о_чём: 'об ином собственнике части имущества'
			, short_item_description_html: function (item) { return item.Наименование; }
                        , skip_delete_msg: true
		};
		return h_array(settings,selector);
	}
});
