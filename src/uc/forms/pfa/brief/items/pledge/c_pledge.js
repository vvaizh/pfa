define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/brief/items/pledge/e_pledge.html'
],
function (c_fastened, tpl)
{
	return function(options_arg)
	{
		var controller = c_fastened(tpl, { readonly: options_arg && options_arg.readonly });

		controller.size = {width:1000,height:550};
		
		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			var self = this;
			self.OnChangeRadio();
			$(sel + ' input.pfa-input-checkbox').change(function () { self.OnChangeRadio(); });
		}

		controller.OnChangeRadio = function ()
		{
			var sel = this.fastening.selector;
			if (document.getElementById("pfa-not-pledged-radio").checked) {
				$(sel + ' .pledge-data').hide();
				$('[model-selector="Залогодержатель.Наименование"]').removeAttr('value');
				$('[model-selector="Залогодержатель.ИНН"]').removeAttr('value');
				$('[model-selector="Залогодержатель.ОГРН"]').removeAttr('value');
			}
			else {
				$(sel + ' div.pledge-data').show();
			}
		}

		return controller;
	}
});
