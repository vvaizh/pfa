﻿include ..\in.lib.txt quiet

wait_text "Сумма задолженности"

check_stored_lines pfa_debt_fields_1

play_stored_lines pfa_debt_fields_2

shot_check_png ..\..\shots\02edt.png

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\02edt.json.result.txt
exit