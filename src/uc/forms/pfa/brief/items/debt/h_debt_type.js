﻿define([
'forms/pfa/base/h_switch_variants'
],
function (h_switch_variants)
{
	var variants_text = [
		  { text: 'перед банком', field_name: 'Кредит' }

		, { text: 'по налогам', field_name: 'Долг' }
		, { text: 'по социальному страхованию', field_name: 'Долг' }
		, { text: 'по административным штрафам', field_name: 'Долг' }
		, { text: 'по уголовным штрафам', field_name: 'Долг' }
		, { text: 'по алиментам', field_name: 'Долг_с_получателем' }
		, { text: 'по ЖКХ', field_name: 'Долг_с_кредитором' }

		, { text: 'перед микрофинансовой организацией', field_name: 'Кредит' }
		, { text: 'перед коллекторским агенством', field_name: 'Долг_с_кредитором' }
		, { text: 'перед другим физическим лицом', field_name: 'Долг_с_получателем' }
		, { text: 'перед организацией', field_name: 'Долг_с_кредитором' }

		, { text: 'исполнительное производство', field_name: 'Исполнительное_производство' }
	];

	return h_switch_variants(variants_text);
});
