﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/brief/items/debt/e_debt.html'
	, 'forms/pfa/base/h_switch'
	, 'forms/pfa/brief/items/debt/h_debt_type'
	, 'forms/pfa/calc/x_debt_payments'
	, 'forms/base/h_msgbox'
	, 'forms/pfa/brief/items/payments/c_payments'
	, 'forms/pfa/base/h_select'
	, 'forms/pfa/base/h_inputmask'
	, 'forms/pfa/base/h_input_validator'
],
function (c_fastened, tpl, h_switch, h_debt_type, x_debt_payments, h_msgbox, c_payments, h_select, h_inputmask, h_input_validator)
{
	return function(options_arg)
	{
		var switch_Тип = h_switch({variants: h_debt_type.variants});

		var controller = c_fastened(tpl, { Тип: switch_Тип, readonly: options_arg && options_arg.readonly });

		controller.size = {width:1200,height:900};

		//url на ui-backend.php
		controller.base_url = 'ui-backend.php';

		controller.fssp_proxy_api_url = controller.base_url+"?action=fssp.proxy";

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			switch_Тип.Fasten(this);

			var self= this;
			$(sel + ' button.payments').click(function () { self.OnPayments(); });
			$('#pfa-debt-type-selector').change(function () { self.OnTypeChange(); });
			$('#pfa-ip-mode-selector').change(function () { self.OnIPTypeChange(); });
			
			$('#datepicker-credit-last-payment-date').change(function () {h_input_validator.monthValidation('#datepicker-credit-last-payment-date', 'pfa-debt-credit-error-month')});
			
			$('#pfa-fl-ip-search').click(function () { self.OnFL_IP_Search(); });
			$('#pfa-ip-ip-search').click(function () { self.OnIP_IP_Search(); });
			$('#pfa-number-ip-search').click(function () { self.OnNumber_IP_Search(); });
			$('#pfa-id-ip-search').click(function () { self.OnID_IP_Search(); });
			
			$('button.select-all').click(controller.OnSelectAll);
			$('button.unselect-all').click(controller.OnUnselectAll);
			$('button.select').click(controller.OnSelect);
			$('button.unselect').click(controller.OnUnselect);
			
			self.OnTypeChange();
			self.OnIPTypeChange();
			h_inputmask.render_input(sel);
			$(sel + ' input').change(function (e) { $(sel + ' span').focus(); });
		}

		/*
		controller.validate = function (model) {
			console.log('Вызывается validate от c_debt.js');
			console.log('Пришли данные:');
			console.log(model);
			for (i = 0; i < model.length; i++) {
				console.log('Валидирую элемент #'+i);
				console.log(model[i]);
				if (model[i].Кредит) {
					console.log('Нашел тип Кредит');
					console.log('Провожу валидацию даты');
					if (model[i].Кредит.Дата_погашения) {

					}

				}
				else if (model[i].Долг) {
					console.log('Нашел тип Долг');
				}
				else {
					console.log('Для типа нет правил валидации.')
				}
			}
			
		}
		*/

		controller.OnSelectAll = function()
		{
			var unchoosen_select = $('.unchoosen-select')[0];
			var choosen_select =  $('.choosen-select')[0];
			choosen_select.innerHTML += unchoosen_select.innerHTML;
			unchoosen_select.innerHTML = "";
		}
		
		controller.OnUnselectAll = function()
		{
			var unchoosen_select = $('.unchoosen-select')[0];
			var choosen_select =  $('.choosen-select')[0];
			unchoosen_select.innerHTML += choosen_select.innerHTML;
			choosen_select.innerHTML = "";

		}
		
		controller.OnSelect = function()
		{
			var unchoosen_select = $('.unchoosen-select')[0];
			var choosen_select =  $('.choosen-select')[0];
			for(var i = 0; i < unchoosen_select.children.length; i++)
			{
				var option = unchoosen_select.children[i];
				if(option.selected)
				{
					choosen_select.appendChild(option);
					i--;
				}
			}
		}
		
		controller.OnUnselect = function()
		{
			var unchoosen_select = $('.unchoosen-select')[0];
			var choosen_select =  $('.choosen-select')[0];
			for(var i = 0; i < choosen_select.children.length; i++)
			{
				var option = choosen_select.children[i];
				if(option.selected)
				{
					unchoosen_select.appendChild(option);
					i--;
				}
			}
		}
		
		controller.ShowSelectionResults = function()
		{
			$('.search-result-selects').show();
		}
		
		controller.HideSelectionResults = function()
		{
			$('.search-result-selects').hide();
		}
		
		controller.OnFL_IP_Search = function(){
			alert('Вызвал поиск по ФЛ, заглушка');
			var url = controller.fssp_proxy_api_url+"&cmd=physical";
			var lastname = $("input[model-selector='Должник.Фамилия']").val();
			var firstname = $("input[model-selector='Должник.Имя']").val();
			var secondname = $("input[model-selector='Должник.Отчество']").val();
			var birthdate = $("input[model-selector='Должник.Дата_рождения']").val();
			var region = $("select[model-selector='Исполнительное_производство.Территориальные_органы']").val();
			if (region === "Удмуртская_Республика") {
				region = '18';
			}
			// var Режим = $("select[model-selector='Исполнительное_производство.Режим']").val();
			// var Тип = $("select[model-selector='Тип']").val();
			if(!firstname && !lastname && !region)
				alert("Обязательные поля не заполнены!");
			else
						{
				url += "&firstname=" + firstname;
				url += "&lastname=" + lastname;
				url += "&region=" + region;
				url += secondname? "&secondname=" + firstname: "";
				url += birthdate? "&secondname=" + birthdate: "";
				$.ajax({
					url:url
					, success: function(data)
					{
						var parsed_data = JSON.parse(data);
						var select = $('.unchoosen-select')[0];
						var search_result = parsed_data.response.result[0].result;
						var errors = false;
						for(var i = 0; i<search_result.length; i++)
						{
							//var option = document.createElement("option");
							var option = '<option option_id='+i+' value=\''+
								JSON.stringify(search_result[i])+'\'>'+search_result[i].name+'</option>'
							select.innerHTML+=option;
						}
						if(errors) alert("При выгрузке данных возникли ошибки на стороне фссп. Проверьте правильность заполнения полей.")
						controller.ShowSelectionResults();
					}
				})
			}
		}
		
		controller.OnIP_IP_Search = function(){
			alert('Вызвал поиск по ЮЛ, заглушка');
			var url = controller.fssp_proxy_api_url+"&cmd=legal";
			var name = $("input[model-selector='Исполнительное_производство.Предприятие.Наименование']").val();
        	var region = $("select[model-selector='Исполнительное_производство.Территориальные_органы']").val();
			var address = $("input[model-selector='Исполнительное_производство.Предприятие.Адрес']").val();
			if(region=="Удмуртская_Республика")
			{
				region=18;
			}
			if (!name && !region)
				alert("Обязательные поля не заполнены!");
			else 
			{
				url += "&name=" + name;
				url += "&region=" + region;
				url += address? "&address=" + address: "";
				$.ajax({
					url:url
					, success: function(data)
					{
						var parsed_data = JSON.parse(data);
						var select = $('.unchoosen-select')[0];
						var search_result = parsed_data.response.result[0].result;
						var errors = false;
						for(var i = 0; i<search_result.length; i++)
						{
							//var option = document.createElement("option");
							var option = '<option option_id='+i+' value=\''+
								JSON.stringify(search_result[i])+'\'>'+search_result[i].name+'</option>'
							select.innerHTML+=option;
						}
						if(errors) alert("При выгрузке данных возникли ошибки на стороне фссп. Проверьте правильность заполнения полей.")
						controller.ShowSelectionResults();
					}
				})
			}
		}
		
		controller.OnNumber_IP_Search = function(){
			alert('Вызвал поиск по номеру, заглушка');
			var url = controller.fssp_proxy_api_url+"&cmd=ip";
			// в урл необходимо добавить аргумент number
			var number = $("input[model-selector='Исполнительное_производство.Номер']").val();
			if (!number)
				alert("Обязательные поля не заполнены!");
			else 
			{
				url += "&number=" + number;
				$.ajax({
					url:url
					, success: function(data)
					{
						var parsed_data = JSON.parse(data);
						var select = $('.unchoosen-select')[0];
						var search_result = parsed_data.response.result[0].result;
						var errors = false;
						for(var i = 0; i<search_result.length; i++)
						{
							//var option = document.createElement("option");
							if(search_result[i].errors) errors = true;
							var option = '<option option_id='+i+' value=\''+
								JSON.stringify(search_result[i])+'\'>'+search_result[i].name+'</option>'
							select.innerHTML+=option;
						}
						if(errors) alert("При выгрузке данных возникли ошибки на стороне фссп. Проверьте правильность заполнения полей.")
						controller.ShowSelectionResults();
					}
				})
			}
		}
		
		controller.OnID_IP_Search = function(){
			alert('Вызвал поиск по ИД, заглушка');
		}
		
		controller.CheckFSSPResponseErrors = function(response)
		{
			var errors = parsed_data.response.result[0].result;
		}
		
		controller.OnIPTypeChange = function (){
			$('#pfa-fl-ip-inputs').hide();
			$('#pfa-ip-ip-inputs').hide();
			$('#pfa-number-ip-inputs').hide();
			$('#pfa-id-ip-inputs').hide();
			val = $('#pfa-ip-mode-selector').val();
			switch(val){
				case 'Поиск_физических_лиц':
					$('#pfa-fl-ip-inputs').show();
					break;
				case 'Поиск_юридических_лиц':
					$('#pfa-ip-ip-inputs').show();
					break;
				case 'Поиск_по_номеру_исполнительного_производства':
					$('#pfa-number-ip-inputs').show();
					break;
				case 'Поиск_по_номеру_исполнительного_документа':
					$('#pfa-id-ip-inputs').show();
					break;
			}
		}
		
		
		controller.OnTypeChange = function (){
			val = $('#pfa-debt-type-selector').val();
			if (val === 'исполнительное_производство'){
				$('#pfa-debt-amount').parent().hide();
				$('#pfa-ip-mode-selector').parent().parent().show();
			}
			else {
				$('#pfa-debt-amount').parent().show();
				$('#pfa-ip-mode-selector').parent().parent().hide();
			}
		}
		
		controller.OnPayments = function ()
		{
			var debt = this.GetFormContent();

			var платежи = x_debt_payments().Decode(debt);

			var cpayments= c_payments();
			cpayments.SetFormContent(платежи);
			h_msgbox.ShowModal
			({
					title: 'График платежей по кредиту'
				, controller: cpayments
				, width: 800, height: 600
			});
		}

		return controller;
	}
});
