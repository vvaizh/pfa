define([
	  'forms/base/fastened/fastening/array/h_array'
	, 'forms/pfa/brief/items/debt/c_debt'
],
function (h_array, c_debt)
{
	return function(controller,selector)
	{
		var settings = {
			controller: controller
			, item_controller: function (e)
			{
				var cdebt = c_debt({ readonly: (controller.options && controller.options.readonly) });
				if (e && e.target)
				{
					var item = $(e.target);
					if (!item.hasClass('add'))
						item= item.parents('.add');
					var dtype = item.attr('data-debt-type');
					var debt = { Тип:dtype };
					cdebt.SetFormContent(debt);
				}
				return cdebt;
			}
			, id_div_prefix: 'pfa-debt'
			, Кого_чего_элемента: 'информации'
			, Кого_что_элемент: 'информацию'
			, О_ком_о_чём: 'о задолженности'
			, short_item_description_html: function (item) { return item.Сумма_задолженности + ' руб.'; }
                        , skip_delete_msg: true
		};
		return h_array(settings,selector);
	}
});
