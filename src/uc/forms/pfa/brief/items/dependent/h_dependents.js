define([
	  'forms/base/fastened/fastening/array/h_array'
	, 'forms/pfa/brief/items/dependent/c_dependent'
],
function (h_array, c_dependent)
{
	return function(controller,selector)
	{
		var settings = {
			controller: controller
			, item_controller: function () { return c_dependent({readonly: (controller.options && controller.options.readonly)}); }
			, О_ком_о_чём: 'об иждивенце'
			, id_div_prefix: 'pfa-dependent'
			, short_item_description_html: function (item) { return item.Фамилия + ' ' + item.Имя + ' ' + item.Отчество; }
                        , skip_delete_msg: true
		};
		return h_array(settings,selector);
	}
});
