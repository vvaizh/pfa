﻿include ..\in.lib.txt quiet

wait_text "Работодатель"
shot_check_png ..\..\shots\00new.png

play_stored_lines pfa_income_fields_1_salary

shot_check_png ..\..\shots\01sav-salary.png

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\01sav-salary.json.result.txt
exit