﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/brief/items/income/e_income.html'
	, 'forms/pfa/base/h_switch'
	, 'forms/pfa/brief/items/income/h_income_type'
	, 'forms/pfa/probili/h_probili'
	, 'forms/pfa/base/h_inputmask'
],
function (c_fastened, tpl, h_switch, h_income_type, h_probili, h_inputmask)
{
	return function(options_arg)
	{
		var switch_Тип = h_switch({ variants: h_income_type.variants });

		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'pfa.back');
		var base_probili_url = base_url + '?action=probili.company&query=%QUERY';

		var options = {
			Тип: switch_Тип
			, readonly: options_arg && options_arg.readonly
			, field_spec: {
				Заработная_плата: {
					field_spec: {
						'Работодатель.Наименование': h_probili.typeahead_field_spec_options_company(base_probili_url)
					}
				}
				
			}
		};

		var controller = c_fastened(tpl, options);

		controller.size = {width:1200,height:900};

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			switch_Тип.Fasten(this);

			var self= this;
			$(sel + ' input[model-selector="Работодатель.Наименование"]').bind(
				  "typeahead:select typeahead:autocompleted"
				, function (ev, с) { self.OnSelect_Работодатель(с); }
			);
			h_inputmask.render_input(sel);
		}

		controller.OnSelect_Работодатель = function (c)
		{
			var sel = this.fastening.selector;
			$(sel + ' input[model-selector="Работодатель.Наименование"]').val(c.stitle);
			$(sel + ' input[model-selector="Работодатель.ИНН"]').val(c.inn);
			$(sel + ' input[model-selector="Работодатель.ОГРН"]').val(c.ogrn);
		}

		return controller;
	}
});
