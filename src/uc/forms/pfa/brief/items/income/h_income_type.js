﻿define([
'forms/pfa/base/h_switch_variants'
],
function (h_switch_variants)
{
	var variants_text = [
		  'Заработная плата'
		, 'Подработка по гражданскому договору'
		, 'Деятельность ИП'
		, 'Алименты'
		, 'Пособие и подобные выплаты'
		, 'Пенсия'
		, 'Иное'
	];

	return h_switch_variants(variants_text);
});
