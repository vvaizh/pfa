define([
	  'forms/base/fastened/fastening/array/h_array'
	, 'forms/pfa/brief/items/income/c_income'
],
function (h_array, c_income)
{
	return function(controller,options,selector)
	{
		var settings = {
			controller: controller
			, item_controller: function (e)
			{
				var cincome = c_income({ readonly: (controller.options && controller.options.readonly) });
				if (e && e.target)
				{
					var item = $(e.target);
					if (!item.hasClass('add'))
						item= item.parents('.add');
					var itype = item.attr('data-income-type');
					var income = { Тип:itype };
					cincome.SetFormContent(income);
				}
				return cincome;
			}
			
			, id_div_prefix: 'pfa-income'
			, Кого_чего_элемента: 'статьи'
			, Кого_что_элемент: 'статью'
			, О_ком_о_чём: 'дохода'
			, short_item_description_html: function (item) { return item.Среднемесячный_размер + ' руб.'; }
                        , skip_delete_msg: true
		};
		return h_array(settings,selector);
	}
});
