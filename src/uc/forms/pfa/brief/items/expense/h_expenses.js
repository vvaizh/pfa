define([
	  'forms/base/fastened/fastening/array/h_array'
	, 'forms/pfa/brief/items/expense/c_expense'
],
function (h_array, c_expense)
{
	return function(controller,selector)
	{
		var settings = {
			controller: controller
			, item_controller: function () { return c_expense({readonly: (controller.options && controller.options.readonly)}); }
			, О_ком_о_чём: 'о расходе'
			, id_div_prefix: 'pfa-expense'
			, short_item_description_html: function (item) { return item.Назначение; }
                        , skip_delete_msg: true
		};
		return h_array(settings,selector);
	}
});
