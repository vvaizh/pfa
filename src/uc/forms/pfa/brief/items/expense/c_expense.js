define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/brief/items/expense/e_expense.html'
	, 'forms/pfa/base/h_inputmask'
],
function (c_fastened, tpl, h_inputmask)
{
	return function(options_arg)
	{
		var controller = c_fastened(tpl, { readonly: options_arg && options_arg.readonly });

		controller.size = {width:1100,height:400};
		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			h_inputmask.render_input(sel);
		}
		return controller;
	}
});
