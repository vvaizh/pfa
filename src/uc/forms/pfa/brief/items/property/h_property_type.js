define([
'forms/pfa/base/h_switch_variants'
],
function (h_switch_variants)
{
	var variants = [

		  { text:'Денежные средства' }

		, { text:'Транспортное средство', field_name: 'Транспорт' }
		, { text:'Сельскохозяйственная техника', field_name: 'Транспорт' }


		, { text:'Недвижимость', field_name: 'Недвижимость' }

		, { text:'Акции в собственности', field_name: 'Акции_и_участие_в_коммерческих_организациях' }
		, { text:'Ценные бумаги', field_name: 'Ценные_бумаги' }

		, { text:'Задолженность третьих лиц перед должником', field_name: 'Долги_третьих_лиц' }
		, { text:'Иное ценное имущество', field_name: 'Ценности' }
	];
	return h_switch_variants(variants);
});
