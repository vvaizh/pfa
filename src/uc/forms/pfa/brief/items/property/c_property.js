define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/brief/items/property/e_property.html'
	, 'forms/pfa/base/h_switch'
	, 'forms/pfa/brief/items/property/h_property_type'
	, 'forms/pfa/brief/items/pledge/c_pledge'
	, 'forms/pfa/brief/collections/coowners/c_coowners'
	, 'forms/pfa/probili/h_probili'
	, 'forms/pfa/base/h_inputmask'
],
function (c_fastened, tpl, h_switch, h_property_type, c_pledge, c_coowners, h_probili, h_inputmask)
{
	return function(options_arg)
	{
		var get_payments = function (model)
		{
			type = model.Тип;
			types_with_payments = [
					"Недвижимость",
					"Транспорт",
					"Ценности"
			];
			if (types_with_payments.indexOf(type)){
				return model.type.Расходы;
			}
			else {
				return "-";
			}
		}
		
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'pfa.back');
		var base_probili_url = base_url + '?action=probili.company&query=%QUERY';
		
		var switch_Тип = h_switch({ variants: h_property_type.variants });
		var Залог_spec= {
			  controller: c_pledge
			, title: 'Редактирование информации о залоге'
			, btn_ok_title: 'Сохранить информацию о залоге'
			, width: 400, height:250
			, text: function(залог)
			{
				return (!залог || null == залог || 'да_в_залоге'!=залог.В_залоге_ли)
					? 'Имущество не находится в залоге (кликните чтобы указать другое..)'
					: 'Имущество находится в залоге у ' + залог.Залогодержатель.Наименование;
			}
		};
		var Иные_собственники_spec = {
			  controller: c_coowners
			, title: 'Редактирование информации о совладельцах'
			, btn_ok_title: 'Сохранить информацию о совладельцах'
			, width: 400, height:250
			, text: function(совладельцы)
			{
				return (!совладельцы || null == совладельцы || 0==совладельцы.length)
					? 'Иных собственников нет (кликните чтобы указать другое..)'
					: 'Указано ' + совладельцы.length + ' иных собственника..';
			}
		};
		
		var options =
		{
			Тип: switch_Тип
			, field_spec:
			{
				Недвижимость: { field_spec: { Залог: Залог_spec, Иные_собственники: Иные_собственники_spec } },
				Транспорт: { field_spec: { Залог: Залог_spec, Иные_собственники: Иные_собственники_spec } },
				Акции_и_участие_в_коммерческих_организациях: { field_spec: { Залог: Залог_spec, 'Коммерческая_организация': h_probili.typeahead_field_spec_options_company(base_probili_url)} },
				Ценные_бумаги: { field_spec: { Залог: Залог_spec, 'Ценную_бумагу_выпустило': h_probili.typeahead_field_spec_options_company(base_probili_url) } },
				Долги_третьих_лиц: { field_spec: { Залог: Залог_spec, 'Должно': h_probili.typeahead_field_spec_options_company(base_probili_url) } },
				Ценности: { field_spec: { Залог: Залог_spec } },
			}
		};

		var controller = c_fastened(tpl, options);

		controller.size = {width:1200,height:900};

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			switch_Тип.Fasten(this);
			var self= this;
			$(sel + ' input[model-selector="Коммерческая_организация"]').bind(
				  "typeahead:select typeahead:autocompleted"
				, function (ev, с) { self.OnSelect_Коммерческая_организация(с); }
			);
			$(sel + ' input[model-selector="Ценную_бумагу_выпустило"]').bind(
				  "typeahead:select typeahead:autocompleted"
				, function (ev, с) { self.OnSelect_Ценную_бумагу_выпустило(с); }
			);
			$(sel + ' input[model-selector="Должно"]').bind(
				  "typeahead:select typeahead:autocompleted"
				, function (ev, с) { self.OnSelect_Должно(с); }
			);
			h_inputmask.render_input(sel);
		}
		
		controller.OnSelect_Коммерческая_организация = function (c)
		{
			var sel = this.fastening.selector;
			$(sel + ' input[model-selector="Коммерческая_организация"]').val(c.stitle);
			$(sel + ' input[model-selector="ИНН"]').val(c.inn);
			$(sel + ' input[model-selector="ОГРН"]').val(c.ogrn);
		}

		controller.OnSelect_Ценную_бумагу_выпустило = function (c)
		{
			var sel = this.fastening.selector;
			$(sel + ' input[model-selector="Ценную_бумагу_выпустило"]').val(c.stitle);
			$(sel + ' input[model-selector="ИНН"]').val(c.inn);
			$(sel + ' input[model-selector="ОГРН"]').val(c.ogrn);
		}
		
		controller.OnSelect_Должно = function (c)
		{
			var sel = this.fastening.selector;
			$(sel + ' input[model-selector="Должно"]').val(c.stitle);
			$(sel + ' input[model-selector="ИНН"]').val(c.inn);
			$(sel + ' input[model-selector="ОГРН"]').val(c.ogrn);
		}
		return controller;
	}
});
