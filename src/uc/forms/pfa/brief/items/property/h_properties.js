define([
	  'forms/base/fastened/fastening/array/h_array'
	, 'forms/pfa/brief/items/property/c_property'
	, 'forms/pfa/brief/items/property/h_property_type'
],
function (h_array, c_property, h_property_type)
{
	return function(controller,selector)
	{
		var settings = {
			controller: controller
			, item_controller: function (e)
			{
				var cproperty = c_property({ readonly: (controller.options && controller.options.readonly) });
				if (e && e.target)
				{
					var item = $(e.target);
					if (!item.hasClass('add'))
						item= item.parents('.add');
					var ptype = item.attr('data-property-type');
					var property = { Тип:ptype };
					cproperty.SetFormContent(property);
				}
				return cproperty;
			}
			, id_div_prefix: 'pfa-property'
			, О_ком_о_чём: 'об имуществе'
			, short_item_description_html: function (item) { return h_property_type.txt_for_value(item.Тип) + ' стоимостью ' + item.Стоимость + ' руб.'; }
                        , skip_delete_msg: true
		};
		return h_array(settings,selector);
	}
});
