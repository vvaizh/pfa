﻿
start_store_lines_as pfa_alimony_fields_1

  js wbt.SetModelFieldValue("Фамилия", "Зиновьев");
  js wbt.SetModelFieldValue("Имя", "Алексей");
  js wbt.SetModelFieldValue("Отчество", "Петрович");

  js wbt.SetModelFieldValue("Выплачивается_ежемесячно.Рублей", "15000");
  js wbt.SetModelFieldValue("Выплачивается_ежемесячно.Процентов_от_дохода", "");

stop_store_lines

start_store_lines_as pfa_alimony_fields_2

  js wbt.SetModelFieldValue("Фамилия", "Козарновская");
  js wbt.SetModelFieldValue("Имя", "Любовь");
  js wbt.SetModelFieldValue("Отчество", "Михайловна");

  js $("#pfa-percentage-radio").click();
  js wbt.SetModelFieldValue("Выплачивается_ежемесячно.Процентов_от_дохода", "10");

stop_store_lines

