define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/brief/items/alimony/e_alimony.html'
	, 'forms/pfa/base/h_inputmask'

],
function (c_fastened, tpl, h_inputmask)
{
	return function(options_arg)
	{
		var controller = c_fastened(tpl, { readonly: options_arg && options_arg.readonly });

		controller.size = { width: 1200, height: 700 };

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			var self = this;
			self.OnChangeRadio();
			$(sel + ' input.pfa-input-checkbox').change(function () { self.OnChangeRadio(); });
			h_inputmask.render_input(sel);
			$(sel + ' input').change(function (e) { $(sel + ' span').focus(); });
		}

		controller.OnChangeRadio = function ()
		{
			var sel = this.fastening.selector;
			if (document.getElementById("pfa-fixed-amount-radio").checked) {
				$(sel + ' div.inp.percentage').hide();
				$('[model-selector="Выплачивается_ежемесячно.Процентов_от_дохода"]').removeAttr('value');
				$(sel + ' div.inp.flat-amount').show();
			}
			else {
				$(sel + ' div.inp.percentage').show();
				$(sel + ' div.inp.flat-amount').hide();
				$('[model-selector="Выплачивается_ежемесячно.Рублей"]').removeAttr('value');
			}
		}

		return controller;
	}
});
