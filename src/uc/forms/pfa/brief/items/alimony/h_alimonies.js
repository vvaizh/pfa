define([
	  'forms/base/fastened/fastening/array/h_array'
	, 'forms/pfa/brief/items/alimony/c_alimony'
],
function (h_array, c_alimony)
{
	return function(controller,selector)
	{
		var settings = {
			controller: controller
			, item_controller: function () { return c_alimony({readonly: (controller.options && controller.options.readonly)}); }
			, О_ком_о_чём: 'о расходах на алименты'
			, id_div_prefix: 'pfa-alimony'
			, short_item_description_html: function (item) { return item.Фамилия + ' ' + item.Имя + ' ' + item.Отчество; }
                        , skip_delete_msg: true
		};
		return h_array(settings,selector);
	}
});
