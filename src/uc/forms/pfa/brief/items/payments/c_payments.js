define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/brief/items/payments/v_payments.html'
	, 'forms/base/h_number_format'
],
function (c_fastened, tpl, h_number_format)
{
	return function()
	{
		var денежная_сумма = function (d)
		{
			return ('string'==typeof d) ? d : h_number_format(d, 2, ' ,', ' ');
		}
		var controller = c_fastened(tpl, {денежная_сумма:денежная_сумма});
		return controller;
	}
});
