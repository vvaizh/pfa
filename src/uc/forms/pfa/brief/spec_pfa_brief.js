define([
	  'forms/base/h_spec'
	, 'forms/pfa/brief/steps/spec_pfa_steps'
	, 'forms/pfa/brief/items/spec_pfa_brief_items'
	, 'forms/pfa/brief/collections/spec_pfa_brief_collections'
]
, function (h_spec, spec_steps, spec_pfa_brief_items, spec_pfa_brief_collections)
{
	return h_spec.combine(spec_steps, spec_pfa_brief_items, spec_pfa_brief_collections);
});