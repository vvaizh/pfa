define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/brief/steps/step5_person/e_step5_person.html'
	, 'forms/pfa/base/h_inputmask'
	, 'forms/pfa/base/h_input_validator'
],
function (c_fastened, tpl, h_inputmask, h_input_validator)
{
	return function()
	{
		var controller = c_fastened(tpl);
		
		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' #pfa-anonymous-checkbox').change(function () { self.OnChangeAnonCheckbox(); });
			$(sel + ' #pfa-step5-input-email').change(function () { h_input_validator.emailValidation('#pfa-step5-input-email', 'pfa-step5-error-email')});
			$(sel + ' #pfa-step5-input-phone').change(function () { h_input_validator.phoneValidation('#pfa-step5-input-phone', 'pfa-step5-error-phone')});
                        $(sel + ' #pfa-agree-checkbox').change(function () { h_input_validator.checkedValidation('#pfa-agree-checkbox')});
			h_inputmask.render_input(sel);
		}
		controller.OnChangeAnonCheckbox = function ()
		{
			var sel = this.fastening.selector;
			if (document.getElementById("pfa-anonymous-checkbox").checked) {
				$(sel + ' #personal-data-fields').hide();
				$('[model-selector="Фамилия"]').removeAttr('value');
				$('[model-selector="Имя"]').removeAttr('value');
				$('[model-selector="Отчество"]').removeAttr('value');
			}
			else {
				$(sel + ' #personal-data-fields').show();
			}
		}
		
		controller.willValidate = function (){
			var emailValid = h_input_validator.emailValidationResult('#pfa-step5-input-email', 'pfa-step5-error-email');
			var phValid = h_input_validator.phoneValidationResult('#pfa-step5-input-phone', 'pfa-step5-error-phone');
                        var agreementChecked = h_input_validator.checkedValidationResult('#pfa-agree-checkbox');
			return (emailValid && phValid && agreementChecked) === true;
		}
		controller.forceValidation = function (){
			h_input_validator.emailValidation('#pfa-step5-input-email', 'pfa-step5-error-email');
			h_input_validator.phoneValidation('#pfa-step5-input-phone', 'pfa-step5-error-phone');
                        h_input_validator.checkedValidation('#pfa-agree-checkbox');
		}
		
		return controller;
	}
}
);
