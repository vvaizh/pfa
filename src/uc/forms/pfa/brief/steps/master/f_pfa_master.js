define(['forms/pfa/brief/steps/master/c_pfa_master'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'master'
		, Title: 'Анкета системы финансовой помощи гражданам'
	};
	return form_spec;
});
