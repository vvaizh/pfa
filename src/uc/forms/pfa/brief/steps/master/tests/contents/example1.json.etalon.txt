﻿{
	"Долги": [
            {
                "Тип": "перед_банком",
                "Сумма_задолженности": "100000",
                "Кредит": {
                        "Ежемесячный_платёж": "100,00",
                        "Дата_погашения": "05.2021",
                        "Просроченный_платёж": false,
                        "Сумма_просроченного_платёжа": "",
                        "Дата_последнего_платежа": "",
                        "Кредитор": {
                                "Наименование": "ПАО СБЕРБАНК",
                                "ИНН": "7707083893",
                                "ОГРН": "1027700132195"
                        }
                }
            }
	],
	"Доходы": [
		{
			"Среднемесячный_размер": "15000",
			"Тип": "Заработная_плата",
			"Заработная_плата": {
				"Работодатель": {
					"Наименование": "ООО ДорМозгВынос",
					"ИНН": "6329041675",
					"ОГРН": "5081451079628"
				},
				"Должность": "прораб"
			}
		},
		{
			"Среднемесячный_размер": "10000",
			"Тип": "Подработка_по_гражданскому_договору",
			"Подработка_по_гражданскому_договору": {
				"Периодичность_получения": "НЕ регулярная"
			}
		}
	],
	"Имущество": [
		{
			"Тип": "Денежные_средства",
			"Стоимость": "100000",
			"Денежные_средства": {
				"Краткое_описание": "Зарыты под сараем"
			}
		},
		{
			"Тип": "Гараж",
			"Стоимость": "200000",
			"Недвижимость": {
				"Краткое_описание": "коробка во дворе",
				"Расходы": "300",
				"Залог": null,
				"Вид_собственности": "долевая",
				"Размер_доли": "1/3",
				"Иные_собственники": null,
				"Единственное_жильё": "Да"
			}
		}
	],
	"Расходы": [
		{
			"Сумма": "15000",
			"Назначение": "на курево"
		},
		{
			"Сумма": "12000",
			"Назначение": "на прокладки"
		}
	],
	"Иждивенцы": [
		{
			"Дата_рождения": "03.02.2005",
			"Расходы_в_месяц": "7500",
			"Фамилия": "Воробьёв",
			"Имя": "Сергей",
			"Отчество": "Витальевич"
		},
		{
			"Дата_рождения": "07.05.2015",
			"Расходы_в_месяц": "7000",
			"Фамилия": "Тихонова",
			"Имя": "Лариса",
			"Отчество": "Фёдоровна"
		}
	],
	"Выплачиваемые_алименты": [
		{
			"Выплачивается_ежемесячно": {
				"Рублей": "15000",
				"Процентов_от_дохода": ""
			},
			"Фамилия": "Зиновьев",
			"Имя": "Алексей",
			"Отчество": "Петрович"
		},
		{
			"Выплачивается_ежемесячно": {
				"Рублей": "",
				"Процентов_от_дохода": "10"
			},
			"Фамилия": "Козарновская",
			"Имя": "Любовь",
			"Отчество": "Михайловна"
		}
	],
	"Супруги": [
		{
			"Расходы_в_месяц": "30000",
			"Фамилия": "Зёленкина",
			"Имя": "Валентина",
			"Отчество": "Денисовна",
			"ИНН": "796723099221",
			"СНИЛС": "36252510960"
		},
		{
			"Расходы_в_месяц": "500",
			"Фамилия": "Оленин",
			"Имя": "Пётр",
			"Отчество": "Александрович",
			"ИНН": "039741188553",
			"СНИЛС": "56948672168"
		}
	],
	"Персональные_данные": {
		"Фамилия": "Иванов",
		"Имя": "Иван",
		"Отчество": "Иванович",
		"Телефон": "89005553535",
		"Email": "йо@aga.ugu"
	},
	"Время_заполнения": "24.05.2020 17:36"
}