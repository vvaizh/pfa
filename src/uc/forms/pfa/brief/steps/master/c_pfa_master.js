﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/brief/steps/master/e_pfa_master.html'
	, 'forms/pfa/brief/steps/step5_person/c_step5_person'
	, 'forms/pfa/brief/collections/debts/c_debts'
	, 'forms/pfa/brief/collections/income/c_income'
	, 'forms/pfa/brief/collections/properties/c_properties'
	, 'forms/pfa/brief/steps/step4_expenses/c_step4_expenses'
	, 'forms/pfa/brief/items/finish/c_finish'
	, 'forms/base/h_msgbox'
	, 'forms/pfa/debtor/main/c_pfa_main'
	, 'forms/pfa/debtor/doc/xv_pfa_doc'
	, 'forms/base/h_times'
	, 'forms/base/codec/datetime/h_codec.datetime'
],
function (c_fastened, tpl, c_step5_person, c_debts, c_incomes, c_properties, c_step4_expenses, c_finish, h_msgbox, c_pfa_main, xv_pfa_doc, h_times, h_codec_datetime)
{
	return function(options_arg)
	{
                styles = document.styleSheets;
		for (i = 0; i < styles.length; i++){
			if (styles[i].href && styles[i].href.indexOf('wbt.css') != -1){
				var isTest = true;
				break;
			}
		}

		var base_url= ((options_arg && options_arg.base_url) ? options_arg.base_url : 'pfa.back');
                var base_login_url = base_url + '?action=debtor.login';

		var options =
		{
			field_spec:
			{
				  Персональные_данные: { controller: c_step5_person }
				, Долги: { controller: c_debts }
				, Доходы: { controller: function () { return c_incomes({ base_url: base_url }); } }
				, Имущество: { controller: c_properties }
				, Шаг4: { controller: c_step4_expenses }
			}
		};

		var controller = c_fastened(tpl, options);

		controller.base_url = base_url;
		controller.base_crud_url = controller.base_url + '?action=debtor.crud';
                
                controller.decode = function(input) {
			input = input
				.replace(/-/g, '+')
				.replace(/_/g, '/');
			var pad = input.length % 4;
			if(pad) {
			  if(pad === 1) {
				alert('InvalidLengthError: Input base64url string is the wrong length to determine padding');
			  }
			  input += new Array(5-pad).join('=');
			}
			return input;
		}
		
		controller.urlParam = function(name){
			var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
			if (results == null){
			   return null;
			}
			else {
			   return decodeURI(results[1]) || 0;
			}
		}

                
                controller.getAnketa = function(login, pw, id){
                    var url = base_login_url;
                    var v_ajax = h_msgbox.ShowAjaxRequest("Отправка запроса на доступ на сервер", url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: {login:login,password:pw, id_Abonent:id}
				, success: function (response, textStatus)
				{
					if (null === response)
					{
						v_ajax.ShowAjaxError(response, textStatus);
					}
					else
					{
                                            controller.ShowResult(response);
					}
				}
			});
                }

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			this.steps_selector= sel + ' > div.cpw-ama-pfa-master > div.steps';

			this.current_step = 1;

			var self = this;
			$(sel + ' button.next-with-person').click(function (e) { e.preventDefault(); self.OnNextStepWithPerson(); });
			$(sel + ' button.next-without-person').click(function (e) { e.preventDefault(); self.OnNextStepWithoutPerson(); });
			$(sel + ' button.next').click(function (e) { e.preventDefault(); self.OnNextStep(); });
			$(sel + ' button.back').click(function (e) { e.preventDefault(); self.OnPrevStep(); });
                        
                        var self = this;
                        var auth = controller.urlParam('auth');
                        if (auth){
                                var temp = controller.decode(auth);
                                temp = atob(auth);
                                if (temp){
                                        var credentials = temp.split(',');
                                        var login = credentials[0];
                                        var pw = credentials[1];
                                        var id_Abonent = credentials[2]
                                }
                                controller.getAnketa(login, pw, id_Abonent);
                        }
		}

		var step_title = [
			  'Сведения о задолженности'
			, 'Сведения о доходах'
			, 'Сведения об имуществе'
			, 'Сведения о расходах'
			, 'Установочные данные на лицо, финансовое положение которого анализируется'
		];

		controller.ShowNewStep = function (previous_step)
		{
			var sel = this.fastening.selector;

			$(this.steps_selector + ' div.step' + previous_step).hide();
			$(this.steps_selector + ' div.step' + this.current_step).show();

			$(this.steps_selector + ' div.header span.step-number').text(this.current_step);
			$(this.steps_selector + ' div.header div.title').text(step_title[this.current_step - 1]);

			$(this.steps_selector + ' button.next').css('display', 5 == this.current_step ? 'none' : 'inline-block');
			$(this.steps_selector + ' button.next-with-person').css('display', 5 != this.current_step ? 'none' : 'inline-block');
			$(this.steps_selector + ' button.next-without-person').css('display',5 != this.current_step ? 'none' : 'inline-block');

			$(this.steps_selector + ' button.back').css('display', 1 == this.current_step ? 'none' : 'inline-block');

			$(sel + ' div.progress-value').css('width', (this.current_step*20) + '%');
		}

		controller.OnPrevStep = function ()
		{
			var previous_step= this.current_step;
			this.current_step--;
			this.ShowNewStep(previous_step);
		}

		controller.OnNextStep = function ()
		{
			var previous_step= this.current_step;
			this.current_step++;			
			if (this.current_step <= step_title.length)
			{
				this.ShowNewStep(previous_step);
				this.Disable
			}
			else
			{
				this.OnFinish();
			}
		}

		controller.OnCancel = function ()
		{
		}

		controller.OnNextStepWithPerson = function ()
		{
			var self= this;
			if(c_step5_person().willValidate()){
				var cfinish= c_finish();
				h_msgbox.ShowModal
				({
					title: 'Финал'
					, controller: cfinish
					, width: 630, height: 373
					, buttons: ['Завершить анкетирование']
					, id_div: 'pfa-final-pop-up'
					, onclose:function()
					{
						self.OnNextStep();
					}
				});
			} else {
				c_step5_person().forceValidation();
			}
		}

		controller.OnNextStepWithoutPerson = function ()
		{
			this.OnNextStep();
		}

		var base_GetFormContent= controller.GetFormContent;
		controller.GetFormContent = function ()
		{
			var res = base_GetFormContent.call(this);
			var dt_codec = h_codec_datetime.ru_legal_txt2dt();
			res.Время_заполнения= dt_codec.Decode(h_codec_datetime.Date2dt(h_times.safeDateTime()));
			return res;
		}

		controller.OnFinish = function ()
		{
			var model = this.GetFormContent();

			model.Заключение= xv_pfa_doc().Decode(model);

			var self= this;
			var url = this.base_crud_url + '&cmd=add';
			var v_ajax = h_msgbox.ShowAjaxRequest("Отправка собранной информации на сервер", url);
			v_ajax.ajax({
				  dataType: "json"
				, type: 'POST'
				, cache: false
				, data: model
				, success: function (responce, textStatus)
				{
					if (null == responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else
					{
                                                !isTest ? window.open("https://restart.expert/","_self") : self.ShowResult(responce.data);
					}
				}
			});
		}

		controller.ShowResult = function (logged_data)
		{
			var sel = this.fastening.selector;

			$(this.steps_selector).hide();
			$(sel + ' div.cpw-ama-pfa-master > div.logos-container').hide();

			var result_selector = sel + ' > div.result';

			$(result_selector).show();

			var c_main = c_pfa_main({ base_url: this.base_url });
			c_main.SetFormContent(logged_data);

			c_main.Edit(result_selector);
		}

		return controller;
	}
});
