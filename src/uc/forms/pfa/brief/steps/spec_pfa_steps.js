define([
	  'forms/base/h_spec'
]
, function (h_spec)
{
	var spec_pfa_properties= {

		controller:
		{
			"step5_person":
			{
				path: 'forms/pfa/brief/steps/step5_person/c_step5_person'
				, title: 'Шаг5 Информация о должнике'
				, keywords: 'установочные данные персональные'
			}
			,"step4_expenses":
			{
				path: 'forms/pfa/brief/steps/step4_expenses/c_step4_expenses'
				, title: 'Шаг4 Сведения о расходах'
			}
			,"master":
			{
				path: 'forms/pfa/brief/steps/master/c_pfa_master'
				, title: 'Мастер указания начальной информации'
			}
		}

		, content:
		{
			"step5_person-01sav":
			{
				path: 'txt!forms/pfa/brief/steps/step5_person/tests/contents/01sav.json.etalon.txt'
				, title: 'Информация о должнике v1'
			}
			,"master-01sav":
			{
				path: 'txt!forms/pfa/brief/steps/master/tests/contents/01sav.json.etalon.txt'
				, title: 'Заполненная анкета v1'
			}
			, "master-example1":
			{
				path: 'txt!forms/pfa/brief/steps/master/tests/contents/example1.json.txt'
				, title: 'Заполненная анкета v2'
			}
			, "master-02sav":
			{
				path: 'txt!forms/pfa/brief/steps/master/tests/contents/example2.json.txt'
				, title: 'Заполненная анкета v3'
			}
                        , "master-03sav":
			{
				path: 'txt!forms/pfa/brief/steps/master/tests/contents/description-test.json.txt'
				, title: 'Заполненная анкета v4(тест описаний)'
			}
		}

	};
	return h_spec.combine(spec_pfa_properties);
});