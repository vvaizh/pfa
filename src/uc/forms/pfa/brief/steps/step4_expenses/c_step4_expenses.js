define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/brief/steps/step4_expenses/e_step4_expenses.html'
	, 'forms/pfa/brief/collections/dependents/c_dependents'
	, 'forms/pfa/brief/collections/alimonies/c_alimonies'
	, 'forms/pfa/brief/collections/spouses/c_spouses'
	, 'forms/pfa/brief/collections/expenses/c_expenses'
	, 'forms/base/h_number_format'
],
function (c_fastened, tpl, c_dependents, c_alimonies, c_spouses, c_expenses, h_number_format)
{
	return function()
	{
		var денежная_сумма_текстом = function (d)
		{
			var res= '';
			if (d && null != d)
			{
				if ('string' != typeof d)
				{
					res= h_number_format(d, 0, '&nbsp;,', '&nbsp;');
				}
				else
				{
					var num= parseFloat(d);
					res= isNaN(num) ? d : h_number_format(num, 0, '&nbsp;,', '&nbsp;');
				}
			}
			return res;
		}
		
		var options=
		{
			field_spec:
			{
				  Расходы: { controller: c_expenses }
				, Иждивенцы: { controller: c_dependents }
				, Выплачиваемые_алименты: { controller: c_alimonies }
				, Супруги: { controller: c_spouses }
			}
		};
		var controller = c_fastened(tpl, options);

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this,sel);
			var self = this;
			$(sel + ' div.total').hide();
			$(sel + ' [model-selector="Расходы"]').on('model_change', function () { self.OnChange(); });
			$(sel + ' [model-selector="Иждивенцы"]').on('model_change', function () { self.OnChange(); });
			$(sel + ' [model-selector="Выплачиваемые_алименты"]').on('model_change', function () { self.OnChange(); });
			$(sel + ' [model-selector="Супруги"]').on('model_change', function () { self.OnChange(); });
			var model = this.GetFormContent();
			if (model && null != model) {
				if ((model.Расходы && model.Расходы.length) || (model.Иждивенцы && model.Иждивенцы.length) || (model.Выплачиваемые_алименты && model.Выплачиваемые_алименты.length) || (model.Супруги && model.Супруги.length))
					controller.OnChange();					
			}
		}
		
		controller.OnChange = function ()
		{
			var sel = this.fastening.selector;
			var model = this.GetFormContent();
			var total_sum = 0;
			if (model && null != model)
			{
				if (model.Расходы && null != model.Расходы && model.Расходы.length)
				{
					for (var i = 0; i < model.Расходы.length; i++)
						if (!isNaN(model.Расходы[i].Сумма)) {
							total_sum += parseInt(model.Расходы[i].Сумма);
						}
				}
				if (model.Иждивенцы && null != model.Иждивенцы && model.Иждивенцы.length) {
					for (var i = 0; i < model.Иждивенцы.length; i++)
						if (!isNaN(model.Иждивенцы[i].Расходы_в_месяц)) {
							total_sum += parseInt(model.Иждивенцы[i].Расходы_в_месяц);
						}
				}
				if (model.Выплачиваемые_алименты && null != model.Выплачиваемые_алименты && model.Выплачиваемые_алименты.length) {
					for (var i = 0; i < model.Выплачиваемые_алименты.length; i++)
						if (model.Выплачиваемые_алименты[i].Выплачивается_ежемесячно.Рублей && !isNaN(model.Выплачиваемые_алименты[i].Выплачивается_ежемесячно.Рублей)) {
							total_sum += parseInt(model.Выплачиваемые_алименты[i].Выплачивается_ежемесячно.Рублей);
						}
				}
				if (model.Супруги && null != model.Супруги && model.Супруги.length) {
					for (var i = 0; i < model.Супруги.length; i++)
						if (!isNaN(model.Супруги[i].Расходы_в_месяц)) {
							total_sum += parseInt(model.Супруги[i].Расходы_в_месяц);
						}
				}
			}
			if (0 == total_sum && !isNaN(total_sum))
			{
				$(sel + ' div.total').hide();
			}
			else
			{
				$(sel + ' div.total').show();
				$(sel + ' div.total span.payments').html(денежная_сумма_текстом(total_sum)+' ₽/мес');
			}
		}

		return controller;
	}
});
