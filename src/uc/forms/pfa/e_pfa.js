require.config
({
	enforceDefine: true,
	urlArgs: "bust" + (new Date()).getTime(),
	baseUrl: '.',
	paths:
	{
		styles: 'css',
		images: 'images'
	},
	map:
	{
		'*':
		{
			tpl: 'js/libs/tpl',
			css: 'js/libs/css',
			img: 'js/libs/image',
			txt: 'js/libs/txt'
		}
	}
}),

require
(
	[
		  'forms/pfa/root/f_pfa_root'
		, 'forms/pfa/debtor/login/f_pfa_debtor'
		, 'forms/pfa/brief/steps/master/f_pfa_master'
	],
	function ()
	{
		var extension =
		{
			Title: 'pfa'
			, key: 'pfa'
			, forms: {}
		};
		var forms = Array.prototype.slice.call(arguments, 0);
		for (var i = 0; i < forms.length; i++)
		{
			var form = arguments[i];
			extension.forms[form.key] = form;
		}
		if (RegisterCpwFormsExtension)
		{
			RegisterCpwFormsExtension(extension);
		}
		return extension;
	}
);