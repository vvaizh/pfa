define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/root/e_pfa_root.html'
	, 'forms/base/h_msgbox'
	, 'forms/pfa/debtor/login/c_pfa_debtor_login'
	, 'forms/pfa/abonent/login/c_pfa_abonent_login'
	, 'forms/pfa/brief/steps/master/c_pfa_master'
],
function (c_fastened, tpl, h_msgbox, c_pfa_debtor_login, c_pfa_abonent_login, c_pfa_master)
{
	return function(options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'pfa.back');
		controller.base_list_url = controller.base_url + '?action=abonent.list';

		var base_Render = controller.Render;
		
		controller.decode = function(input) {
			input = input
				.replace(/-/g, '+')
				.replace(/_/g, '/');
			var pad = input.length % 4;
			if(pad) {
			  if(pad === 1) {
				alert('InvalidLengthError: Input base64url string is the wrong length to determine padding');
			  }
			  input += new Array(5-pad).join('=');
			}
			return input;
		}
		
		controller.urlParam = function(name){
			var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
			if (results == null){
			   return null;
			}
			else {
			   return decodeURI(results[1]) || 0;
			}
		}
		
		controller.Render = function (sel)
		{
			if (this.model && null != this.model)
			{
				this.Render_model(sel);
			}
			else
			{
				var self = this;
				var url= this.base_list_url;
				var v_ajax = h_msgbox.ShowAjaxRequest("Отправка запроса на получение списка абонентов", url);
				v_ajax.ajax({
					dataType: "json"
					, type: 'GET'
					, cache: false
					, success: function (responce, textStatus)
					{
						if (null == responce)
						{
							v_ajax.ShowAjaxError(responce, textStatus);
						}
						else
						{
							self.model = responce;
							self.Render_model(sel);
						}
					}
				});
			}
		}

		controller.Render_model = function (sel)
		{
			base_Render.call(this, sel);

			this.list_selector = sel + ' div.cpw-ama-pfa-root > table.root-abonents';
			this.form_selector= sel + ' div.cpw-ama-pfa-root > div.root-abonent-form';

			var self = this;
			var auth = this.urlParam('auth');
			if (auth){
				var temp = this.decode(auth);
				temp = atob(auth);
				if (temp){
					var credentials = temp.split(',');
					var login = credentials[0];
					var pw = credentials[1];
					var id_Abonent = credentials[2]
				}
				self.OnAbonentedForm(credentials, c_pfa_debtor_login);	
			}
			
			$(this.list_selector + ' a.apply').click(function (e) { self.OnAbonentedForm(e,c_pfa_master); });
			$(this.list_selector + ' a.cabinet').click(function (e) { self.OnAbonentedForm(e,c_pfa_debtor_login); });
			$(this.list_selector + ' a.admin').click(function (e) { self.OnAbonentedForm(e, c_pfa_abonent_login); });

			$(this.form_selector + ' > a.back-to-root').click(function (e) { self.OnBackToRoot(); });
		}

		controller.OnAbonentedForm = function (e, form_controller)
		{
			if (Array.isArray(e)){
                            id_Abonent = e[2]; 
			}
			else 
			{
                            var tr = $(e.target).parents('tr');
                            var id_Abonent = tr.attr('data-id-abonent');
			}
			

			this.form_controller = form_controller({ base_url: this.base_url });
			this.form_controller.SetFormContent({id_Abonent:id_Abonent});

			$(this.list_selector).hide();
			$(this.form_selector).show();

			var abonent = {};
			for (var i = 0; i < this.model.length; i++)
			{
				var a = this.model[i];
				if (id_Abonent == a.id_Abonent)
				{
					abonent = a;
					break;
				}
			}

			$(this.form_selector + ' > span.abonent-name').text(abonent.Наименование);

			this.form_controller.Edit(this.form_selector + ' > div.content');
		}

		controller.OnBackToRoot = function ()
		{
			this.form_controller.Destroy();
			delete this.form_controller;

			$(this.form_selector).hide();
			$(this.form_selector + ' > div.content').html('');
			$(this.list_selector).show();
		}

		return controller;
	}
});
