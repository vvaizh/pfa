define(['forms/pfa/root/c_pfa_root'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'root'
		, Title: 'Корневая страница системы финансовой помощи гражданам'
	};
	return form_spec;
});
