define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/debtor/resolution/e_pfa_resolution.html'
	, 'forms/pfa/calc/x_debtor_resolution'
],
function (c_fastened, tpl, x_debtor_resolution)
{
	return function()
	{
		var controller = c_fastened(tpl);
		controller.UseCodec(x_debtor_resolution());
		return controller;
	}
});
