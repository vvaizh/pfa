﻿define(['forms/pfa/calc/x_debtor_prognosis']
,function (x_graph)
{
	return function ()
	{
		var codec = {};

		var взять_год = function (rr, месяц, год)
		{
			var res = [];
			var started= false;
			for (var i = 0; i < rr.length; i++)
			{
				var r= rr[i];
				if (!started)
				{
					if (r.месяц == месяц && r.год == год)
					{
						started = true;
						res.push(r);
					}
				}
				else
				{
					if (r.месяц == месяц)
						break;
					res.push(r);
				}
			}
			return res;
		}

		var взять_календарный_год = function (rr, год)
		{
			var res = [];
			for (var i = 0; i < rr.length; i++)
			{
				var r = rr[i];
				if (r.год == год)
				{
					res.push(r);
				}
			}
			return res;
		}

		codec.Decode = function (data)
		{
			if ('string' == typeof data)
				data = JSON.parse(data);

			var r = x_graph().Decode(data);

			var first = r[0];

			for (var i = 1; i < r.length; i++)
			{
				if (!r[i].денег) {
					r[i - 1].месяц_банкротства = true;
					break;
				}
			}		

			var res =
			{
				  tab_1: { Год1: first.год, Год2: first.год + 1 }
				, tab_2: { Год1: first.год + 1, Год2: first.год + 2 }
				, tab_3: { Год1: first.год + 2, Год2: first.год + 3 }

				, tab_year_1: { Год: first.год }
				, tab_year_2: { Год: first.год + 1 }
				, tab_year_3: { Год: first.год + 2 }
				, tab_year_4: { Год: first.год + 3 }

				, ряды_1: взять_год(r,first.месяц,first.год)
				, ряды_2: взять_год(r,first.месяц,first.год+1)
				, ряды_3: взять_год(r,first.месяц,first.год+2)

				, ряды_year_1: взять_календарный_год(r,first.год)
				, ряды_year_2: взять_календарный_год(r,first.год+1)
				, ряды_year_3: взять_календарный_год(r,first.год+2)
				, ряды_year_4: взять_календарный_год(r,first.год+3)
			};

			return res;
		}

		return codec;
	}
});
