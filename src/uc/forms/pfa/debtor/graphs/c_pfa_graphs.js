define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/debtor/graphs/e_pfa_graphs.html'
	, 'forms/pfa/debtor/graph/c_pfa_graph'
	, 'forms/pfa/debtor/graphs/x_graphs'
],
function (c_fastened, tpl, c_pfa_graph, x_graphs)
{
	return function()
	{
		var options =
		{
			field_spec:
			{
				  ряды_1: { controller: c_pfa_graph }
				, ряды_2: { controller: c_pfa_graph }
				, ряды_3: { controller: c_pfa_graph }

				, ряды_year_1: { controller: c_pfa_graph }
				, ряды_year_2: { controller: c_pfa_graph }
				, ряды_year_3: { controller: c_pfa_graph }
				, ряды_year_4: { controller: c_pfa_graph }
			}
		};

		var controller = c_fastened(tpl, options);

		controller.UseCodec(x_graphs());

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			var self = this;
			$(sel + ' input.align').change(function () { self.OnChangeAlign(); });
		}

		controller.OnChangeAlign = function ()
		{
			var sel = this.fastening.selector;
			var aligned = 'checked' == $(sel + ' input.align').attr('checked');
			if (aligned)
			{
				$(sel + ' div.cpw-ama-pfa-graphs')
					.addClass('years-aligned').removeClass('years-3');
				$(sel + " #cpw-pfa-graph-tabs" ).tabs({ active: 3 });
			}
			else
			{
				$(sel + ' div.cpw-ama-pfa-graphs')
					.addClass('years-3').removeClass('years-aligned');
				$(sel + " #cpw-pfa-graph-tabs" ).tabs({ active: 0 });
			}
		}

		return controller;
	}
});
