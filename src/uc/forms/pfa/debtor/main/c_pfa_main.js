define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/debtor/main/e_pfa_main.html'
	, 'forms/pfa/debtor/summary/c_pfa_summary'
	, 'forms/pfa/debtor/graphs/c_pfa_graphs'
	, 'forms/pfa/debtor/details/c_pfa_details'
	, 'forms/base/h_msgbox'
	, 'forms/pfa/debtor/doc/xv_pfa_doc'
	, 'forms/pfa/debtor/resolution/c_pfa_resolution'
],
function (c_fastened, tpl, c_pfa_summary, c_pfa_graphs, c_pfa_details, h_msgbox, xv_pfa_doc, c_pfa_resolution)
{
	return function(options_arg)
	{
		var base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'pfa.back');

		var controller = c_fastened(tpl);

		controller.base_url = base_url;
		controller.base_update_url = controller.base_url + '?action=debtor.crud&cmd=update';

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			this.RenderParts();
		}

		controller.RenderSummary = function ()
		{
			var self= this;
			var sel = this.fastening.selector;

			if (this.c_summary && null != this.c_summary)
			{
				this.c_summary.Destroy();
				delete this.c_summary;
			}
			this.c_summary = c_pfa_summary();
			this.c_summary.OnClickPart = function (part) { self.OnPart(part); }
			if (!this.model || !this.model.Body || null == this.model.Body)
			{
				this.c_summary.CreateNew(sel + ' div.summary');
			}
			else
			{
				this.c_summary.SetFormContent(this.model.Body);
				this.c_summary.Edit(sel + ' div.summary');
			}
		}

		controller.RenderGraphs = function ()
		{
			var sel = this.fastening.selector;
			if (this.c_graphs && null != this.c_graphs)
			{
				this.c_graphs.Destroy();
				delete this.c_graphs;
			}
			this.c_graphs = c_pfa_graphs();
			if (!this.model || !this.model.Body || null == this.model.Body)
			{
				this.c_graphs.CreateNew(sel + ' div.graphs');
			}
			else
			{
				this.c_graphs.SetFormContent(this.model.Body);
				this.c_graphs.Edit(sel + ' div.graphs');
			}
		}

		controller.RenderResolution = function ()
		{
			var sel = this.fastening.selector;
			if (this.c_resolution && null != this.c_resolution)
			{
				this.c_resolution.Destroy();
				delete this.c_resolution;
			}
			this.c_resolution = c_pfa_resolution();
			if (!this.model || !this.model.Body || null == this.model.Body)
			{
				this.c_resolution.CreateNew(sel + ' div.resolution');
			}
			else
			{
				this.c_resolution.SetFormContent(this.model.Body);
				this.c_resolution.Edit(sel + ' div.resolution');
			}
		}

		controller.RenderParts = function ()
		{
			this.RenderSummary();
			this.RenderGraphs();
			this.RenderResolution();
		}

		controller.OnPart= function(part)
		{
			var readonly= options_arg && options_arg.readonly;
			var с_details = c_pfa_details({readonly:readonly});
			с_details.SetFormContent(this.model.Body);
			с_details.part = part;
			var self= this;
			var btnOk = 'Сохранить детали финансового состояния';
			h_msgbox.ShowModal
				({
					  title: (readonly ? 'Детали' : 'Редактирование деталей') + ' финансового состояния'
					, controller: с_details
					, buttons: readonly ? ['Закрыть'] : [btnOk, 'Отмена']
					, id_div: 'cpw-pfa-details-form'
					, onclose: function(btn)
					{
						if (btn==btnOk)
							self.Update(с_details.GetFormContent());
					}
				});
		}

		controller.Update = function (body)
		{
			body.Заключение= xv_pfa_doc().Decode(body);
			var self= this;
			var url= this.base_update_url + '&id=' + this.model.id_Debtor;
			var v_ajax = h_msgbox.ShowAjaxRequest("Сохранение анкеты на сервере", url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: body
				, success: function (responce, textStatus)
				{
					if (null == responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else
					{
						self.model.Body = body;
						self.RenderParts();
					}
				}
			});
		}

		return controller;
	}
});
