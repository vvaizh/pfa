define([
	  'forms/base/h_spec'
]
, function (h_spec)
{
	var items_spec = {

		controller:
		{
			"main":
			{
				path: 'forms/pfa/debtor/main/c_pfa_main'
				, title: 'Главная форма'
			}
			,"summary":
			{
				path: 'forms/pfa/debtor/summary/c_pfa_summary'
				, title: 'Навигационная панель (сводная таблица)'
			}
			,"graphs":
			{
				path: 'forms/pfa/debtor/graphs/c_pfa_graphs'
				, title: 'Графики, иллюстрирующие текущее финансовое состояние'
			}
			,"graph":
			{
				path: 'forms/pfa/debtor/graph/c_pfa_graph'
				, title: 'График, иллюстрирующий финансовое на период'
			}
			,"details":
			{
				path: 'forms/pfa/debtor/details/c_pfa_details'
				, title: 'Редактирование деталей финансового состояния'
			}
			,"debtor-login":
			{
				path: 'forms/pfa/debtor/login/c_pfa_debtor_login'
				, title: 'Вход в кабинет должника (в анкету)'
			}
			,"doc":
			{
				path: 'forms/pfa/debtor/doc/c_pfa_doc'
				, title: 'Результат анализа в виде документа'
			}
			,"resolution":
			{
				path: 'forms/pfa/debtor/resolution/c_pfa_resolution'
				, title: 'Резолюция вывод и рекомендации по результатам анализа'
			}
		}

		, content:
		{
			"graph-example-1":
			{
				path: 'txt!forms/pfa/debtor/graph/tests/contents/example1.json.txt'
				, title: 'График, иллюстрирующий финансовое на период'
			}
			,"debtor-logged-1":
			{
				path: 'txt!forms/pfa/debtor/main/tests/contents/logged-debtor1.json.txt'
				, title: 'Авторизованная анкета 1'
			}
			, "debtor-logged-2":
			{
				path: 'txt!forms/pfa/debtor/main/tests/contents/logged-debtor2.json.txt'
				, title: 'Авторизованная анкета 2'
			}
			,"resolution-example-ok": 
			{
				path: 'txt!forms/pfa/debtor/resolution/tests/contents/ok.json.txt'
			}
			,"resolution-example-volatile": 
			{
				path: 'txt!forms/pfa/debtor/resolution/tests/contents/volatile.json.txt'
			}
			,"resolution-example-critical": 
			{
				path: 'txt!forms/pfa/debtor/resolution/tests/contents/critical.json.txt'
			}
		}
	};
	return h_spec.combine(items_spec);
});