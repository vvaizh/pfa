define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/debtor/summary/e_pfa_summary.html'
	, 'forms/pfa/calc/x_debtor_summary'
	, 'txt!forms/pfa/calc/tests/debtor_summary/master-00min.json.etalon.txt'
	, 'forms/base/h_number_format'
],
function (c_fastened, tpl, x_summary, smaster_00min, h_number_format)
{
	return function()
	{
		var controller = c_fastened(tpl, {summary0: JSON.parse(smaster_00min), h_number_format: h_number_format });
		
		controller.UseCodec(x_summary());
		styles = document.styleSheets;
		for (i = 0; i < styles.length; i++){
			if (styles[i].href && styles[i].href.indexOf('wbt.css') != -1){
				var isTest = true;
				break;
			}
		}

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			var self = this;
			
			
			if (!isTest){
				$('.cpw-ama-pfa-summary a').attr('data-part', '');
				$('.cpw-ama-pfa-summary a').css('pointer-events', 'none');
			}
			else {
				$(sel + ' a').click(function (e) { self.OnClick(e); });
			}
		}

		controller.OnClick = function (e)
		{
			e.preventDefault();
			var part = $(e.target).attr('data-part');
			if (part != null && part != '' && this.OnClickPart)
				this.OnClickPart(part);
		}

		return controller;
	}
});
