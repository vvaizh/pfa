define(['forms/pfa/debtor/login/c_pfa_debtor_login'],
function (CreateController)
{
	var form_spec =
	{
		CreateController: CreateController
		, key: 'debtor'
		, Title: 'Вход в личный кабинет системы финансовой помощи гражданам'
	};
	return form_spec;
});
