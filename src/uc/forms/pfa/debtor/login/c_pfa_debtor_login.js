define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/debtor/login/e_pfa_debtor_login.html'
	, 'forms/base/h_msgbox'
	, 'forms/pfa/debtor/main/c_pfa_main'
],
function (c_fastened, tpl, h_msgbox, c_pfa_main)
{
	return function(options_arg)
	{
		var controller = c_fastened(tpl);

		controller.base_url = ((options_arg && options_arg.base_url) ? options_arg.base_url : 'pfa.back');
		controller.base_login_url = controller.base_url + '?action=debtor.login';
		
		var base_Render = controller.Render;
		
		controller.decode = function(input) {
			input = input
				.replace(/-/g, '+')
				.replace(/_/g, '/');
			var pad = input.length % 4;
			if(pad) {
			  if(pad === 1) {
				alert('InvalidLengthError: Input base64url string is the wrong length to determine padding');
			  }
			  input += new Array(5-pad).join('=');
			}
			return input;
		}
				
		controller.urlParam = function(name){
			var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
			if (results == null){
			   return null;
			}
			else {
			   return decodeURI(results[1]) || 0;
			}
		}
		
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);

			this.form_selector = sel + ' > div.cpw-ama-pfa-debtor-login > div.login-form';

			var self= this;
			$(this.form_selector + ' button.login').click(function () { self.OnLogin(); });

			var auth = this.urlParam('auth');
			if (auth){
				var temp = this.decode(auth); 
				temp = atob(auth);
				if (temp){
					var credentials = temp.split(',');
					var login = credentials[0];
					var pw = credentials[1];
					var id_Abonent = credentials[2]
					self.OnLogin(login, pw, id_Abonent);
				}
			}
		}

		controller.OnLogin = function (login, password, id_Abonent)
		{
			var sel = this.fastening.selector;
			if (!login){
				var login = $(this.form_selector + ' input.login').val();
			}
			if (!password){
				var password = $(this.form_selector + ' input.password').val();
			}
			if (!id_Abonent){
				var id_Abonent = this.model.id_Abonent;
			}
			var self= this;
			var url= this.base_login_url;
			var v_ajax = h_msgbox.ShowAjaxRequest("Отправка запроса на доступ на сервер", url);
			v_ajax.ajax({
				dataType: "json"
				, type: 'POST'
				, cache: false
				, data: {login:login,password:password, id_Abonent:id_Abonent}
				, success: function (responce, textStatus)
				{
					if (null == responce)
					{
						v_ajax.ShowAjaxError(responce, textStatus);
					}
					else
					{
						self.OnLoged(responce);
					}
				}
			});
		}

		controller.OnLoged = function (logged_data)
		{
			var sel = this.fastening.selector;

			this.main_controller = c_pfa_main({ base_url: this.base_url });
			this.main_controller.SetFormContent(logged_data);

			$(this.form_selector).hide();

			var logged_selector= sel + ' > div.cpw-ama-pfa-debtor-login > div.logger-part';
			$(logged_selector).show();

			this.main_controller.Edit(logged_selector);
		}

		return controller;
	}
});
