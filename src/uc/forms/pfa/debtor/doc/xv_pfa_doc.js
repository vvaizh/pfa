define([
	'tpl!forms/pfa/debtor/doc/v_pfa_doc.html'
	, 'tpl!forms/pfa/base/v_test_a4_portrait.html'
	, 'forms/base/h_times'
]
, function (view, v_test_a4, h_times)
{
	return function ()
	{
		var res = {};

		res.Decode= function(json_txt)
		{
			var json_obj = ('string'!=typeof json_txt) ? json_txt : JSON.parse(json_txt);
			var report_html = view({
				анкета: json_obj
				, h_times: h_times
			});
			var layout_html = v_test_a4({});
			return layout_html.replace('test-content', report_html);
		}

		return res;
	}

});
