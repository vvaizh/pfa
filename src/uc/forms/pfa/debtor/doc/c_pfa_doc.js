define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/debtor/doc/v_pfa_doc.html'
],
function (c_fastened, tpl)
{
	return function()
	{
		var controller = c_fastened(function (obj)
			{
				var tpl_obj = { анкета: obj.value() };
				var html = tpl(tpl_obj);
				return html;
			});
		return controller;
	}
});
