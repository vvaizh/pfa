define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/debtor/graph/e_pfa_graph.html'
	, 'forms/pfa/calc/x_debtor_prognosis'
	, 'forms/base/h_number_format'
],
function (c_fastened, tpl, x_graph, h_number_format)
{
	return function()
	{
		var controller = c_fastened(tpl, {h_number_format:h_number_format});

		controller.UseCodec(x_graph());

		return controller;
	}
});
