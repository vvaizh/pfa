define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/pfa/debtor/details/e_pfa_details.html'
	, 'forms/pfa/brief/collections/dependents/c_dependents'
	, 'forms/pfa/brief/collections/alimonies/c_alimonies'
	, 'forms/pfa/brief/collections/spouses/c_spouses'
	, 'forms/pfa/brief/collections/expenses/c_expenses'
	, 'forms/pfa/brief/collections/debts/c_debts'
	, 'forms/pfa/brief/collections/income/c_income'
	, 'forms/pfa/brief/collections/properties/c_properties'
],
function (c_fastened, tpl, c_dependents, c_alimonies, c_spouses, c_expenses, c_debts, c_income, c_properties)
{
	return function(options_arg)
	{
		var options=
		{
			field_spec:
			{
				  Расходы: { controller: function () { return c_expenses({ readonly: options_arg && options_arg.readonly }); } }
				, Иждивенцы: { controller: function () { return c_dependents({ readonly: options_arg && options_arg.readonly }); } }
				, Выплачиваемые_алименты: { controller: function () { return c_alimonies({ readonly: options_arg && options_arg.readonly }); } }
				, Супруги: { controller: function () { return c_spouses({ readonly: options_arg && options_arg.readonly }); } }
				, Долги: { controller: function () { return c_debts({ readonly: options_arg && options_arg.readonly }); } }
				, Доходы: { controller: function () { return c_income({ readonly: options_arg && options_arg.readonly }); } }
				, Имущество: { controller: function () { return c_properties({ readonly: options_arg && options_arg.readonly }); } }
			}
		};

		var controller = c_fastened(tpl, options);

		controller.size = { width: 950, height: 600 };

		var base_Render = controller.Render;
		controller.Render = function (sel)
		{
			base_Render.call(this, sel);
			if (this.part)
			{
				var activate_tab = function (num)
				{
					$(sel + ' div#cpw-pfa-details-tabs').tabs({ active: num });
				}
				switch (this.part)
				{
					case 'Расходы на погашения':
					case 'Расходы на проценты':
					case 'Долги': activate_tab(0); break;
					case 'Расходы на имущество':
					case 'Имущество': activate_tab(1); break;
					case 'Деньги': activate_tab(1); break;
					case 'Доходы': activate_tab(2); break;
					case 'Расходы на жизнь': activate_tab(3); break;
				}
			}
			
		}

		return controller;
	}
});
