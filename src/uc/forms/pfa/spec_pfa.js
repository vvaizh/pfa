define([
	  'forms/base/h_spec'
	, 'forms/pfa/brief/spec_pfa_brief'
	, 'forms/pfa/debtor/spec_pfa_debtor'
	, 'forms/pfa/abonent/spec_pfa_abonent'
]
, function (h_spec, spec_pfa_brief, spec_pfa_debtor, spec_pfa_abonent)
{
	var items_spec = {

		controller:
		{
			"root":
			{
				path: 'forms/pfa/root/c_pfa_root'
				, title: 'Страница суперадминистратора'
			}
		}

		, content:
		{
			"root-example1":
			{
				path: 'txt!forms/pfa/root/tests/contents/example1.json.txt'
				, title: 'Абоненты для суперадминистратора'
			}
		}
	};
	return h_spec.combine(spec_pfa_brief, spec_pfa_debtor, spec_pfa_abonent, items_spec);
});