set names utf8;

select 'insert into abonent' as '';
insert into `abonent` set `id_Abonent`= 1, `Title`= 'Рестарт', `URL`= 'http://restart.ru', `AbonentLogin`= '1', `AbonentPassword`= '1';
insert into `abonent` set `id_Abonent`= 2, `Title`= 'Не Рестарт', `URL`= 'http://ne-restart.ru', `AbonentLogin`= '2', `AbonentPassword`= '2';
insert into `abonent` set `id_Abonent`= 3, `Title`= 'Совсем Не Рестарт', `URL`= 'http://sovsem-ne-restart.ru', `AbonentLogin`= '2', `AbonentPassword`= '2';

select 'insert into debtor' as '';
insert into `debtor` set `id_Debtor`= 1, `DebtorEmail`= 'a@a.a', `DebtorPassword`= '1', `id_Abonent`= 1, `lastName`= 'Иванов', `firstName`= 'Иван', `middleName`= 'Иванович', `sum_debt`= 0, `sum_expense`= 0, `sum_income`= 0, `sum_property`= 0, `State`= 'a', `TimeCreated`= '2020-01-02T10:01', `Body`= compress(''), `DocBody`= 'test1';
insert into `debtor` set `id_Debtor`= 2, `DebtorEmail`= 'b@b.b', `DebtorPassword`= '1', `id_Abonent`= 2, `lastName`= 'Петров', `firstName`= 'Пётр', `middleName`= 'Петрович', `sum_debt`= 0, `sum_expense`= 0, `sum_income`= 0, `sum_property`= 0, `State`= 'a', `TimeCreated`= '2020-01-02T10:01', `Body`= compress(''), `DocBody`= 'test2';
insert into `debtor` set `id_Debtor`= 3, `DebtorEmail`= 'с@с.с', `DebtorPassword`= '1', `id_Abonent`= 1, `lastName`= 'Сидоров', `firstName`= 'Сидор', `middleName`= 'Сидорович', `sum_debt`= 0, `sum_expense`= 0, `sum_income`= 0, `sum_property`= 0, `State`= 'a', `TimeCreated`= '2020-01-02T10:01', `Body`= compress(''), `DocBody`= 'test3';

select 'insert into email_message' as '';
insert into `email_message` set `id_Email_Message`= 1, `EmailType`= 'a', `TimeDispatch`= '2019-10-15T10:01', `RecipientEmail`= 'a@a.a', `RecipientType`= 'd', `RecipientId`= 1, `Details`= compress('{\n  \"Subject\": \"subject1\",\n  \"Receiver\": \"Иванов Иван Иванович\",\n  \"Body\": \"body1\"\n}');
insert into `email_message` set `id_Email_Message`= 2, `EmailType`= 'a', `TimeDispatch`= '2019-10-16T10:01', `RecipientEmail`= 'b@b.b', `RecipientType`= 'd', `RecipientId`= 2, `Details`= compress('{\n  \"Subject\": \"subject2\",\n  \"Receiver\": \"Пётр Петрович Петров\",\n  \"Body\": \"body2\"\n}');
insert into `email_message` set `id_Email_Message`= 3, `EmailType`= 'a', `TimeDispatch`= '2019-10-17T10:01', `RecipientEmail`= 'c@c.c', `RecipientType`= 'd', `RecipientId`= 3, `Details`= compress('{\n  \"Subject\": \"subject3\",\n  \"Receiver\": \"Сидоров С С\",\n  \"Body\": \"body3\"\n}');

select 'insert into email_attachment' as '';
insert into `email_attachment` set `id_Email_Attachment`= 1, `id_Email_Message`= 2, `FileName`= 'a.txt', `Content`= 'content1';
insert into `email_attachment` set `id_Email_Attachment`= 2, `id_Email_Message`= 3, `FileName`= 'b.txt', `Content`= 'content2';
insert into `email_attachment` set `id_Email_Attachment`= 3, `id_Email_Message`= 3, `FileName`= 'c.txt', `Content`= 'content3';

select 'insert into email_sender' as '';
insert into `email_sender` set `id_Email_Sender`= 1, `SenderServer`= 'server1', `SenderUser`= 'user1';
insert into `email_sender` set `id_Email_Sender`= 2, `SenderServer`= 'server2', `SenderUser`= 'user2';

