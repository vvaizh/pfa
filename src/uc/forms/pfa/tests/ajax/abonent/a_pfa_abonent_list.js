﻿define([
	  'forms/base/codec/url/codec.url'
	, 'forms/base/codec/url/codec.url.args'
	, 'forms/pfa/tests/d_pfa'
	, 'forms/base/ql'
],
function (codec_url, codec_url_args, db, ql)
{
	var url_prefix = 'pfa.back?action=abonent.list';

	var transport = { options: { url_prefix: url_prefix } };

	transport.prepare_try_to_prepare_send_abort = function ()
	{
		var self = this;
		return function (options, originalOptions, jqXHR)
		{
			if (self.options && 0 == options.url.indexOf(self.options.url_prefix))
			{
				var send_abort =
				{
					send: function (headers, completeCallback)
					{
						var decoded_url = codec_url().Decode(options.url);
						var args = codec_url_args().Decode(decoded_url);

						var items = ql.map(db.abonent, function (d)
						{
							return {
								id_Abonent: d.id_Abonent
								,Наименование: d.Title
								,URL: d.URL
							};
						});

						completeCallback(200, 'success', { text: JSON.stringify(items) });
					}
					, abort: function ()
					{
					}
				}
				return send_abort;
			}
		}
	}

	return transport;
});