define
(
	[
		  'forms/base/ajax/ajax-collector'

		, 'forms/pfa/tests/ajax/debtor/a_pfa_debtor_jqgrid.js'
		, 'forms/pfa/tests/ajax/debtor/a_pfa_debtor_login.js'
		, 'forms/pfa/tests/ajax/debtor/a_pfa_debtor_crud.js'

		, 'forms/pfa/tests/ajax/abonent/a_pfa_abonent_list.js'
		, 'forms/pfa/tests/ajax/abonent/a_pfa_abonent_login.js'

		, 'forms/pfa/tests/ajax/probili/a_probili_company.js'
	],
	function (ajax_collector)
	{
		return ajax_collector(Array.prototype.slice.call(arguments, 1));
	}
);