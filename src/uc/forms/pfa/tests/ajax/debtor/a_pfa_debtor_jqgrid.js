﻿define([
	  'forms/pfa/tests/d_pfa'
	, 'forms/base/ajax/ajax-jqGrid'
	, 'forms/base/codec/codec.copy'
	, 'forms/base/ql'
	, 'forms/base/codec/url/codec.url'
	, 'forms/base/codec/url/codec.url.args'
], function (db, ajax_jqGrid, codec_copy, ql, codec_url, codec_url_args)
{
	var transport = ajax_jqGrid();

	transport.options.url_prefix = 'pfa.back?action=debtor.jqgrid';

	transport.rows_all = function (url)
	{
		var decoded_url = codec_url().Decode(url);
		var args = codec_url_args().Decode(decoded_url);
		var ccodec_copy = codec_copy();

		return ql.from(db.debtor, 'd')
			.where(function (r) { return r.d.id_Abonent==args.id_Abonent; })
			.map(function (r)
			{
				return ccodec_copy.Copy({
					id_Debtor : r.d.id_Debtor
					, firstName: r.d.firstName
					, lastName: r.d.lastName
					, middleName: r.d.middleName
				});
			});
	}

	return transport.prepare_try_to_prepare_send_abort();
});