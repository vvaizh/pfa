﻿define([
	'forms/pfa/tests/d_pfa'
	, 'forms/base/ajax/ajax-CRUD'
	, 'forms/base/codec/codec.copy'
	, 'forms/base/ql'
	, 'forms/pfa/tests/ajax/debtor/h_ajax_debtor'
	, 'forms/base/codec/url/codec.url'
	, 'forms/base/codec/url/codec.url.args'
]
, function (db, ajax_CRUD, codec_copy, ql, h_ajax_debtor, codec_url, codec_url_args)
{
	var transport = ajax_CRUD();

	transport.options.url_prefix = 'pfa.back?action=debtor.crud';

	transport.on_create = function (options,parsed_args)
	{
		var data_decoded_as_url = codec_url().Decode(options.data);
		var data_decoded_as_args = codec_url_args().Decode(data_decoded_as_url);
		var data = this.create(data_decoded_as_args, parsed_args);
		var res = { ok: true };
		if (data)
			res.data = data;
		return res;
	}

	transport.create = function (d_info)
	{
		var debtor= ql.insert_with_next_id(db.debtor, 'id_Debtor', {
			id_Abonent: d_info.id_Abonent

			, lastName: d_info.Персональные_данные.Фамилия
			, firstName: d_info.Персональные_данные.Имя
			, middleName: d_info.Персональные_данные.Отчество

			, DebtorEmail: d_info.Персональные_данные.Email
			, DebtorPassword: '1'

			, Body: d_info
		});

		return h_ajax_debtor(debtor);
	}

	transport.update = function (id, body)
	{
		var debtor= ql.find_first_or_null(db.debtor, function (d)
		{
			return id==d.id_Debtor;
		});
		debtor.Body= body;
	}

	transport.delete = function (ids)
	{
	}

	transport.read = function (id)
	{
		var debtor= ql.find_first_or_null(db.debtor, function (d)
		{
			return id==d.id_Debtor;
		});

		return h_ajax_debtor(debtor);
	}

	return transport.prepare_try_to_prepare_send_abort();
});