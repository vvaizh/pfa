﻿define([
	  'forms/base/codec/url/codec.url'
	, 'forms/base/codec/url/codec.url.args'
	, 'forms/pfa/tests/d_pfa'
	, 'forms/base/ql'
	, 'forms/pfa/tests/ajax/debtor/h_ajax_debtor'
],
function (codec_url, codec_url_args, db, ql, h_ajax_debtor)
{
	var url_prefix = 'pfa.back?action=debtor.login';

	var transport = { options: { url_prefix: url_prefix } };

	transport.prepare_try_to_prepare_send_abort = function ()
	{
		var self = this;
		return function (options, originalOptions, jqXHR)
		{
			if (self.options && 0 == options.url.indexOf(self.options.url_prefix))
			{
				var send_abort =
				{
					send: function (headers, completeCallback)
					{
						var scodec_url = codec_url();
						var scodec_url_args = codec_url_args();

						var args = ('string' != typeof options.data)
							? options.data : scodec_url_args.Decode(scodec_url.Decode(options.data));

						var debtor= ql.find_first_or_null(db.debtor, function (d)
						{
							return d.DebtorEmail == args.login && d.DebtorPassword == args.password;
						});

						var res= h_ajax_debtor(debtor);

						completeCallback(200, 'success', { text: JSON.stringify(res) });
					}
					, abort: function ()
					{
					}
				}
				return send_abort;
			}
		}
	}

	return transport;
});