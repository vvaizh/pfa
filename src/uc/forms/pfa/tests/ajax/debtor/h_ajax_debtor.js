﻿define([
	'txt!forms/pfa/brief/steps/master/tests/contents/01sav.json.etalon.txt'
	,'txt!forms/pfa/brief/steps/master/tests/contents/example1.json.txt'
]
, function (master_01sav, master_example1)
{
	return function (debtor)
	{
		if (null == debtor)
		{
			return null;
		}
		else
		{
			var res= {
				id_Debtor: debtor.id_Debtor
				, Фамилия: debtor.lastName
				, Имя: debtor.firstName
				, Отчество: debtor.middleName
				, Body: debtor.Body
			};
			if ('' == res.Body)
			{
				var i = parseInt(res.id_Debtor);
				switch (i % 2)
				{
					case 0: res.Body = master_01sav; break;
					case 1: res.Body = master_example1; break;
				}
			}
			return res;
		}
	}
});