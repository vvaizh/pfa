﻿define([
	'txt!forms/pfa/tests/pfa_db_test.json.txt'
	, 'forms/pfa/tests/d_pfa_read'
],
function (content_ini_txt, d_pfa_read)
{
	d_pfa_read.content= JSON.parse(content_ini_txt);
	return d_pfa_read.content;
});