﻿define([
	  'forms/base/log'
	, 'forms/pfa/calc/x_debtor_prognosis'
	, 'forms/pfa/calc/x_debtor_summary'
]
,function (GetLogger, x_debtor_prognosis, x_debtor_summary)
{
	/*
	 Расчётный модуль, для расчёта заключения о финансовом состоянии дожника
	 На вход:  анкета должника
	 На выход: заключение в виде полей:
		- состояние: критическое или неустойчивое или стабильное или неопределённое
		- время_непсособности_платить: месяц, год
		- рекомендации: массив строк
	 */
	var log= GetLogger('x_debtor_resolution');
	return function ()
	{
		var codec = {};

		codec.Найти_время_неспособности_платить = function (forecast)
		{
			if (forecast && null != forecast && forecast.length)
			{
				for (var i = 0; i < forecast.length; i++)
				{
					var r = forecast[i];
					if (!r.денег || r.денег < 0)
						return { год: r.год, месяц: r.месяц };
				}
			}
		}

		codec.Сформулировать_рекомендации = function (anketa, forecast, summary, состояние, рекомендации)
		{

		}

		codec.Decode = function (data)
		{
			if ('string' == typeof data)
				data = JSON.parse(data);

			if (data.состояние)
				return data;

			var anketa= data;

			var res = {
				состояние: 'неопределённое'
				, рекомендации: [
					'найти дополнительные источника дохода.'
				]
			};

			var forecast = x_debtor_prognosis().Decode(anketa);

			var время_непсособности_платить = this.Найти_время_неспособности_платить(forecast);
			if (время_непсособности_платить && null != время_непсособности_платить)
			{
				res.состояние = "критическое";
				res.время_непсособности_платить= время_непсособности_платить;
			}
			else
			{
				var summary = x_debtor_summary().Decode(anketa);
				var приращение = summary.Среднемесячно.Приращение;
				res.состояние = (приращение > 0) ? "стабильное" : "неустойчивое";
				this.Сформулировать_рекомендации(anketa, forecast, summary, res.состояние, res.рекомендации);
			}

			return res;
		}

		return codec;
	}
});
