define([
	  'forms/base/codec/datetime/h_codec.datetime'
	, 'forms/base/log'
],
function (h_codec_datetime, GetLogger)
{
	/*
	 Расчётный модуль, для расчёта платежей по кредиту
	 На вход:  описание кредита
	 На выход: массив платежей с полями (Дата,Размер_платежа,Проценты)
	 */
	return function ()
	{
		var log= GetLogger('x_credit_payments');
		var res = {};
		
		res.Decode= function(json_txt)
		{
			var c = ('string' != typeof json_txt) ? json_txt : JSON.parse(json_txt);
			var платежи = [];
			var date = new Date();
			var date = new Date(date.getFullYear(), date.getMonth(), 15);
			do
			{
				var текущая_обрабатываемая_дата = new Date(date.setMonth(date.getMonth()+1));
				var date_str = (date.getMonth()+1) + '.' + date.getFullYear();
				var amount = 0;
				var penalties = 0;
				finalPayment = new Date(c.Расход_до);
				finalPayment = new Date(finalPayment.setMonth(finalPayment.getMonth()+1));
				if (finalPayment > текущая_обрабатываемая_дата){
						if (c.Пени){
							penalties = parseFloat(c.Пени);
						}
						if (c.Платежи){
							amount = parseFloat(c.Платежи);
						}
				} else {
						c.Сумма_задолженности = 0;
				}
				var платеж = {
					  'Месяц': date_str,
					  'Сумма_задолженности': c.Сумма_задолженности,
					  'Ежемесячный_платеж': amount,
					  'Пени': penalties,
					  'Сумма_платежей': penalties + amount
					};
				платежи.push(платеж);
			}
			while (платежи.length < 36);
			return платежи;
		}

		return res;
	}

});
