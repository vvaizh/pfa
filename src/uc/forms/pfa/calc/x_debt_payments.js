define([
	  'forms/pfa/calc/x_debt_credit'
	, 'forms/pfa/calc/x_credit_payments'
	, 'forms/base/log'
],
function (x_debt_credit, x_credit_payments, GetLogger)
{
	/*
	 Расчётный модуль, для расчёта платежей по описанию задолженности
	 На вход:  описание задолженности
	 На выход: массив платежей с полями (Дата,Размер_платежа,Проценты)
	 */
	var log= GetLogger('x_debt_payments');
	return function ()
	{
		var res = {};

		var xdebt_credit = x_debt_credit();
		var xcredit_payments= x_credit_payments();

		res.Decode= function(json_txt)
		{
			var debt = ('string' != typeof json_txt) ? json_txt : JSON.parse(json_txt);
			var credit = xdebt_credit.Decode(debt);
			var платежи = xcredit_payments.Decode(credit);
			return платежи;
		}
		
		res.GetPayments= function(debt)
		{
				
		}

		return res;
	}

});
