define(function ()
{
	/*
	 Расчётный модуль, для преобразования описания задолженности в 
	   описание кредита, необходим для расчёта платежей по кредиту или задолженности
	 На вход:  описание долга
	 На выход: опиcание кредита
	 */
	return function ()
	{
		var res = {};

		res.Decode= function(json_txt)
		{
			var d = ('string' != typeof json_txt) ? json_txt : JSON.parse(json_txt);
			
			var amount = 0;
			var payment = parseFloat(0);
			var penalties = parseFloat(0);
			var date = new Date();
			/**
			Определение крайней даты расходов
			**/
			if (d.Кредит && d.Кредит.Дата_погашения){
					date_pcs = d.Кредит.Дата_погашения.split(".");
					date = new Date(date_pcs[1], date_pcs[0]-1, '01');
					payment = parseFloat(d.Кредит.Ежемесячный_платёж);
			}
			else if(d.Долг && d.Долг.Месяц_возврата_долга){
					date_pcs =  d.Долг.Месяц_возврата_долга.split(".");
					date = new Date(date_pcs[1], date_pcs[0]-1, '01');
					penalties = parseFloat(d.Долг.Пени);
			}
			else if(d.Долг_с_кредитором && d.Долг_с_кредитором.Месяц_возврата_долга){
					date_pcs =  d.Долг_с_кредитором.Месяц_возврата_долга.split(".");
					date = new Date(date_pcs[1], date_pcs[0]-1, '01');
					penalties = parseFloat(d.Долг_с_кредитором.Пени);
			}
			else if(d.Долг_с_получателем && d.Долг_с_получателем.Месяц_возврата_долга){
					date_pcs =  d.Долг_с_получателем.Месяц_возврата_долга.split(".");
					date = new Date(date_pcs[1], date_pcs[0]-1, '01');
					penalties = parseFloat(d.Долг_с_получателем.Пени);
			};
			
			if (penalties.length <1 || penalties === null || isNaN(penalties)){
				penalties = parseFloat(0);
			};
			
			if (d.Сумма_задолженности){
				amount = d.Сумма_задолженности;
			};
			
			var expense = {
					Сумма_задолженности: parseFloat(amount),
					Расход_до: date,
					Пени: penalties,
					Платежи: payment,
					Сумма_расходов: parseFloat(payment) + penalties
			};
			return expense;
		};
		
				
		res.getPaymentAmountByMonthDifference = function(debt){
			var date1 = new Date();
			str = debt.Долг.Месяц_возврата_долга;
			if (str.length === 7) {
				date2_pcs =  str.split(".");
				var date2 = new Date(date2_pcs[1], date2_pcs[0]-1, '01');
			}
			else {
				date2_pcs =  str.split(".");
				var date2 = new Date(date2_pcs[2], date2_pcs[1]-1, date2_pcs[0]);
			}
			var year_diff = date2.getFullYear() - date1.getFullYear();
			var month_diff = date2.getMonth() + (year_diff*12) - date1.getMonth();

			if (month_diff < 0){
				return 0;
			}
			to_pay = 0;
			if (debt.Сумма_задолженности)
				to_pay = debt.Сумма_задолженности;
			return (to_pay/month_diff).toFixed(2)
		};
		
		return res;
	}
});
