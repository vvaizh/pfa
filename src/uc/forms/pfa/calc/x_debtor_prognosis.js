﻿define([
	  'forms/pfa/calc/x_debtor_summary'
	, 'forms/base/codec/datetime/h_codec.datetime'
]
,function (x_summary, h_codec_datetime)
{
	/*
	 Расчётный модуль, для расчёта помесячного прогноза финансового состояния 
	   должника на 3 года после даты заполнения анкеты
	 На вход:  анкета должника
	 На выход: массив записей с полями (дата,год,месяц,долгов,денег)
	 */
	return function ()
	{
		var codec = {};

		var monthes= ['янв','фев','мар','апр','май','июн','июл','авг','сен','окт','ноя','дек'];
		
		/*
		Фикс indexOf для WScript
		*/
		if ( typeof(Array.prototype.indexOf) === 'undefined' ) {
			Array.prototype.indexOf = function(item, start) {        
				var length = this.length
				start = typeof(start) !== 'undefined' ? start : 0
				for (var i = start; i < length; i++) {
					if (this[i] === item) return i
				}
				return -1
			}
		}
		
		codec.Расчитать_месяц = function (d, data, prev, diff)
		{
			var r = {};
			var Задолженность = 0;
			r.дата = d;
			r.год = d.getFullYear();
			r.месяц = monthes[d.getMonth()];
			if (prev.учтены && prev.учтены.length){
				r.учтены = prev.учтены;
			}
			else {
				r.учтены = [];
			}
			денег = prev.денег + diff.денег;
			if (data.Долги){
				for (var i = 0; i < data.Долги.length; i++)
				if (data.Долги[i] && !data.Долги[i].Кредит && !data.Долги[i].Исполнительное_производство){

					var текущая_обрабатываемая_дата = d;
					if (data.Долги[i].Долг){
						var finalMonth = data.Долги[i].Долг.Месяц_возврата_долга;
						
						date_pcs =  finalMonth.split(".");
						finalDate = new Date(date_pcs[1], date_pcs[0]-1, '01');
						
						if (finalDate <= текущая_обрабатываемая_дата){
							if (r.учтены.indexOf(i) == '-1'){
								r.учтены.push(i);
								денег = денег - data.Долги[i].Сумма_задолженности;
							}
						}
					}
					else if (data.Долги[i].Долг_с_кредитором){
						var finalMonth = data.Долги[i].Долг_с_кредитором.Месяц_возврата_долга;
						
						date_pcs =  finalMonth.split(".");
						finalDate = new Date(date_pcs[1], date_pcs[0]-1, '01');
						
						if (finalDate <= текущая_обрабатываемая_дата){
							if (r.учтены.indexOf(i) == '-1'){
							r.учтены.push(i);
							денег = денег - data.Долги[i].Сумма_задолженности;
							}
						}
					}
					else if (data.Долги[i].Долг_с_получателем){
						var finalMonth = data.Долги[i].Долг_с_получателем.Месяц_возврата_долга;
						
						date_pcs =  finalMonth.split(".");
						finalDate = new Date(date_pcs[1], date_pcs[0]-1, '01');
						
						if (finalDate <= текущая_обрабатываемая_дата){
							if (r.учтены.indexOf(i) == '-1'){
								r.учтены.push(i);
								денег = денег - data.Долги[i].Сумма_задолженности;
							}
						}
					}
					if (r.учтены.indexOf(i) == '-1'){
						Задолженность = parseFloat(Задолженность) + parseFloat(data.Долги[i].Сумма_задолженности);
					}
				}
			}
			if (денег>0){
				r.денег = денег;
			}
			r.долгов = Задолженность;
			if (r.долгов < 0){r.долгов = 0;}
			return r;
		};

		codec.Текущая_дата = function (Время_заполнения)
		{
			var dt = h_codec_datetime.Encode(Время_заполнения, h_codec_datetime.format.ru);

			var d = new Date
			(
				dt.date.YYYY
				, dt.date.MM-1
				, dt.date.DD
				, 12
				, 0
				, 0
				, 0
			);

			return d;
		};

		codec.Decode = function (data)
		{
			if ('string' == typeof data)
				data = JSON.parse(data);

			if (data.length)
				return data;

			var d = this.Текущая_дата(data.Время_заполнения);

			var s= x_summary().Decode(data);

			var prev = {
				денег: s.Баланс.Активы.Составляющие.Наличные
				, долгов: s.Баланс.Пассивы.Составляющие.Долги
			};
			var diff = {
				денег: s.Среднемесячно.Приращение
				, долгов: -s.Среднемесячно.Расходы.Составляющие.На_погашения
			};

			var rr= [];
			for (var i = 0; i < 12*4; i++)
			{
				prev= this.Расчитать_месяц(d, data, prev, diff);
				rr.push(prev);
				d= new Date(d);
				d.setMonth(d.getMonth()+1);
			}

			return rr;
		};

		return codec;
	};
});
