@echo *******************************************************

@echo prepare create_sql
@call %~dp0\create_sql.bat

@set DBName=pfadevel
@echo prepare empty database %DBName%
@set SQL_DROP_CREATE=drop database %DBName%; create database %DBName% default character set utf8;
@echo %SQL_DROP_CREATE% | %~dp0\run_mysql.bat
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!

@echo execute create.sql
@call %~dp0\run_mysql.bat < %~dp0\sql\create.sql
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!

@echo execute migrations.sql
@call %~dp0\run_mysql.bat < %~dp0\sql\migrations.sql
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!

@echo execute datamart.etalon.sql 
@call %~dp0\run_mysql.bat < %~dp0\..\..\uc\forms\pfa\tests\pfa_db_test.etalon.sql
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!

@echo .
@echo fill debtor.Body
@set PHP=php.exe 
@call %PHP% %~dp0\..\tests\db\fill_body.php

