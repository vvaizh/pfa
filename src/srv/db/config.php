<?

global $default_db_options;
$default_db_options= (object)array(
	'host'=> 'localhost'
	,'user'=>'root'
	,'password'=> 'root'
	,'dbname'=> 'pfadevel'
	,'charset'=> 'utf8'
);

global $email_settings;
$email_settings= (object)array(
	 'smtp_server'=>     '?????'
	,'smtp_user'=>       '?????'
	,'smtp_password'=>   '?????'
	,'smtp_port'=>       465
	,'smtp_from_email'=> 'info@russianit.ru'
	,'smtp_from_name'=>  'RIT'
	//,'smtp_secure'=>'ssl'
	,'enabled'=>false
);

global $output_settings;
$output_settings= (object)array(
	'log_file_name'=> 'C:\Apache\htdocs\pfa-master\pfa\src\srv\log.txt'
	,'trace_methods'=> true
	,'use_pretty_json_print'=>true
	,'echo_errors'=>false
);

$base_url= 'http://local.test/pfa/';
global $url_settings;
$url_settings= (object)array(
	'base'=> $base_url
	,'ui_backend'=> $base_url.'ui-backend.php'
);

global $generate_password_length;
$generate_password_length= 8;

global $session_settings;
$session_settings= (object)array(
	'token_salt'=> '000102030405060702390a0b0c0d0e0f10fe12131415161718191a1b1c1d1e1f'
	,'max_auth_seconds'=> 60*60 // 1 час
	,'max_auth_idle_seconds'=> 10*60 // 10 минут
);

global $job_params;
$job_params= (object)array(
	'pid_file_path_for_send_email_job'=>'/home/rsit/pfa/cron/sendemails.pid'
);
