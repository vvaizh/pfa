
alter table debtor add `DocBody` longblob NOT NULL;

insert into tbl_migration set MigrationNumber='m003', MigrationName='AddDocBody';
