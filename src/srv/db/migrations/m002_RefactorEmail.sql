
drop table attachment;
drop table emailmessage;

CREATE TABLE `email_message` (
  `Details` longblob,
  `EmailType` char(1) NOT NULL,
  `RecipientEmail` varchar(100) NOT NULL,
  `RecipientId` int(11) NOT NULL,
  `RecipientType` char(1) NOT NULL,
  `TimeDispatch` datetime NOT NULL,
  `id_Email_Message` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Email_Message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `email_sender` (
  `SenderServer` varchar(50) DEFAULT NULL,
  `SenderUser` varchar(50) DEFAULT NULL,
  `id_Email_Sender` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Email_Sender`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `email_error` (
  `Description` text NOT NULL,
  `TimeError` datetime NOT NULL,
  `id_Email_Error` int(11) NOT NULL AUTO_INCREMENT,
  `id_Email_Message` int(11) NOT NULL,
  `id_Email_Sender` int(11) NOT NULL,
  PRIMARY KEY (`id_Email_Error`),
  KEY `Email_Message_Error` (`id_Email_Message`),
  KEY `Email_Sender_Error` (`id_Email_Sender`),
  CONSTRAINT `Email_Message_Error` FOREIGN KEY (`id_Email_Message`) REFERENCES `email_message` (`id_Email_Message`),
  CONSTRAINT `Email_Sender_Error` FOREIGN KEY (`id_Email_Sender`) REFERENCES `email_sender` (`id_Email_Sender`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `email_attachment` (
  `Content` longblob,
  `FileName` varchar(50) DEFAULT NULL,
  `id_Email_Attachment` int(11) NOT NULL AUTO_INCREMENT,
  `id_Email_Message` int(11) NOT NULL,
  PRIMARY KEY (`id_Email_Attachment`),
  KEY `Email_Message_to_Attachment` (`id_Email_Message`),
  CONSTRAINT `Email_Message_to_Attachment` FOREIGN KEY (`id_Email_Message`) REFERENCES `email_message` (`id_Email_Message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `email_sent` (
  `Message` longblob,
  `TimeSent` datetime NOT NULL,
  `id_Email_Message` int(11) NOT NULL,
  `id_Email_Sender` int(11) NOT NULL,
  `id_Email_sent` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Email_sent`),
  KEY `Email_Message_Sent` (`id_Email_Message`),
  KEY `Email_Sender_Sent` (`id_Email_Sender`),
  CONSTRAINT `Email_Message_Sent` FOREIGN KEY (`id_Email_Message`) REFERENCES `email_message` (`id_Email_Message`),
  CONSTRAINT `Email_Sender_Sent` FOREIGN KEY (`id_Email_Sender`) REFERENCES `email_sender` (`id_Email_Sender`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `email_tosend` (
  `TimeDispatch` datetime NOT NULL,
  `id_Email_Message` int(11) NOT NULL,
  KEY `Email_Message_ToSend` (`id_Email_Message`),
  KEY `byTimeDispatch` (`TimeDispatch`,`id_Email_Message`),
  CONSTRAINT `Email_Message_ToSend` FOREIGN KEY (`id_Email_Message`) REFERENCES `email_message` (`id_Email_Message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

alter table debtor drop index byAbonentEmailPassword;
alter table debtor add UNIQUE KEY `byAbonentEmailPassword` (`id_Abonent`,`DebtorEmail`);

alter table email_sender add UNIQUE KEY `byServerUser` (`SenderServer`,`SenderUser`);

insert into tbl_migration set MigrationNumber='m002', MigrationName='RefactorEmail';
