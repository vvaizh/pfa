CREATE TABLE sync_log(
    id             INT            AUTO_INCREMENT,
    id_Abonent     INT            NOT NULL,
    object_id      INT,
    object_type    VARCHAR(24),
    synchronized    BIT            NOT NULL,
    log_text       TEXT,
    PRIMARY KEY (id)
)ENGINE=INNODB
;

CREATE INDEX abonent_to_synclog ON sync_log(id_Abonent)
;

ALTER TABLE sync_log ADD CONSTRAINT abonent_to_synclog 
    FOREIGN KEY (id_Abonent)
    REFERENCES abonent(id_Abonent)
;

alter table debtor drop index byAbonentEmailPassword;
alter table debtor add UNIQUE KEY `byEmailPassword` (`DebtorEmail`,`DebtorPassword`);

insert into tbl_migration set MigrationNumber='m004', MigrationName='AddSyncLog';