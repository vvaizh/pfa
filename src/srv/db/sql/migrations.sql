insert into tbl_migration set MigrationNumber='m001', MigrationName='AddIndexes';
insert into tbl_migration set MigrationNumber='m002', MigrationName='RefactorEmail';
insert into tbl_migration set MigrationNumber='m003', MigrationName='AddDocBody';
insert into tbl_migration set MigrationNumber='m004', MigrationName='AddSyncLog';