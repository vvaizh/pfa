
CREATE TABLE abonent(
    id_Abonent         INT             AUTO_INCREMENT,
    Title              VARCHAR(100)    NOT NULL,
    URL                VARCHAR(100),
    AbonentLogin       VARCHAR(40),
    AbonentPassword    VARCHAR(40),
    PRIMARY KEY (id_Abonent)
)ENGINE=INNODB
;



CREATE TABLE debtor(
    id_Debtor         INT               AUTO_INCREMENT,
    id_Abonent        INT               NOT NULL,
    DebtorEmail       VARCHAR(40),
    DebtorPassword    VARCHAR(40),
    lastName          VARCHAR(40),
    firstName         VARCHAR(40),
    middleName        VARCHAR(40),
    sum_debt          DECIMAL(10, 0),
    sum_expense       CHAR(10),
    sum_property      INT,
    sum_income        INT,
    State             CHAR(1),
    TimeCreated       DATETIME          NOT NULL,
    TimeProblem       DATE,
    Body              LONGBLOB          NOT NULL,
    DocBody           LONGBLOB          NOT NULL,
    PRIMARY KEY (id_Debtor)
)ENGINE=INNODB
;



CREATE TABLE email_attachment(
    id_Email_Attachment    INT            AUTO_INCREMENT,
    id_Email_Message       INT            NOT NULL,
    FileName               VARCHAR(50),
    Content                LONGBLOB,
    PRIMARY KEY (id_Email_Attachment)
)ENGINE=INNODB
;



CREATE TABLE email_error(
    id_Email_Error      INT         AUTO_INCREMENT,
    id_Email_Message    INT         NOT NULL,
    id_Email_Sender     INT         NOT NULL,
    TimeError           DATETIME    NOT NULL,
    Description         TEXT        NOT NULL,
    PRIMARY KEY (id_Email_Error)
)ENGINE=INNODB
;



CREATE TABLE email_message(
    id_Email_Message    INT             AUTO_INCREMENT,
    RecipientEmail      VARCHAR(100)    NOT NULL,
    RecipientId         INT             NOT NULL,
    RecipientType       CHAR(1)         NOT NULL,
    EmailType           CHAR(1)         NOT NULL,
    TimeDispatch        DATETIME        NOT NULL,
    Details             LONGBLOB,
    PRIMARY KEY (id_Email_Message)
)ENGINE=INNODB
;



CREATE TABLE email_sender(
    id_Email_Sender    INT            AUTO_INCREMENT,
    SenderServer       VARCHAR(50),
    SenderUser         VARCHAR(50),
    PRIMARY KEY (id_Email_Sender)
)ENGINE=INNODB
;



CREATE TABLE email_sent(
    id_Email_sent       INT         AUTO_INCREMENT,
    id_Email_Message    INT         NOT NULL,
    id_Email_Sender     INT         NOT NULL,
    TimeSent            DATETIME    NOT NULL,
    Message             LONGBLOB,
    PRIMARY KEY (id_Email_sent)
)ENGINE=INNODB
;



CREATE TABLE email_tosend(
    TimeDispatch        DATETIME    NOT NULL,
    id_Email_Message    INT         NOT NULL
)ENGINE=INNODB
;



CREATE TABLE session(
    id_Session      INT             AUTO_INCREMENT,
    Token           VARCHAR(100),
    Who             CHAR(1)         NOT NULL,
    id_Owner        INT             NOT NULL,
    TimeStarted     TIMESTAMP       NOT NULL,
    TimeLastUsed    DATETIME        NOT NULL,
    PRIMARY KEY (id_Session)
)ENGINE=INNODB
;



CREATE TABLE sync_log(
    id              INT            AUTO_INCREMENT,
    id_Abonent      INT            NOT NULL,
    object_id       INT,
    object_type     VARCHAR(24),
    synchronized    BIT            NOT NULL,
    log_text        TEXT,
    PRIMARY KEY (id)
)ENGINE=INNODB
;



CREATE TABLE tbl_migration(
    MigrationNumber    VARCHAR(250)    NOT NULL,
    MigrationName      VARCHAR(250)    NOT NULL,
    MigrationTime      TIMESTAMP       NOT NULL,
    PRIMARY KEY (MigrationNumber)
)ENGINE=INNODB
;



CREATE UNIQUE INDEX byEmailPassword ON debtor(DebtorEmail, DebtorPassword)
;
CREATE INDEX Abonent_to_Debtor ON debtor(id_Abonent)
;
CREATE INDEX Email_Message_to_Attachment ON email_attachment(id_Email_Message)
;
CREATE INDEX Email_Message_Error ON email_error(id_Email_Message)
;
CREATE INDEX Email_Sender_Error ON email_error(id_Email_Sender)
;
CREATE UNIQUE INDEX byServerUser ON email_sender(SenderServer, SenderUser)
;
CREATE INDEX Email_Message_Sent ON email_sent(id_Email_Message)
;
CREATE INDEX Email_Sender_Sent ON email_sent(id_Email_Sender)
;
CREATE INDEX byTimeDispatch ON email_tosend(TimeDispatch, id_Email_Message)
;
CREATE INDEX Email_Message_ToSend ON email_tosend(id_Email_Message)
;
CREATE UNIQUE INDEX byToken ON session(Token)
;
CREATE UNIQUE INDEX byWhoOwner ON session(Who, id_Owner)
;
CREATE INDEX abonent_to_synclog ON sync_log(id_Abonent)
;
ALTER TABLE debtor ADD CONSTRAINT Abonent_to_Debtor 
    FOREIGN KEY (id_Abonent)
    REFERENCES abonent(id_Abonent)
;


ALTER TABLE email_attachment ADD CONSTRAINT Email_Message_to_Attachment 
    FOREIGN KEY (id_Email_Message)
    REFERENCES email_message(id_Email_Message)
;


ALTER TABLE email_error ADD CONSTRAINT Email_Message_Error 
    FOREIGN KEY (id_Email_Message)
    REFERENCES email_message(id_Email_Message)
;

ALTER TABLE email_error ADD CONSTRAINT Email_Sender_Error 
    FOREIGN KEY (id_Email_Sender)
    REFERENCES email_sender(id_Email_Sender)
;


ALTER TABLE email_sent ADD CONSTRAINT Email_Message_Sent 
    FOREIGN KEY (id_Email_Message)
    REFERENCES email_message(id_Email_Message)
;

ALTER TABLE email_sent ADD CONSTRAINT Email_Sender_Sent 
    FOREIGN KEY (id_Email_Sender)
    REFERENCES email_sender(id_Email_Sender)
;


ALTER TABLE email_tosend ADD CONSTRAINT Email_Message_ToSend 
    FOREIGN KEY (id_Email_Message)
    REFERENCES email_message(id_Email_Message)
;


ALTER TABLE sync_log ADD CONSTRAINT abonent_to_synclog 
    FOREIGN KEY (id_Abonent)
    REFERENCES abonent(id_Abonent)
;


