<?php

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';

class integration_restart
{
	private static $_token = '';
	private static $_db_mode = '';

	const API_PATH = 'https://restart.expert';
	const API_METHOD_OAUTH = '/oauth/token';
	const API_METHOD_CLIENT = '/api/v1/clients';
	const API_METHOD_REPORT = '/api/v1/surveyresult';

	const OBJECT_TYPE_C = 'client';
	const OBJECT_TYPE_S = 'surveyresult';

	const DB_MODE_CREATE = 'create';
	const DB_MODE_UPDATE = 'update';

	private static function getToken ()
	{
		$ch = curl_init(self::API_PATH.self::API_METHOD_OAUTH);

		$data = json_encode(array(
			'client_id' => 4,
			'client_secret' => '412h0LdFS7Yr3f595oRj7rDZaFuIBBFAZfUGvY45',
			'grant_type' => 'client_credentials'
		));

		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		$result = curl_exec($ch);

		if($result === false) {
			throw new Exception('Ошибка curl: ' . curl_error($ch));
		} else {
			$result = json_decode($result);
			if(isset($result->access_token))
				self::setToken($result->access_token);
		}

		curl_close($ch);
	}

	private static function setToken ($token)
	{
		self::$_token = $token;
	}

	static function log_record($msg)
	{
		$dt= new DateTime();
		return array($dt->format('Y-m-d H:i:s'), $msg);
	}

	private static function setClient ($arr, $user, $log_id)
	{
		$logs = array();

		$logs[] = array(self::log_time(), 'start setClient');

		if(empty(self::$_token))
			self::getToken();

		if(!empty(self::$_token)){
			$logs[] = self::log_record('get token');
			$ch = curl_init(self::API_PATH.self::API_METHOD_CLIENT);

			$auth = "Authorization: Bearer ".self::$_token;
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', $auth));
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($user, JSON_UNESCAPED_UNICODE));

			$logs[] = self::log_record("send client's data");
			$result = curl_exec($ch);

			if($result === false) {
				throw new Exception('Ошибка curl: ' . curl_error($ch));
			} else {
				$sync = 0;

				if($result == '"Client added"'){
					$sync = 1;
					$logs[] = self::log_record('client was added successfully');
				} else {
					$logs[] = self::log_record('client was not added');
					$logs[] = self::log_record("error: $result");
				}

				self::setSyncLog($user['vendor_id'], $arr['vendor_id'], self::OBJECT_TYPE_C, $sync, $logs, $log_id);

				if($sync)
					return true;
			}

			curl_close($ch);
		}

		return false;
	}

	private static function setReport ($arr, $report, $log_id)
	{
		$logs = array();

		$logs[] = self::log_record('start setReport');

		if(empty(self::$_token))
			self::getToken();

		if(!empty(self::$_token)) {
			$report_res = array(
				'vendor_id' => $arr['vendor_id'],
				'user_vendor_id' => $arr['user_id'], // уникальный ид пользователя в base64 DebtorEmail+DebtorPassword
				'survey_result_data' => $report
			);

			$logs[] = self::log_record('get token');
			$ch = curl_init(self::API_PATH . self::API_METHOD_REPORT);

			$auth = "Authorization: Bearer " . self::$_token;
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Accept: application/json', 'Content-Type: application/json', $auth));
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($report_res, JSON_UNESCAPED_UNICODE));

			$logs[] = self::log_record("send report");
			$result = curl_exec($ch);

			if ($result === false) {
				throw new Exception('Ошибка curl: ' . curl_error($ch));
			} else {
				$result = json_decode($result);
				$sync = 0;

				if($result != null && isset($result->message) && empty($result->message)){
					$sync = 1;
					$logs[] = self::log_record('report was send successfully');
				} else {
					$logs[] = self::log_record('report failed');
					$logs[] = self::log_record("error: 404");
				}

				self::setSyncLog($report['id_Abonent'], $arr['vendor_id'], self::OBJECT_TYPE_S, $sync, $logs, $log_id);

				if($sync)
					return true;
			}

			curl_close($ch);
		}

		return false;
	}

	public static function sendData ($arr, $data, $log_id = null)
	{
		self::setDBMode(self::DB_MODE_CREATE);

		if(!empty($arr) && !empty($data)){
			$user = array();

			if(isset($data['Персональные_данные']) && !empty($data['Персональные_данные'])){
				$p = (object)$data['Персональные_данные'];
				$dt= new DateTime($data["Время_заполнения"]);
				$user = array(
					'vendor_id' => $arr['vendor_id'], // id_Debtor
					'create_date' => $dt->format('Y-m-d H:i:s'),
					'name' => $p->Имя,
					'surname' => $p->Фамилия,
					'patronymic' => $p->Отчество,
					'email' => $p->Email,
					'phone' => $p->Телефон,
					'user_access_token' => ''
				);
			}

			if(!empty($user)){
				$client_res = self::setClient($arr, $user, $log_id);

				if($client_res){
					if(self::$_db_mode == self::DB_MODE_UPDATE)
						self::setDBMode(self::DB_MODE_CREATE, true);

					self::setReport($arr, $data, $log_id);
				}
			}
		}
	}

	public static function sendReport ($arr, $report, $log_id = null)
	{
		self::setDBMode(self::DB_MODE_UPDATE);

		if(self::$_db_mode == self::DB_MODE_UPDATE){
			// получаем id записи из sync_log
			$query = "select id from sync_log where id_Abonent=? and object_id=?;";
			$rows = execute_query($query, array('ss', $report['id_Abonent'], $arr['vendor_id']));
			$rows_count = count($rows);

			if ($rows_count > 1)
				$log_id = $rows[0]->id;
		}

		if(!empty($arr) && !empty($report))
			self::setReport($arr, $report, $log_id);
	}

	public static function sendFull () // метод для cron
	{
		self::setDBMode(self::DB_MODE_UPDATE);

		$rows = execute_query("select 
			sl.id log_id, 
			sl.object_id object_id, 
			sl.object_type object_type,
			d.DebtorEmail email,
			d.DebtorPassword password,
			uncompress(d.Body) body
 			from sync_log sl
 			join debtor d
 			on (sl.id_Abonent=d.id_Abonent AND sl.object_id=d.id_Debtor)
 			where synchronized=0;", array());

		$rows_count = count($rows);

		if (1!=$rows_count)
			echo 'table has already synchronized';
		else {
			foreach ($rows as $row){
				// по каждому пользователю выполняем обновление данных
				echo "start sync log: ".$row->log_id."\n";
				$data = array(
					'vendor_id' => $row->object_id,
					'user_id' => base64_encode($row->email.$row->password)
				);
				if($row->object_type == self::OBJECT_TYPE_C) {
					self::sendData($data, json_decode($row->body, true), $row->log_id);
				} elseif($row->object_type == self::OBJECT_TYPE_S) {
					self::setReport($data, json_decode($row->body, true), $row->log_id);
				}
				echo "finish sync log: ".$row->log_id."\n";
			}
		}
	}

	private static function setSyncLog ($id_Abonent, $object_id, $object_type, $sync, $text, $log_id)
	{
		$str_text = '';
		foreach ($text as $str){
			$str_text .= implode(': ', $str)."\n";
		}

		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$connection = default_dbconnect();
		$connection->begin_transaction();

		try {
			if(self::$_db_mode == self::DB_MODE_CREATE){
				$txt_query = "insert into sync_log set
				  id_Abonent=?, object_id=?, object_type=?, synchronized=?, log_text=?
				;";

				execute_query_no_result($txt_query, array('iisis',
					$id_Abonent, $object_id, $object_type, $sync, $str_text));
			};

			if(self::$_db_mode == self::DB_MODE_UPDATE && !is_null($log_id)){
				$txt_query= "update sync_log set synchronized=?, log_text=? where id=?;";
				execute_query_no_result($txt_query, array('ibi', $sync, $str_text, $log_id));
			}

			$connection->commit();
		}
		catch (mysqli_sql_exception $ex)
		{
			$connection->rollback();
		}
		catch (Exception $ex)
		{
			$connection->rollback();
			throw $ex;
		}
	}

	private static function setDBMode ($mode, $force = false)
	{
		if(empty(self::$_db_mode) || $force)
			self::$_db_mode = $mode;
	}
}

if(!empty($argv) && in_array('cli', $argv))
	integration_restart::sendFull();