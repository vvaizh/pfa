<?

require_once __DIR__.'/../../assets/config.php';
require_once __DIR__.'/../../assets/helpers/db.php';
require_once __DIR__.'/../../assets/helpers/log.php';

$body_contents= array(
	file_get_contents(__DIR__.'/../../../uc/forms/pfa/brief/steps/master/tests/contents/01sav.json.etalon.txt')
);

$body_contents_count= count($body_contents);

$txt_query= "select id_Debtor from debtor limit 100;";
$debtors= execute_query($txt_query,array());

$i= 0;
foreach ($debtors as $debtor)
{
	echo 'debtor '.$debtor->id_Debtor."\r\n";
	$txt_query= "update debtor set Body=compress(?) where id_Debtor=?;";
	execute_query_no_result($txt_query,array('bi'
		,$body_contents[$i % $body_contents_count]
		,$debtor->id_Debtor
		));
	$i++;
}
