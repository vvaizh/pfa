<? 

ini_set("memory_limit", "1024M");

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

require_once '../assets/config.php';
require_once '../assets/helpers/log.php';

function trace_txt($txt)
{
	//echo date('m/d/Y h:i:s a: ', time());
	echo $txt;
	echo "\r\n";
	return $txt;
}

function trace($data)
{
	return trace_txt(is_string($data) ? $data : print_r($data,true));
}

function load_and_test_emails($fixed_test_name,$prefix,$argv)
{
	require_once 'console/sendemails/tests_sendemails.php';
	test_emails($fixed_test_name,$argv);
}

function load_and_test_smtp($fixed_test_name,$prefix,$argv)
{
	require_once 'console/sendemails/real_sendemails.php';
	test_emails($fixed_test_name,$argv);
}

global $current_date_time;
$current_date_time= date_create('2019-07-23');

global $prefixed_test_groups;
$prefixed_test_groups= array
(
	 (object)array('prefix'=>'email.', 'tests_func'=>'load_and_test_emails')
	,(object)array('prefix'=>'smtp.', 'tests_func'=>'load_and_test_smtp')
);

function Process_command_line_arguments()
{
	global $argv, $prefixed_test_groups;
	if (!isset($argv[1]))
	{
		echo "skpped test command\r\n";
	}
	else
	{
		$cmd= $argv[1];
		foreach ($prefixed_test_groups as $prefixed_test_group)
		{
			$prefix= $prefixed_test_group->prefix;
			if (0===strpos($cmd,$prefix))
			{
				$fixed_test_name= substr($cmd,strlen($prefix));
				$tests_func= $prefixed_test_group->tests_func;
				$tests_func($fixed_test_name,$prefix,$argv);
				return;
			}
		}
		switch ($cmd)
		{
			default: echo "unknown test command $cmd\r\n";
		}
	}
}

try
{
	Process_command_line_arguments();
}
catch (Exception $ex)
{
	$exception_class= get_class($ex);
	$exception_Message= $ex->getMessage();
	trace("\r\n\r\nUnhandled exception occurred: $exception_class - $exception_Message");
	trace('catched Exception:');
	trace($ex);
}