--

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
--



--
--

DROP TABLE IF EXISTS `abonent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abonent` (
  `AbonentLogin` varchar(40) DEFAULT NULL,
  `AbonentPassword` varchar(40) DEFAULT NULL,
  `Title` varchar(100) NOT NULL,
  `URL` varchar(100) DEFAULT NULL,
  `id_Abonent` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Abonent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `debtor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtor` (
  `Body` longblob NOT NULL,
  `DebtorEmail` varchar(40) DEFAULT NULL,
  `DebtorPassword` varchar(40) DEFAULT NULL,
  `DocBody` longblob NOT NULL,
  `State` char(1) DEFAULT NULL,
  `TimeCreated` datetime NOT NULL,
  `TimeProblem` date DEFAULT NULL,
  `firstName` varchar(40) DEFAULT NULL,
  `id_Abonent` int(11) NOT NULL,
  `id_Debtor` int(11) NOT NULL AUTO_INCREMENT,
  `lastName` varchar(40) DEFAULT NULL,
  `middleName` varchar(40) DEFAULT NULL,
  `sum_debt` decimal(10,0) DEFAULT NULL,
  `sum_expense` char(10) DEFAULT NULL,
  `sum_income` int(11) DEFAULT NULL,
  `sum_property` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Debtor`),
  UNIQUE KEY `byEmailPassword` (`DebtorEmail`,`DebtorPassword`),
  KEY `Abonent_to_Debtor` (`id_Abonent`)
  CONSTRAINT `Abonent_to_Debtor` FOREIGN KEY (`id_Abonent`) REFERENCES `abonent` (`id_Abonent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `email_attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_attachment` (
  `Content` longblob,
  `FileName` varchar(50) DEFAULT NULL,
  `id_Email_Attachment` int(11) NOT NULL AUTO_INCREMENT,
  `id_Email_Message` int(11) NOT NULL,
  PRIMARY KEY (`id_Email_Attachment`),
  KEY `Email_Message_to_Attachment` (`id_Email_Message`)
  CONSTRAINT `Email_Message_to_Attachment` FOREIGN KEY (`id_Email_Message`) REFERENCES `email_message` (`id_Email_Message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `email_error`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_error` (
  `Description` text NOT NULL,
  `TimeError` datetime NOT NULL,
  `id_Email_Error` int(11) NOT NULL AUTO_INCREMENT,
  `id_Email_Message` int(11) NOT NULL,
  `id_Email_Sender` int(11) NOT NULL,
  PRIMARY KEY (`id_Email_Error`),
  KEY `Email_Message_Error` (`id_Email_Message`),
  KEY `Email_Sender_Error` (`id_Email_Sender`)
  CONSTRAINT `Email_Message_Error` FOREIGN KEY (`id_Email_Message`) REFERENCES `email_message` (`id_Email_Message`),
  CONSTRAINT `Email_Sender_Error` FOREIGN KEY (`id_Email_Sender`) REFERENCES `email_sender` (`id_Email_Sender`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `email_message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_message` (
  `Details` longblob,
  `EmailType` char(1) NOT NULL,
  `RecipientEmail` varchar(100) NOT NULL,
  `RecipientId` int(11) NOT NULL,
  `RecipientType` char(1) NOT NULL,
  `TimeDispatch` datetime NOT NULL,
  `id_Email_Message` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Email_Message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `email_sender`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_sender` (
  `SenderServer` varchar(50) DEFAULT NULL,
  `SenderUser` varchar(50) DEFAULT NULL,
  `id_Email_Sender` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Email_Sender`),
  UNIQUE KEY `byServerUser` (`SenderServer`,`SenderUser`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `email_sent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_sent` (
  `Message` longblob,
  `TimeSent` datetime NOT NULL,
  `id_Email_Message` int(11) NOT NULL,
  `id_Email_Sender` int(11) NOT NULL,
  `id_Email_sent` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Email_sent`),
  KEY `Email_Message_Sent` (`id_Email_Message`),
  KEY `Email_Sender_Sent` (`id_Email_Sender`)
  CONSTRAINT `Email_Message_Sent` FOREIGN KEY (`id_Email_Message`) REFERENCES `email_message` (`id_Email_Message`),
  CONSTRAINT `Email_Sender_Sent` FOREIGN KEY (`id_Email_Sender`) REFERENCES `email_sender` (`id_Email_Sender`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `email_tosend`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_tosend` (
  `TimeDispatch` datetime NOT NULL,
  `id_Email_Message` int(11) NOT NULL,
  KEY `Email_Message_ToSend` (`id_Email_Message`),
  KEY `byTimeDispatch` (`TimeDispatch`,`id_Email_Message`)
  CONSTRAINT `Email_Message_ToSend` FOREIGN KEY (`id_Email_Message`) REFERENCES `email_message` (`id_Email_Message`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `TimeLastUsed` datetime NOT NULL,
  `TimeStarted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Token` varchar(100) DEFAULT NULL,
  `Who` char(1) NOT NULL,
  `id_Owner` int(11) NOT NULL,
  `id_Session` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Session`),
  UNIQUE KEY `byWhoOwner` (`Who`,`id_Owner`),
  UNIQUE KEY `byToken` (`Token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `sync_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sync_log` (
  `id_Abonent` int(11) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log_text` text,
  `object_id` int(11) DEFAULT NULL,
  `object_type` varchar(24) DEFAULT NULL,
  `synchronized` bit(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `abonent_to_synclog` (`id_Abonent`)
  CONSTRAINT `abonent_to_synclog` FOREIGN KEY (`id_Abonent`) REFERENCES `abonent` (`id_Abonent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `tbl_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_migration` (
  `MigrationName` varchar(250) NOT NULL,
  `MigrationNumber` varchar(250) NOT NULL,
  `MigrationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MigrationNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

