--

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
--



--
--

DROP TABLE IF EXISTS `abonent`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `abonent` (
  `AbonentLogin` varchar(40) DEFAULT NULL,
  `AbonentPassword` varchar(40) DEFAULT NULL,
  `Title` varchar(100) NOT NULL,
  `URL` varchar(100) DEFAULT NULL,
  `id_Abonent` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Abonent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `attachment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachment` (
  `Content` longblob,
  `FileName` varchar(50) DEFAULT NULL,
  `id_Attachment` int(11) NOT NULL AUTO_INCREMENT,
  `id_EmailMessage` int(11) NOT NULL,
  PRIMARY KEY (`id_Attachment`),
  KEY `EmailMessage_to_Attachment` (`id_EmailMessage`)
  CONSTRAINT `EmailMessage_to_Attachment` FOREIGN KEY (`id_EmailMessage`) REFERENCES `emailmessage` (`id_EmailMessage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `debtor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `debtor` (
  `Body` longblob NOT NULL,
  `DebtorEmail` varchar(40) DEFAULT NULL,
  `DebtorPassword` varchar(40) DEFAULT NULL,
  `State` char(1) DEFAULT NULL,
  `TimeCreated` datetime NOT NULL,
  `TimeProblem` date DEFAULT NULL,
  `firstName` varchar(40) DEFAULT NULL,
  `id_Abonent` int(11) NOT NULL,
  `id_Debtor` int(11) NOT NULL AUTO_INCREMENT,
  `lastName` varchar(40) DEFAULT NULL,
  `middleName` varchar(40) DEFAULT NULL,
  `sum_debt` decimal(10,0) DEFAULT NULL,
  `sum_expense` char(10) DEFAULT NULL,
  `sum_income` int(11) DEFAULT NULL,
  `sum_property` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Debtor`),
  KEY `Abonent_to_Debtor` (`id_Abonent`)
  CONSTRAINT `Abonent_to_Debtor` FOREIGN KEY (`id_Abonent`) REFERENCES `abonent` (`id_Abonent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `emailmessage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `emailmessage` (
  `EmailType` char(1) NOT NULL,
  `ExtraParams` longblob,
  `Message` longblob,
  `RecipientEmail` varchar(100) NOT NULL,
  `RecipientId` int(11) NOT NULL,
  `RecipientType` char(1) NOT NULL,
  `SenderServer` varchar(50) DEFAULT NULL,
  `SenderUser` varchar(50) DEFAULT NULL,
  `TimeDispatch` datetime NOT NULL,
  `TimeSent` datetime DEFAULT NULL,
  `id_EmailMessage` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_EmailMessage`),
  KEY `byRecipient` (`RecipientId`,`RecipientType`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `TimeLastUsed` datetime NOT NULL,
  `TimeStarted` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Token` varchar(100) DEFAULT NULL,
  `Who` char(1) NOT NULL,
  `id_Owner` int(11) NOT NULL,
  `id_Session` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id_Session`),
  UNIQUE KEY `byWhoOwner` (`Who`,`id_Owner`),
  UNIQUE KEY `byToken` (`Token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--

DROP TABLE IF EXISTS `tbl_migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbl_migration` (
  `MigrationName` varchar(250) NOT NULL,
  `MigrationNumber` varchar(250) NOT NULL,
  `MigrationTime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`MigrationNumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

