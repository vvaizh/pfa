*************************** 1. row ***************************
id_Email_Message: 4
  RecipientEmail: t@r.com
     RecipientId: 4
   RecipientType: d
       EmailType: a
         Details: {
	"Subject": "Регистрация анкеты для финансового анализа",
	"Receiver": "Шеварнадзе Тамара Давидовна",
	"Body": "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" height=\"100%\" width=\"100%\" style=\"background-image:url(https:\/\/restart.expert\/mails\/bg_mail.jpg);background-size:cover;\">
    <tbody>
        <tr>
            <td colspan=\"3\" height=\"60\" style=\"font-size:0;line-height:0\">&nbsp;<\/td>
        <\/tr>

        <tr>
            <td align=\"center\" valign=\"top\">
                <table bgcolor=\"#ffffff\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" width=\"600\">
                    <tbody>
                        <tr>
                            <td colspan=\"3\" height=\"25\" style=\"font-size:0;line-height:0\"><\/td>
                        <\/tr>

                        <tr>
                            <td colspan=\"1\" width=\"40\" style=\"font-size:0;line-height:0\">&nbsp;<\/td>
                            <td colspan=\"1\"> <a href=\"https:\/\/restart.expert\/\"><img border=\"0\" src=\"https:\/\/restart.expert\/mails\/logo.png\"><\/a> <\/td>
                            <td colspan=\"1\" width=\"40\" style=\"font-size:0;line-height:0\">&nbsp;<\/td>
                        <\/tr>

                        <tr>
                            <td colspan=\"1\" width=\"40\" style=\"font-size:0;line-height:0\">&nbsp;<\/td>
                            <td colspan=\"1\" height=\"70\" style=\"font-size:0;line-height:0\">&nbsp;<\/td>
                            <td colspan=\"1\" width=\"40\" style=\"font-size:0;line-height:0\">&nbsp;<\/td>
                        <\/tr>

                        <tr>
                            <td colspan=\"1\" width=\"40\" style=\"font-size:0;line-height:0\">&nbsp;<\/td>
                            <td colspan=\"1\" style=\"font-size:20px;font:'helvetica'\">Добрый день, Тамара!<\/td>
                            <td colspan=\"1\" width=\"40\" style=\"font-size:0;line-height:0\">&nbsp;<\/td>
                        <\/tr>

                        <tr>
                            <td colspan=\"1\" width=\"40\" style=\"font-size:0;line-height:1\">&nbsp;<\/td>
                            <td colspan=\"1\" height=\"40\" style=\"font-size:0;line-height:0\">&nbsp;<\/td>
                            <td colspan=\"1\" width=\"40\" style=\"font-size:0;line-height:0\">&nbsp;<\/td>
                        <\/tr>

                        <tr>
                            <td colspan=\"1\" width=\"40\" style=\"font-size:0;line-height:0\">&nbsp;<\/td>
                            <td colspan=\"1\" style=\"font-size:20px;font:'helvetica'\">
                                Мы завершили анализ вашего финансового состояния. <br>С результатом вы можете ознакомиться перейдя по ссылке: 
                                <a href=\"https:\/\/rsit.ru\/pfa\/restart-master.php?auth=dEByLmNvbSx0ZXN0dGVzdCwx\" style=\"color: #00C9FF\">https:\/\/rsit.ru\/pfa\/restart-master.php?auth=dEByLmNvbSx0ZXN0dGVzdCwx<\/a>.
                            <\/td>
                            <td colspan=\"1\" width=\"40\" style=\"font-size:0;line-height:0\">&nbsp;<\/td>
                        <\/tr>

                        <tr>
                            <td colspan=\"1\" width=\"40\" style=\"font-size:0;line-height:0\">&nbsp;<\/td>
                            <td colspan=\"1\" height=\"70\" style=\"font-size:0;line-height:0\">&nbsp;<\/td>
                            <td colspan=\"1\" width=\"40\" style=\"font-size:0;line-height:0\">&nbsp;<\/td>
                        <\/tr>

                        <tr>
                            <td colspan=\"1\" width=\"40\" style=\"font-size:0;line-height:0\">&nbsp;<\/td>
                            <td colspan=\"1\" style=\"font-size:20px;font:'helvetica'\">С уважением,<br>
                                Команда Re<a href=\"https:\/\/restart.expert\/\" style=\"color: #00C9FF\">Start<\/a>
                            <\/td>
                            <td colspan=\"1\" width=\"40\" style=\"font-size:0;line-height:0\">&nbsp;<\/td>
                        <\/tr>

                        <tr>
                            <td colspan=\"3\" height=\"50\" style=\"font-size:0;line-height:0\"><\/td>
                        <\/tr>
                    <\/tbody>
                <\/table>
            <\/td>
        <\/tr>

        <tr>
            <td colspan=\"3\" height=\"25\" style=\"font-size:0;line-height:0\">&nbsp;<\/td>
        <\/tr>
    <\/tbody>
<\/table>"
}
*************************** 1. row ***************************
id_Email_Attachment: 4
   id_Email_Message: 4
           FileName: fa_ШеварнадзеТД.doc
     Content_length: 1177
