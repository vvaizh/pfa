﻿include ..\prep.lib.txt

# тест abonent.list
wait_text "Заполнить"
shot_check_png ..\..\shots\000root.png

# тест abonent.login
je $('tr[data-id-abonent="1"] a.admin').click();
wait_text "Войти"
shot_check_png ..\..\shots\000admin_login.png

je $('input.login').val('1');
je $('input.password').val('1');
wait_click_text "Войти"

# тест debtor.jqgrid
wait_text "Сидоров"
shot_check_png ..\..\shots\000admin1.png

# тест debtor.read
wait_click_text "Иванов"
wait_text_disappeared "ajax запрос"
jw wbt.loaded_svg_with_data_attr('img/restart_logo.svg');
shot_check_png ..\..\shots\000admin1_debtor1.png

wait_click_text "Общий список абонентов"
wait_text "Заполнить"
shot_check_png ..\..\shots\000root.png

# тест debtor.login
je $('tr[data-id-abonent="1"] a.cabinet').click();
wait_text "Войти"
shot_check_png ..\..\shots\000debtor_login.png

je $('input.login').val('a@a.a');
je $('input.password').val('1');
wait_click_text "Войти"

wait_click_text "Общий список абонентов"
wait_text "Заполнить"
wait_text_disappeared "начали отправку ajax запроса"
shot_check_png ..\..\shots\000root.png

je $('tr[data-id-abonent="1"] a.apply').click();
jw wbt.loaded_svg_with_data_attr('img/restart_logo.svg');
shot_check_png ..\..\shots\000root_apply.png

exit