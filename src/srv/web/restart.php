﻿<?

require_once '../assets/config.php';
if (isset($_GET['use-test-time']))
{
	require_once '../assets/helpers/time.php';
	session_start();
	safe_store_test_time();
}
?>
<html>
	<head>
		<title>Личный финансовый помощник</title>
		<meta http-equiv="X-UA-Compatible" content="IE=10" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="language" content="ru" />

		<link rel="icon" type="image/gif/png" href="img/ico_ama.png">
		<script type="text/javascript" src="js/vendors/jquery/jquery.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.cookie.js"></script>
		<script type="text/javascript" src="js/vendors/json2.js"></script>

		<!-- вот это надо в extension ы! { -->
		<link rel="stylesheet" type="text/css" href="css/vendors/jquery/jquery-ui.css" />

		<script type="text/javascript" src="js/vendors/jquery/jquery-ui.min.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.ui.datepicker-ru.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/grid.locale-ru.js"></script>
		<script type="text/javascript" src="js/vendors/jquery/jquery.jqGrid.min.js"></script>

		<script  src="js/jquery.inputmask.min.js"></script>
		<script  src="js/inputmask.phone.extensions.js"></script>
		<script  src="js/phone-ru.js"></script>
		<script  src="js/inputmask.binding.js"></script>

		<link rel="stylesheet" type="text/css" href="css/vendors/ui.jqgrid.css" />
		<link rel="stylesheet" type="text/css" href="css/vendors/select2/select2.css" />

		<script type="text/javascript" src="js/vendors/select2/select2.js"></script>
		<script type="text/javascript" src="js/vendors/select2/select2_locale_ru.js"></script>

		<script type="text/javascript" src="js/vendors/typehead/handlebars.js"></script>
		<script type="text/javascript" src="js/vendors/typehead/typeahead.bundle.js"></script>
		
		<link rel="stylesheet" type="text/css" href="css/normalize.css" />
		<link rel="stylesheet" type="text/css" href="css/pfa.css" />
		<!-- вот это надо в extension ы! } -->

		<script type="text/javascript">
		window.onerror = function (message, source, lineno)
		{
			var msg = "Ошибка: <b>" + message + "</b><br/><br/>\n" +
				"файл: <b>" + source + "</b><br/>\n" +
				"строка: <b>" + lineno + '</b>';
			var div = $("#cpw-unhandled-error-message-form");
			div.html(msg);
			div.dialog({
				modal: true,
				width:800,height:"auto",
				buttons: { Ok: function () { $(this).dialog("close"); } }
			});
		}
		app= { 
		};
		function RegisterCpwFormsExtension(extension)
		{
			if ('pfa'==extension.key)
			{
				var base_url= '<?= $url_settings->ui_backend ?>';
				var sel= 'body.cpw-ama > div.cpw-pfa-ui-content';
				var form_spec = extension.forms.debtor.CreateController({base_url:base_url});
				form_spec.SetFormContent({id_Abonent:1});
				form_spec.Edit(sel);
			}
		}
		</script>

		<script type="text/javascript" src="js/pfa.js?2020_05_13_1619"></script>
	</head>

	<body class="cpw-ama">
		<div id="cpw-unhandled-error-message-form" title="Необработанная ошибка!"></div>
		<div class="cpw-pfa-ui-content">
			Здесь должна быть корневая форма
		</div>
	</body>
</html>
