﻿/**
При первичной иницализации выпадающего списка
**/
if (!window.pfa_select_init){
	window.pfa_select_init= function() 
	{
	  var classes = $(this).attr("class"),
		id = $(this).attr("id"),
		name = $(this).attr("name"),
		set_value = $(this).attr("value");
	  var template = '<div class="' + classes + '">';
	  template +=
		'<span class="custom-select-trigger">' +
		$(this).attr("placeholder") +
		"</span>";
	  template += '<div class="custom-options">';
	  $(this)
		.find("option")
		.each(function() {
		  template +=
			'<span class="custom-option ' +
			$(this).attr("class") +
			'" data-value="' +
			$(this).attr("value") +
			'">' +
			$(this).html() +
			"</span>";
		});
	  template += "</div></div>";

	  $(this).wrap('<div class="custom-select-wrapper"></div>');
	  $(this).hide();
	  $(this).after(template);
	  if (set_value){
		  $(this).val(set_value);
	  }
	}
}

/**
При клике на select
**/
if (!window.pfa_select_on_click){
	window.pfa_select_on_click= function() {
	var maxHeight =
	parseInt($(this)
		.parents("div.ui-dialog")
		.innerHeight())-357;
	var elemCount = $(this)
		.siblings("div.custom-options")
		.children("span.custom-option")
		.length;
	var elemHeight = parseInt($(this)
		.siblings("div.custom-options")
		.children("span.custom-option")
		.first()
		.css('lineHeight'));	
	if (!elemCount){elemCount = 0}
	if (!elemHeight){elemHeight = 0}
	if (maxHeight > elemCount*elemHeight)
		{
			maxHeight = elemCount*elemHeight;
			$(this)
				.siblings("div.custom-options")
				.css("overflow-y", 'hidden');
		}
		else
		{
			$(this)
				.siblings("div.custom-options")
				.css("overflow-y", 'scroll');
		}
	$(this)
		.siblings("div.custom-options")
		.css("height", maxHeight);
	$("html").one("click", function() {
		$(".custom-select").removeClass("opened");
	  });
	$(this)
		.parents(".custom-select")
		.toggleClass("opened");
	if (event.stopPropagation) event.stopPropagation(); else event.cancelBubble = true;
	}
}

/**
При клике на опцию выпаюающего списка
**/
if (!window.pfa_select_on_option_click)
{
	window.pfa_select_on_option_click= function() {	
	$(this)
		.parent()
		.siblings('span.select-label')
		.removeClass("full-scale");	
	$(this)
		.parents(".custom-select-wrapper")
		.find("select")
		.val($(this).data("value"));	
	$(this)
		.parents(".custom-select-wrapper")
		.find("select")
		.change();
	$(this)
		.parents(".custom-options")
		.find(".custom-option")
		.removeClass("selection");
	$(this).addClass("selection");
	$(this)
		.parents(".custom-select")
		.removeClass("opened");
	$(this)
		.parents(".custom-select")
		.find(".custom-select-trigger")
		.text($(this).text());
	}
}

/**
При подстановке значения
**/
if (!window.pfa_select_on_changes)
{
	window.pfa_select_on_changes= function() {
		$(".select-label").removeClass("full-scale");
		
		var value = $(this).val().replace(/_/g, ' ');
		
	  $(this)
		.parent(".custom-select-wrapper")
		.children("div.custom-select.sources")
		.children('.custom-options')
		.find(".custom-option")
		.removeClass("selection");
		
	  $(this)
		.parent(".custom-select-wrapper")
		.children("div.custom-select.sources")
		.children('.custom-options')
		.find(" :contains('" + value + "')")
		.addClass("selection")

	  $(this)
		.parent(".custom-select-wrapper")
		.find(".custom-select-trigger")
		.text(value);
	} 
}

var bind_custom_select = function ()
{
	$(".custom-select").each(window.pfa_select_init);
	$(".custom-select").off("change", window.pfa_select_on_changes);
	$(".custom-select").on("change", window.pfa_select_on_changes);
	$(".custom-select-trigger").off("click", window.pfa_select_on_click);
	$(".custom-select-trigger").on("click", window.pfa_select_on_click);
	$(".custom-option").off("click", window.pfa_select_on_option_click);
	$(".custom-option").on("click", window.pfa_select_on_option_click);
}

bind_custom_select();