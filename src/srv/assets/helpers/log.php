<?

function write_to_log_string($txt)
{
	global $output_settings;
	file_put_contents($output_settings->log_file_name,date('m/d/Y h:i:s a: ', time()).$txt."\r\n",FILE_APPEND);
	return $txt;
}

function write_to_log($data)
{
	return write_to_log_string(is_string($data) ? $data : print_r($data,true));
}

