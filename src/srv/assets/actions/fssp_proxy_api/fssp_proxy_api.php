<?php

write_to_log("Entered fssp_proxy_api");

require_once '../assets/helpers/validate.php';

global $fssp_api_url, $fssp_api_physical_url, $fssp_api_legal_url, $fssp_api_ip_url, $fssp_api_group_url, $fssp_api_result_url, $fssp_api_token;
$fssp_api_url = "https://api-ip.fssp.gov.ru/api/v1.0/";
$fssp_api_physical_url = "https://api-ip.fssp.gov.ru/api/v1.0/search/physical";
$fssp_api_legal_url = "https://api-ip.fssp.gov.ru/api/v1.0/search/legal";
$fssp_api_ip_url = "https://api-ip.fssp.gov.ru/api/v1.0/search/ip";
$fssp_api_group_url = "https://api-ip.fssp.gov.ru/api/v1.0/search/group";
$fssp_api_result_url = "https://api-ip.fssp.gov.ru/api/v1.0/result";
$fssp_api_token = "qhMJEG6HRyg1";

if(!isset($_GET['cmd']))
	exit_bad_request('cmd is missed');

write_to_log("cmd param is ".$_GET['cmd']);

switch ($_GET['cmd'])
{
	case "physical":
		GetPhysical();
		break;
	case "legal":
		GetLegal();
		break;
	case "ip":
		GetIP();
		break;
}

function GetPhysical()
{
	write_to_log("In GetPhysical");
	ValidatePhysicalArgs();
	global $fssp_api_token, $fssp_api_physical_url;
	$query = http_build_query(
		array(
			'firstname' => $_GET['firstname']
			, 'lastname' => $_GET['lastname']
			, 'region' => $_GET['region']
		)
	);
	$url = $fssp_api_physical_url.'?'.$query.'&token='.$fssp_api_token;
	if(isset($_GET['secondname']))
		$url .= http_build_query(array('secondname'=>$_GET['secondname']));
	if(isset($_GET['birthdate']))
		$url .= http_build_query(array('birthdate'=>$_GET['birthdate']));
	write_to_log("builded url is \"$url\"");
	$curl = CreateCurl($url);
	$response = curl_exec($curl);
	write_to_log("curl response is $response");
	ValidateCurl($curl);
	$decoded_response = json_decode($response);
	if ($decoded_response && $decoded_response->status=="success")
	{
		GetResult($decoded_response->response->task);
	}
}

function GetLegal()
{
	write_to_log("In GetLegal");
	ValidateLegalArgs();
	global $fssp_api_token, $fssp_api_legal_url;
	$query = http_build_query(
		array(
			'name' =>$_GET['name']
			, 'region' => $_GET['region']
		)
	);
	$url = $fssp_api_legal_url.'?'.$query.'&token='.$fssp_api_token;
	if(isset($_GET['address']))
		$url .= http_build_query(array('address'=>$_GET['address']));
	write_to_log("builded url is \"$url\"");
	$curl = CreateCurl($url);
	$response = curl_exec($curl);
	write_to_log("curl response is $response");
	ValidateCurl($curl);
	$decoded_response = json_decode($response);
	if ($decoded_response&&$decoded_response->status=="success")
	{
		GetResult($decoded_response->response->task);
	}
}

function GetIP()
{
	write_to_log("In GetIP");
	ValidateIPArgs();
	global $fssp_api_token, $fssp_api_ip_url;
	$query = http_build_query(array('number' =>$_GET['number']));
	$url = $fssp_api_ip_url.'?'.$query.'&token='.$fssp_api_token;
	write_to_log("builded url is \"$url\"");
	$curl = CreateCurl($url);
	$response = curl_exec($curl);
	write_to_log("curl response is $response");
	ValidateCurl($curl);
	$decoded_response = json_decode($response);
	if ($decoded_response&&$decoded_response->status=="success")
	{
		GetResult($decoded_response->response->task);
	}
}

function ValidatePhysicalArgs()
{
	if(!isset($_GET['firstname']))
		exit_bad_request("firstname missed!");
	if(!isset($_GET['lastname']))
		exit_bad_request("lastname missed!");
	if(!isset($_GET['region']))
		exit_bad_request("region missed!");
	write_to_log("Physical args validated");
}

function ValidateLegalArgs()
{
	if(!isset($_GET['name']))
		exit_bad_request("name missed!");
	if(!isset($_GET['region']))
		exit_bad_request("region missed!");
	write_to_log("Legal args validated");

}

function ValidateIPArgs()
{
	if(!isset($_GET['number']))
		exit_bad_request("number missed!");
	write_to_log("IP args validated");
}

function GetResult($task)
{
	do
	{
		global $fssp_api_result_url, $fssp_api_token;
		$url = $fssp_api_result_url."?task=".$task."&token=".$fssp_api_token;
		$curl = CreateCurl($url);
		$response = curl_exec($curl);
		write_to_log("results response is $response");
		ValidateCurl($curl);
		$result = json_decode($response);
		$status = $result->response->status;
		if ($result->status == "success" && $status == 0)
		{
			echo $response;
		}
		
	} while($status !=0 );
}

function ValidateCurl($curl)
{
	$error = curl_error($curl);
	$errno = curl_errno($curl);
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
	curl_close($curl);
	if (200 != $httpcode || $errno!=0 || $error!="")
		exit_internal_server_error("can not bad responsefor url \"$url\", httpcode - $httpcode, error - $error, errno - $errno)");
	write_to_log("Curl validated");
}

function CreateCurl($url)
{
	$curl = curl_init();
    curl_setopt($curl,CURLOPT_URL,$url);
    curl_setopt($curl,CURLOPT_RETURNTRANSFER,true);
	curl_setopt($curl,CURLOPT_SSL_VERIFYPEER,false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	write_to_log("Created curl for $url");
	return $curl;
}