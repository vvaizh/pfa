<?

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';

$auth_token='7b944ac60e6b04df5324565f2bd5fece';
$query= urlencode($_GET['query']);

$url= "https://probili.ru/rest/company?auth_token=$auth_token&query=$query";

$curl = curl_init();
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_FAILONERROR, 1);
curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
$curl_response= curl_exec($curl);
$httpcode= curl_getinfo($curl, CURLINFO_HTTP_CODE);
curl_close($curl);

if (200!=$httpcode)
	exit_internal_server_error("can not request probili company list");

header('Content-Type: text/plain');
echo $curl_response;
