<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';

require_once '../assets/helpers/validate.php';

CheckMandatoryPOST('login');
CheckMandatoryPOST('password');
CheckMandatoryPOST('id_Abonent');

$login= $_POST['login'];
$password= $_POST['password'];
$id_Abonent= $_POST['id_Abonent'];

$txt_query= "select 
	id_Abonent
	,Title Название
from abonent
where AbonentLogin=?
and AbonentPassword=?
and id_Abonent=?
;";

$rows= execute_query($txt_query,array('sss',$login,$password,$id_Abonent));
$rows_count= count($rows);

if (1!=$rows_count)
{
	write_to_log("wrong login \"$login\" password \"$password\" as admin of $id_Abonent - found $rows_count rows");
	echo 'null';
}
else
{
	$admin= $rows[0];

	global $_SESSION, $auth_info;
	if (!isset($auth_info))
		session_start();
	$auth_info= (object)array('category'=>'abonent','id_Abonent'=>$admin->id_Abonent);
	$_SESSION['auth_info']= $auth_info;

	echo nice_json_encode($admin);
}
