<?

require_once '../assets/helpers/json.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/jqgrid.php';
require_once '../assets/helpers/validate.php';

require_once '../assets/libs/alib_auth.php';

CheckMandatoryGET_id('id_Abonent');
$id_Abonent= intval($_GET['id_Abonent']);

$auth_info= CheckAuthAbonent();

if ($id_Abonent != $auth_info->id_Abonent)
	exit_unauthorized("wrong access to id_Abonent=$id_Abonent");

$fields= "
	 d.id_Debtor id_Debtor
	,d.firstName firstName
	,d.lastName lastName
	,d.middleName middleName
";

$from_where= " from debtor d where d.id_Abonent=$id_Abonent ";

$filter_rule_builders= array(
	'firstName'=>'std_filter_rule_builder'
	,'lastName'=>'std_filter_rule_builder'
	,'middleName'=>'std_filter_rule_builder'
);
execute_query_for_jqgrid($fields,$from_where,$filter_rule_builders);
