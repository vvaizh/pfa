<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/crud.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';
require_once '../assets/helpers/password.php';
require_once '../assets/helpers/b64.php';

require_once '../assets/libs/alib_auth.php';
require_once '../assets/actions/debtor/alib_debtor_login.php';

require_once '../integrations/integration_restart.php';

function exit_duplicate_debtor_login()
{
	echo '{ "ok": false, "reason":"В базе уже есть анкета с таким адресом электронной почты!" }';
	exit;
}

function ProcessMySqlException_check_duplicate_Debtor_login($ex, $email)
{
	if ("Duplicate entry '$email' for key 'byDebtorEmailPassword'" != $ex->getMessage())
	{
		throw $ex;
	}
	else
	{
		exit_duplicate_debtor_login();
	}
}

function Dispatch_email_on_создание_анкеты($connection, $debtor, $password, $id_Debtor, $idAbonent, $Заключение)
{
	require_once '../assets/libs/alib_email_dispatch_to_db.php';

	$p= (object)$debtor['Персональные_данные'];
	$letter= (object)array('subject'=>'Регистрация анкеты для финансового анализа');
        $url = 'https://rsit.ru/pfa/restart-master.php?auth='.base64url_encode($p->Email.','.$password.','.$idAbonent);
	ob_start();
	include "../assets/views/letter_debtor_register.html";
	$letter->body_txt= ob_get_contents();
	ob_end_clean();
	$to_name= "$p->Фамилия $p->Имя $p->Отчество";

	$sname= $p->Фамилия;
	if (''!=$p->Имя)
		$sname.= mb_strtoupper(mb_substr($p->Имя,0,1));
	if (''!=$p->Отчество)
		$sname.= mb_strtoupper(mb_substr($p->Отчество,0,1));
	$fname= ''==$sname ? 'fa.doc' : "fa_$sname.doc";

	$letter->attachments= array(array('filename'=>$fname,'content'=>$Заключение));
	return Dispatch_Email_Message($connection, $letter, $p->Email, $to_name, 'регистрация анкеты', $id_Debtor);
}

class debtor_crud extends Base_crud
{
	function create($debtor)
	{
		$Заключение = $debtor['Заключение'];
		unset($debtor['Заключение']);
		$p = (object)$debtor['Персональные_данные'];
		mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
		$connection= default_dbconnect();
		$connection->begin_transaction();
		try
		{
			$id_Abonent = $debtor['id_Abonent'];
            if (in_array($p->Email, array('t@r.com', 'test@test.com'))){
                $password = 'testtest';
            } else {
                $password = generate_password(8);
            }
			$txt_query= "insert into debtor set
				  id_Abonent=?, TimeCreated=now()
				, lastName=?, firstName=?, middleName=?
				, DebtorEmail=?, DebtorPassword=?
				, Body=compress(?), DocBody=compress(?)
			;";
			$id_Debtor= execute_query_get_last_insert_id($txt_query,array('ssssssbs',
				$id_Abonent
				,$p->Фамилия, $p->Имя, $p->Отчество
				,$p->Email, $password,
				json_encode($debtor),$Заключение));
			if (false==Dispatch_email_on_создание_анкеты($connection,$debtor,$password,$id_Debtor,$id_Abonent,$Заключение))
				throw new Exception('can not dispatch email to register debtor');
			$connection->commit();

			// отправляем на интеграцию в restart
			$data = array(
				'vendor_id' => $id_Debtor,
				'user_id' => base64_encode($p->Email.$password)
			);
			integration_restart::sendData($data, $debtor);
		}
		catch (mysqli_sql_exception $ex)
		{
			$connection->rollback();
			ProcessMySqlException_check_duplicate_Debtor_login($ex, $p->Email);
		}
		catch (Exception $ex)
		{
			$connection->rollback();
			throw $ex;
		}
		$res= array('ok'=>true,'data'=>array('id_Debtor'=>$id_Debtor, 'Body'=> $debtor, 
			'Фамилия'=>$p->Фамилия, 'Имя'=>$p->Имя, 'Отчество'=>$p->Отчество));
		OkLoginDebtor($id_Debtor,$id_Abonent);
		echo nice_json_encode($res);
	}

	function read($id_Debtor)
	{
		$auth_info= CheckAuth();
		$id_Abonent= $auth_info->id_Abonent;
		$txt_query= "
		select 
			  id_Debtor
			, id_Abonent
			, lastName Фамилия 
			, firstName Имя 
			, middleName Отчество
			, uncompress(Body) Body
		from debtor
		where id_Debtor=? && id_Abonent=?
		;";
		$rows= execute_query($txt_query,array('ss',$id_Debtor,$id_Abonent));
		$rows_count= count($rows);

		if (1!=$rows_count)
			exit_unauthorized("wrong access to id_Debtor=$id_Debtor, id_Abonent=$id_Abonent");

		$debtor= $rows[0];
		$debtor->Body= json_decode($debtor->Body);
		echo nice_json_encode($debtor);
	}

	function update($id_Debtor,$debtor)
	{
		$auth_info= CheckAuthDebtor();

		if ($id_Debtor != $auth_info->id_Debtor)
			exit_unauthorized("wrong access to id_Debtor=$id_Debtor");

		$Заключение= $debtor['Заключение'];
		unset($debtor['Заключение']);

		$txt_query= "update debtor set Body=compress(?), DocBody=compress(?) where id_Debtor=?;";
		execute_query_no_result($txt_query,array('bss',json_encode($debtor),$Заключение,$id_Debtor));

		// отправляем на интеграцию в restart
		$rows = execute_query("select 
			DebtorEmail email,
			DebtorPassword password
 			from debtor
 			where id_Debtor=?;", array('s', $id_Debtor));

		$rows_count = count($rows);
		if ($rows_count > 0) {
			$data = array(
				'vendor_id' => $id_Debtor,
				'user_id' => base64_encode($rows[0]->email.$rows[0]->password)
			);
			integration_restart::sendReport($data, $debtor);
		}

		echo nice_json_encode(array('ok'=>true));
	}

	function delete($id_Debtors)
	{

	}
}

$crud= new debtor_crud();
$crud->process_cmd();

