<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/validate.php';

require_once '../assets/actions/debtor/alib_debtor_login.php';

CheckMandatoryPOST('login');
CheckMandatoryPOST('password');
CheckMandatoryPOST('id_Abonent');

$login= $_POST['login'];
$password= $_POST['password'];
$id_Abonent= $_POST['id_Abonent'];

$txt_query= "select 
	  id_Debtor
	, lastName Фамилия 
	, firstName Имя 
	, middleName Отчество
	, uncompress(Body) Body
from debtor
where id_Abonent=? && DebtorEmail=?
and DebtorPassword=?
;";

$rows= execute_query($txt_query,array('sss',$id_Abonent,$login,$password));
$rows_count= count($rows);

if (1!=$rows_count)
{
	write_to_log("wrong login \"$login\" password \"$password\" as debtor of $id_Abonent - found $rows_count rows");
	echo 'null';
}
else
{
	$debtor= $rows[0];
	$debtor->Body= json_decode($debtor->Body);

	OkLoginDebtor($debtor->id_Debtor,$id_Abonent);

	echo nice_json_encode($debtor);
}
