<?php

require_once '../assets/helpers/db.php';
require_once '../assets/helpers/log.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/time.php';

global $email_Recipient_specs;
$email_Recipient_specs= array
(
   (object)array('table'=>'debtor', 'code'=>'d')
);

global $email_Template_specs;
$email_Template_specs= array
(
	  (object)array('table'=>'debtor', 'code'=>'a', 'descr'=>'регистрация анкеты')
	, (object)array('table'=>'debtor', 'code'=>'p', 'descr'=>'восстановление пароля анкеты')
);

function FindByField($description_array,$key_name,$key_value)
{
	foreach ($description_array as $d)
	{
		if ($key_value==$d->$key_name)
			return $d;
	}
	return null;
}

function Find_Email_spec($email_descriptor)
{
	global $email_Template_specs;
	$Email_spec= FindByField($email_Template_specs,'descr',$email_descriptor);
	if (null!=$Email_spec)
	{
		if (!isset($Email_spec->Recipient))
		{
			global $email_Recipient_specs;
			$Email_spec->Recipient= FindByField($email_Recipient_specs,'table',$Email_spec->table);
		}
	}
	return $Email_spec;
}

function Dispatch_Email_Message($connection, $letter, $to_email, $to_name, $tpl_descr, $RecipientId)
{
	$email_spec= Find_Email_spec($tpl_descr);
	if (null==$email_spec)
	{
		write_to_log("wrong email template descriptor: \"{$tpl_descr}\"");
		return false;
	}
	else
	{
		$EmailType= $email_spec->code;
		$RecipientType= $email_spec->Recipient->code;
		$Details= array('Subject'=>$letter->subject, 'Receiver'=>$to_name, 'Body'=>$letter->body_txt);
		$dispatch_time= safe_date_create();
		$dispatch_time_sql= date_format($dispatch_time,'Y-m-d\TH:i:s');

		$txt_query= "insert into email_message set
			  RecipientEmail=?, RecipientId=?, RecipientType=?
			, EmailType=?, TimeDispatch=?, Details=compress(?);";
		$id_Email_Message= $connection->execute_query_get_last_insert_id($txt_query,
			array('ssssss'
			, $to_email,        $RecipientId,  $RecipientType
			, $EmailType, $dispatch_time_sql
			, nice_json_encode($Details)));

		if (isset($letter->attachments))
		{
			$txt_query= "insert into email_attachment set id_Email_Message=?, FileName=?, Content=?;";
			foreach ($letter->attachments as $a)
				$connection->execute_query($txt_query,array('sss', $id_Email_Message, $a['filename'], $a['content']));
		}
		$txt_query= "insert into email_tosend set id_Email_Message=?, TimeDispatch=?;";
		$connection->execute_query($txt_query,array('ss', $id_Email_Message, $dispatch_time_sql));

		return (object)array('EmailType'=>$EmailType
			, 'TimeDispatch'=> date_format($dispatch_time,'d.m.Y H:i:s')
			, 'id_Email_Message'=>$id_Email_Message
		);
	}
}
