<?

function CheckAuth()
{
	session_start();
	if (!isset($_SESSION['auth_info']))
	{
		header("HTTP/1.1 401 Unauthorized");
		write_to_log("wrong access without auth_info");
		write_to_log($_SESSION);
		exit;
	}
	return $_SESSION['auth_info'];
}

function write_to_log_auth_info()
{
	global $auth_info;
	write_to_log('for auth info:');
	write_to_log($auth_info);
}

function CheckAuth_cmd_Category($cmd_category)
{
	global $auth_info;
	if (!isset($auth_info))
		$auth_info= CheckAuth();
	if (!in_array($auth_info->category,$cmd_category[$_GET['cmd']]))
		exit_unauthorized('bad cmd!');
	return $auth_info;
}

function CheckAuthCategory($category)
{
	global $auth_info;
	if (!isset($auth_info))
		$auth_info= CheckAuth();
	if (!in_array($auth_info->category,$category))
		exit_unauthorized('bad role!');
	return $auth_info;
}

function CheckAuthAbonent()
{
	return CheckAuthCategory(array('abonent'));
}

function CheckAuthDebtor()
{
	return CheckAuthCategory(array('debtor'));
}
